
from setPortModeDlg import Ui_SetPortModeDlg

from utils import *

from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QDialog, QWidget

from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout
from PyQt6.QtWidgets import QPushButton, QLabel, QRadioButton
from PyQt6.QtWidgets import QTableWidgetItem

import discrete as dscr


#print("portMode instance")
cF6_CLASS = 	  0x0101f600

INPUT_PORT_COUNT = 35
portModeArr = [1]*INPUT_PORT_COUNT

import dependence as dep

parent = None	
dialog = None
ui = None

dimmerPwmPortIndArr = []
for i in range(5) :
	dimmerPwmPortIndArr += [str(i)]

daliUnitIndArr = []
for i in range(64) :
	daliUnitIndArr += [str(i)]

daliGroupIndArr = []
for i in range(16) :
	daliGroupIndArr += [str(i)]

#ui.comboBoxPortNum.addItems(scrpitNumArr)	
		
def init(self):
	global parent, dialog, ui
	parent = self
	dialog = QDialog()
	ui = Ui_SetPortModeDlg()
	ui.setupUi(dialog)
	scrpitNumArr = []
	for i in range(INPUT_PORT_COUNT) :
		if (i<11):
			scrpitNumArr += [str(i+1)+" DS18b20 " + str(i+1)]		
		elif (i>=11) and (i<15):
			scrpitNumArr += [str(i+1)+" INPUT " + str(i-11+6)]		
		elif (i>=15) and (i<20):
			scrpitNumArr += [str(i+1)+" INPUT " + str(i-15+1)]		
		elif (i==20):
			scrpitNumArr += [str(i+1)+" INPUT 11"]		
		elif (i==21):
			scrpitNumArr += [str(i+1)+" INPUT 10"]		
		elif (i>=22) and (i<25):
			scrpitNumArr += [str(i+1)+" ADC " + str(i-22+1)]		
		elif (i>=25) and (i<30):
			scrpitNumArr += [str(i+1)+" PWM_BUT " + str(i-25+1)]		
		elif (i>=30) and (i<35):
			scrpitNumArr += [str(i+1)+" DIM_BUT " + str(i-30+1)]		
		else:
			scrpitNumArr += [str(i+1)]		
	ui.comboBoxPortNum.addItems(scrpitNumArr)	
	ui.comboBoxPortNum.currentIndexChanged.connect(handleChangeCurrentIndex)
	
	scrpitNumArr = []
	for i in range(5) :
		scrpitNumArr += [str(i+1)]
	ui.comboBoxCtrlObjChNum.addItems(scrpitNumArr)	
	
	tab = QWidget()
	ui.tabWidget.insertTab(0, tab,"Dali") #(8)
	tab = QWidget()
	ui.tabWidget.insertTab(0, tab,"DHT") #(4)
	tab = QWidget()
	ui.tabWidget.insertTab(0, tab,"iButton") #(A)
	tab = QWidget()
	ui.tabWidget.insertTab(0, tab,"DS18B20") #(3)
	tab = QWidget()
	dscr.createDiscreteParamsWdg(self, tab)
	ui.tabWidget.insertTab(0, tab,"Discrete") #(1)
	
	ui.pushButtonWrite.clicked.connect(handleSetPortModeWrite)
	
	ui.comboBoxCtrlObj.currentIndexChanged.connect(handleComboBoxCtrlObjIndexChanged)
	#tab = QWidget()
	#ui.tabWidget.addTab(tab,"Сложный вход")
	
def handleComboBoxCtrlObjIndexChanged(id):
	ui.comboBoxCtrlObjChNum.clear()
	if id == 0 or id == 1:
		ui.comboBoxCtrlObjChNum.addItems(dimmerPwmPortIndArr)
	elif id == 2:
		ui.comboBoxCtrlObjChNum.addItems(daliUnitIndArr)
	elif id == 3:
		ui.comboBoxCtrlObjChNum.addItems(daliGroupIndArr)
	
	
def show(portInd=-1):
	#print("setportmode show " + str(portInd))
	if portInd != -1:
		ui.comboBoxPortNum.setCurrentIndex(portInd)
	dialog.exec()

def handleChangeCurrentIndex(pInd):
	pass
	#print("vava " + str(pInd))
	# if portModeArr == 0:
		# ui.tabWidget.setCurrentIndex(2)
	# else:
		# ui.tabWidget.setCurrentIndex(0)

setPortModeWindow = None	
comboPortNum = None
radioButDiscrete = None
radioButDs18b20 = None
radioButIButton = None
radioButDht     = None
dataPeriodLineEdit = None
	
def handleSetPortMode(self, pInd=-1) :
	global setPortModeWindow,comboPortNum
	global radioButDiscrete,radioButDs18b20,radioButIButton,radioButDht
	global dataPeriodLineEdit

	#portMode.show()
	#dd.show()
	#print("handleSetPortMode " + str(self) + "  " + str(pInd))
	#print(self.setPortModeWindow)
	if True :  #self.setPortModeWindow == None
		setPortModeWindow = QWidget()
		setPortModeWindow.setWindowTitle("set port mode")
		#self.setModal(True)
		
		vLayout = QVBoxLayout()
		#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
		#self.comboReason.addItems(["rise","fall"])		
		#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
		#self.comboAction.addItems(["on", "off", "inv"])
		
		#self.comboPortNum1 = QtWidgets.QComboBox()
		#self.comboActionPort = QtWidgets.QComboBox()		
		#portList = ["1","2","3","4","5","6","7","8"]
		#self.comboPortNum1.addItems(portList)			
		#self.comboActionPort.addItems(portList)
		
		if pInd == False:
			#print("add c")
			comboPortNum = QtWidgets.QComboBox()
			scrpitNumArr = []
			for i in range(1,12) :
				scrpitNumArr += [str(i)]
			#print(scrpitNumArr)
			self.comboPortNum.addItems(scrpitNumArr)	
		else:
			#print("add l")
			comboPortNum = QtWidgets.QLabel(str(pInd))
		#print("add kk")
		#self.lineEditTimeOut = QLineEdit()
		#self.lineEditTimeOut.setMaxLength(4)
		#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
		#self.lineEditTimeOut.setText("3")
		#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
		#self.lineEditTimeOut.setFixedWidth(60)
		
		#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
		#self.lineEditTimeOut.setMinimum(3)
		#self.lineEditTimeOut.setMaximum(0xffff)
			
		layout1 = QHBoxLayout()
		layout1.addWidget(QLabel("номер порта:"))
		layout1.addWidget(comboPortNum)			
		layout1.addStretch(1)
		vLayout.addLayout(layout1)
		
		layout2 = QHBoxLayout()
		layout2.addWidget(QLabel("mode:"))
		
		radioButDiscrete = QRadioButton("Discrete")
		radioButDs18b20 = QRadioButton("DS18B20")
		radioButIButton = QRadioButton("iButton")
		radioButDht = QRadioButton("DHT")
		radioButWf = QRadioButton("DHT")
		radioButDali = QRadioButton("Dali")
		radioButSwitchDevice = QRadioButton("Сложный вход")
		layout2.addWidget(radioButDiscrete )
		layout2.addWidget(radioButDs18b20)
		layout2.addWidget(radioButIButton)
		layout2.addWidget(radioButDht)
		layout2.addWidget(radioButDali)
		layout2.addWidget(radioButSwitchDevice)
		layout2.addStretch(1)
		vLayout.addLayout(layout2)
		
		layout2 = QHBoxLayout()
		layout2.addStretch(1)
		layout2.addWidget(QLabel("Период отправки данных, с:"))
		dataPeriodLineEdit = dataPeriod = QtWidgets.QComboBox() #
		dataPeriod.addItem("0 (OFF)", 0)
		dataPeriod.addItem("1", 1)
		dataPeriod.addItem("2", 2)
		dataPeriod.addItem("5", 5)
		dataPeriod.addItem("10", 10)
		dataPeriod.setFixedWidth(80)
		layout2.addWidget(dataPeriod)
		vLayout.addLayout(layout2)
		
		#layout3 = QHBoxLayout()
		#layout3.addWidget(QLabel("действие - порт:"))
		#layout3.addWidget(self.comboActionPort)
		#layout3.addWidget(QLabel("код:"))
		#layout3.addWidget(self.comboAction)
		#layout3.addWidget(QLabel("to:"))
		#layout3.addWidget(self.lineEditTimeOut)		
		#layout3.addStretch(1)
		#vLayout.addLayout(layout3)
		
		layout4 = QHBoxLayout()
		layout4.addStretch(1)
		
		butWrite = QPushButton("write")
		layout4.addWidget(butWrite)
		butWrite.clicked.connect(handleSetPortModeWrite)
		
		butOk = QPushButton("OK")
		layout4.addWidget(butOk)
		butOk.clicked.connect(lambda: self.setPortModeWindow.close())
		vLayout.addLayout(layout4)

		setPortModeWindow.setLayout(vLayout)
		setPortModeWindow.show() 
	else :
		if self.setPortModeWindow.isVisible() == True:
			self.setPortModeWindow.close()
			self.setPortModeWindow = None
		else : 
			self.setPortModeWindow.show() 
			

def handleSetPortModeWrite() :
	global parent
	id = ui.tabWidget.currentIndex()
	txt = ui.tabWidget.tabText(id)
	# if txt == "Dali":
		# print("Dali")
	# elif txt == "DHT":
		# print("DHT")
	# elif txt == "iButton":
		# print("iButton")
	# elif txt == "DS18B20":
		# print("DS18B20")
	# elif txt == "Discrete":
		# print("Discrete")
	# elif txt == "Сложный вход":
		# print("Сложный")
	# else:
		# print("unknown " + str(txt))
	
	cmdNum = None
	pNum = 0
	if isinstance(comboPortNum, QtWidgets.QLabel):
		pNum = int(comboPortNum.text()) 
	elif isinstance(comboPortNum, QtWidgets.QComboBox):
		pNum = int(comboPortNum.currentText())
	
	idx = ui.tabWidget.currentIndex() 
	tabText = ui.tabWidget.tabText(idx)
	portInd = ui.comboBoxPortNum.currentIndex()	
	
	print("set params, port:" + str(pNum) + " tabIndex:" + tabText + " portInd:"+str(portInd)) # '''+ " event period: " + str(evPeriod)''')
	if idx == 0: #"Discrete"
		parent.sendQuery(SET_PROP, [0x01, pNum, 0x00, 0x00, 0x00, 0x01,  0x00,0x01])
	elif idx == 1:  #ds18b20
		evPeriod = 1 #int(dataPeriodLineEdit.currentData())
		parent.sendQuery(SET_PROP, [0x03, portInd, evPeriod&0xff])
	elif idx == 2:  #iButton
		parent.sendQuery(SET_PROP, [0x0A, pNum, 0x00, 0x00, 0x00, 0x00, 0x00,0x01, 0x00,0x01])
	elif idx == 3:  #DHT
		evPeriod = int(dataPeriodLineEdit.currentData())
		parent.sendQuery(SET_PROP, [0x04, pNum, evPeriod&0xff])
	elif idx == 4:  #Dali
		#evPeriod = int(dataPeriodLineEdit.currentData())
		#parent.sendQuery(SET_PROP, [0x08, pNum, evPeriod&0xff])
		parent.sendQuery(SET_PROP, [0x08, portInd, 1])
	elif idx == 5:  #Сложный вход
		#portInd = int(ui.comboBoxPortNum.currentData())
		
		ctrlObjIdx = ui.comboBoxCtrlObj.currentIndex()
		ctrlObjChIdx = ui.comboBoxCtrlObjChNum.currentIndex()
		print("portId:"+str(portInd)+ " obj:"+str(ctrlObjIdx)+" ctrlObjChIdx:"+str(ctrlObjChIdx))
		parent.sendQuery(SET_PROP, [0x09, portInd, ctrlObjIdx, ctrlObjChIdx])
	else :
		print("select port mode!")
		
		
def handleGetPortMode(self):
	global parent
	for i in range(INPUT_PORT_COUNT) :
		parent.sendQuery(GET_PROP, [0xf6, i], cF6_CLASS+i)
	#self.handleUpdateDiscrete()
	#self.handleUpdateADC()
	pass

def setTableCell(table, row, cell, text):
	twi = QTableWidgetItem(text)
	twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
	table.setItem(row, cell, twi)
	
def parsePortModeGet(sqi):
	#print("dimmer get resp " + hex(sqi.cmd) + "  "+ hex(sqi.cmd))
	if sqi.cmd == 0xf6:	
		portInd = int.from_bytes(sqi.body[0], 'little')
		mode = int.from_bytes(sqi.body[1], 'little')
		#print("dimmer get f6 " + str(portInd) + "  "+ str(mode))
		modeStr = ""
		if mode==0 : modeStr = "D"
		elif mode==1: modeStr = "DS"
		elif mode==2: modeStr = "IB"
		elif mode==3: modeStr = "DHT"
		elif mode==4: modeStr = "ADC"
		elif mode==5: modeStr = "WF"
		elif mode==8: modeStr = "Dali"
		elif mode==9: modeStr = "SD"
		portModeArr[portInd] = mode
		
		#print(str(i))
		#print("cF6_CLASS portNum:" + str(portNum) + " mode:" + str(mode))						
		setTableCell(parent.ui.tableWidgetPorts, portInd, 0, modeStr)			
		dep.setPortMode(portInd, mode)
		