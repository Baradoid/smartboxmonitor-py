from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton, QCheckBox
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget
from PyQt6.QtGui import QIntValidator

from utils import *

DISCRETE_PORT_COUNT = 35
dlg = None
butWrite = None
comboPortNum = None

#dataPeriod = None

periodLineEdit = None

checkBoxRise = None
checkBoxFall = None
checkBoxAlways = None
checkBoxIncRise = None
checkBoxIncFall = None

pushDelayLineEdit = None

noiseProtectionLineEdit = None

initValLineEdit = None

curIdx = 0

mask = [0] * DISCRETE_PORT_COUNT
per =  [0] * DISCRETE_PORT_COUNT
T = [0] * DISCRETE_PORT_COUNT

DISCRETE_CLASS =  0x01010100

parent = None
def init(self):
	global parent
	parent = self
		
	self.ui.pushButtonDiscreteReadAll.clicked.connect(handleUpdateDiscrete)
	self.ui.pushButtonDiscreteSetPps.clicked.connect(handleSetDiscreteParams)
		
	self.ui.tableWidgetDiscrete.setRowCount(DISCRETE_PORT_COUNT)
	self.ui.tableWidgetDiscrete.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
	self.ui.tableWidgetDiscrete.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
	
	for i in range(self.ui.tableWidgetDiscrete.rowCount()):
		btn = QPushButton("set")
		btn.clicked.connect(callback_factory(handleSetDiscreteParams, i))
		self.ui.tableWidgetDiscrete.setCellWidget(i, 6, btn)
		#print("add")
	
def showDiscreteProps(self, pInd=-1):
	global dlg, butWrite, comboPortNum
	global periodLineEdit #dataPeriod,
	global checkBoxRise, checkBoxFall, checkBoxAlways, checkBoxIncRise, checkBoxIncFall
	global initValLineEdit, pushDelayLineEdit
	global curIdx, mask
	global butWriteCounter
	global noiseProtectionLineEdit
	
	dlg = QDialog(self)
	dlg.setWindowTitle("discrete params")
	#self.setADCPropsWindow.setWindowModality(Qt.WindowModal)
	#self.setModal(True)

	createDiscreteParamsWdg(self, dlg)

	#setDiscretePropsDlg.show() 
	if(pInd != -1):
		curIdx = pInd
	comboPortNum.setCurrentIndex(curIdx)
	handleCurIdxChanged(curIdx)

def createDiscreteParamsWdg(self, wdg):
	global butWrite, comboPortNum
	global periodLineEdit
	global checkBoxRise, checkBoxFall
	global butWriteCounter
	global pushDelayLineEdit, noiseProtectionLineEdit
	
	#print("add c")
	comboPortNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,DISCRETE_PORT_COUNT) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboPortNum.addItems(scrpitNumArr)		
	
	vLayout = QVBoxLayout()
	#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboReason.addItems(["rise","fall"])		
	#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboAction.addItems(["on", "off", "inv"])
	
	#self.comboPortNum1 = QtWidgets.QComboBox()
	#self.comboActionPort = QtWidgets.QComboBox()		
	#portList = ["1","2","3","4","5","6","7","8"]
	#self.comboPortNum1.addItems(portList)			
	#self.comboActionPort.addItems(portList)

	#print("add kk")
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
	#self.lineEditTimeOut.setMinimum(3)
	#self.lineEditTimeOut.setMaximum(0xffff)
			
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер порта:"))
	layout1.addWidget(comboPortNum)			
	comboPortNum.currentIndexChanged.connect(handleCurIdxChanged)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Период выдачи безусловных событий, с:"))
	periodLineEdit = QLineEdit("0")
	periodLineEdit.setValidator(QIntValidator(0, 65535, self));
	periodLineEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	periodLineEdit.setFixedWidth(60)
	layout1.addWidget(periodLineEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Событие и инкремент счётчика:"))
	checkBoxRise = QCheckBox("0->1")
	layout1.addWidget(checkBoxRise)			
	checkBoxFall = QCheckBox("1->0")
	layout1.addWidget(checkBoxFall)			
	#checkBoxAlways = QCheckBox("всегда")
	#layout1.addWidget(checkBoxAlways)
	
	'''layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Событие:"))
	rbRise = QRadioButton("0->1")
	layout1.addWidget(checkBoxRise)			
	rbFall = QRadioButton("1->0")
	layout1.addWidget(checkBoxFall)			
	rbAlways = QRadioButton("всегда")
	layout1.addWidget(checkBoxAlways)'''
	
	vLayout.addLayout(layout1)

	#layout1 = QHBoxLayout()
	#layout1.addWidget(QLabel("Инкремент счётчика:"))
	#checkBoxIncRise = QCheckBox("0->1")
	#layout1.addWidget(checkBoxIncRise)			
	#checkBoxIncFall = QCheckBox("1->0")
	#layout1.addWidget(checkBoxIncFall)					
	#vLayout.addLayout(layout1)
	

#	layout1 = QHBoxLayout()
#	layout1.addWidget(QLabel("Мин время отправки событий, мс:"))
#	dataPeriod = QtWidgets.QComboBox()
#	dataPeriod.addItem("1", 1)
#	dataPeriod.addItem("5", 5)
#	dataPeriod.addItem("10", 10)
#	dataPeriod.addItem("25", 25)
#	dataPeriod.addItem("50", 50)
#	dataPeriod.addItem("100", 100)
#	dataPeriod.addItem("250", 250)
#	dataPeriod.addItem("500", 500)
#	dataPeriod.setFixedWidth(60)
	#dataPeriod.setAlignment(Qt.AlignHCenter)
#	layout1.addWidget(dataPeriod)
#	layout1.addStretch(1)
#	vLayout.addLayout(layout1)
	
	layout = QHBoxLayout()
	layout.addWidget(QLabel("Длительность между нажатиями"))
	#layout.addStretch(1)
	vLayout.addLayout(layout)
	layout = QHBoxLayout()
	layout.addWidget(QLabel("(если >0 - измеряем дл. импульса), мс:"))
	#layout.addStretch(1)
	vLayout.addLayout(layout)
	
	layout = QHBoxLayout()
	le = pushDelayLineEdit = QtWidgets.QLineEdit("0")
	le.setValidator(QIntValidator(0, 0xfffffff, self));
	le.setFixedWidth(60)
	le.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout.addWidget(le)			
	vLayout.addLayout(layout)
		
	layout = QHBoxLayout()
	layout.addWidget(QLabel("Защита от дребезга"))
	vLayout.addLayout(layout)
	layout = QHBoxLayout()
	layout.addWidget(QLabel("(не воспринимать изменения дл. меньше, чем),мс:"))
	vLayout.addLayout(layout)
	
	layout = QHBoxLayout()
	le = noiseProtectionLineEdit = QtWidgets.QLineEdit("0")
	le.setValidator(QIntValidator(0, 0xfffffff, self));
	le.setFixedWidth(60)
	le.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout.addStretch(1)
	layout.addWidget(le)
	layout.addStretch(1)	
	vLayout.addLayout(layout)	
	
	layout = QHBoxLayout()
	#layout.addStretch(1)
	butWrite = QPushButton("write params")
	#layout.addStretch(1)
	butWrite.clicked.connect(handleWrite)
	layout.addWidget(butWrite)
	vLayout.addLayout(layout)


	layout = QHBoxLayout()
	layout.addWidget(QLabel("Значение счётчика:"))
	le = initValLineEdit = QtWidgets.QLineEdit("0")
	le.setValidator(QIntValidator(0, 0xfffffff, self));
	le.setFixedWidth(60)
	le.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout.addWidget(le)			
	butWriteCounter = QPushButton("setCounter")
	butWriteCounter.clicked.connect(handleWrite)
	layout.addWidget(butWriteCounter)
	layout.addStretch(1)
	vLayout.addLayout(layout)	
	
	#layout = QHBoxLayout()
	#layout.addStretch(1)
	#vLayout.addLayout(layout)	
	
	#butWriteCounter = QPushButton("write counter")
	#butWriteCounter.clicked.connect(handleWrite)
	#layout.addWidget(butWriteCounter)
	
	
	layout = QHBoxLayout()
	layout.addStretch(1)
	butOk = QPushButton("close")
	layout.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout)	
	
	wdg.setLayout(vLayout)

#def fill():
	#print("fill")
def handleCurIdxChanged(id):
	global curIdx
	#global periodLineEdit
	print("changed:" + str(id))
	curIdx = id
	
	checkBoxRise.setChecked(mask[id]&0x1)
	checkBoxFall.setChecked(mask[id]&0x2)
	#checkBoxAlways.setChecked(mask[id]&0x4)
	#checkBoxIncRise.setChecked(mask[id]&0x10)
	#checkBoxIncFall.setChecked(mask[id]&0x20)
	periodLineEdit.setText(str(per[id]))
	#fill()
	
def handleWrite():
	pId = int(comboPortNum.currentIndex())
	per[pId] = int(periodLineEdit.text())
	mask[pId] =   0x1 if checkBoxRise.isChecked() else 0
	mask[pId] |=  0x2 if checkBoxFall.isChecked() else 0
	#mask[pId] |=  0x4 if checkBoxAlways.isChecked() else 0
	#mask[pId] |=  0x10 if checkBoxIncRise.isChecked() else 0
	#mask[pId] |=  0x20 if checkBoxIncFall.isChecked() else 0
	#initVal = int(initValLineEdit.text())
	#dataPeriod = int(dataPeriod.currentData())
	
	
def handleSetDiscreteParams(pInd = -1):
	showDiscreteProps(parent, pInd)
	butWrite.clicked.connect(handleWriteDiscreteProps)
	butWriteCounter.clicked.connect(handleWriteDiscreteCounter)
	dlg.exec()

def handleWriteDiscreteProps():
	print("handleWriteDiscreteProps")
	pNum = int(comboPortNum.currentText())-1
	period = int(periodLineEdit.text())
	mask = 0
	#mask =   0x1 if dscr.checkBoxRise.isChecked() else 0
	#mask |=  0x2 if dscr.checkBoxFall.isChecked() else 0
	#mask |=  0x4 if dscr.checkBoxAlways.isChecked() else 0
	#mask |=  0x10 if dscr.checkBoxIncRise.isChecked() else 0
	#mask |=  0x20 if dscr.checkBoxIncFall.isChecked() else 0
	mask |=  0x01 if checkBoxRise.isChecked() else 0
	mask |=  0x02 if checkBoxFall.isChecked() else 0
	#initVal = int(dscr.initValLineEdit.text())
	#minEventPeriodms = int(dscr.dataPeriod.currentData())
	pushDelay = int(pushDelayLineEdit.text())
	glitch = int(noiseProtectionLineEdit.text())
	#print("mask " + hex(mask) + "  " + str(dscr.checkBoxIncRise.isChecked())+ "  "+ str(dscr.checkBoxIncFall.isChecked()))
	#print("write ADC params: " + str(pNum) + " " + str(period) + " " + str(minU1) + " " + str(minU2) + " " + str(dataPeriod))
	#print("write discrete port params: " + str(pNum) + " " + str(period))
	parent.sendQuery(SET_PROP, [0x01, pNum, period&0xff,  #(period>>8)&0xff, 
									mask, 
									(glitch>>8)&0xff, glitch&0xff,    #glitch 500
									(pushDelay>>8)&0xff, pushDelay&0xff])
									#(initVal>>24)&0xff, (initVal>>16)&0xff, (initVal>>8)&0xff, initVal&0xff])
def handleWriteDiscreteCounter(self):
	#print("handle write Counter")
	pNum = int(comboPortNum.currentText())
	initVal = int(initValLineEdit.text())
	print("write discrete counter port params: " + str(pNum) + " " + str(initVal))
	parent.sendQuery(SET_PROP, [0x13, pNum, (initVal>>24)&0xff, (initVal>>16)&0xff, (initVal>>8)&0xff, initVal&0xff])		
	
def handleUpdateDiscrete(self):
	for i in range(DISCRETE_PORT_COUNT) :
		parent.sendQuery(GET_PROP, [0x01, i+1], DISCRETE_CLASS+i)

	

	