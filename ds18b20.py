from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton, QCheckBox
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget

from utils import *

SENSORDS8_CLASS = 0x01010300

dlg = None
butWrite = None
minBorderT1LineEdit = None
minBorderT2LineEdit = None
dataPeriod = None
dataPeriodLineEdit = None 
comboPortNum = None

parent = None
def init(self):
	global parent
	parent = self
	
def showDs18b20Props():
	global dlg, butWrite
	global minBorderT1LineEdit, minBorderT2LineEdit, dataPeriodLineEdit
	global comboPortNum
	dlg = QDialog(parent)
	dlg.setWindowTitle("ds18b20 params")
	#self.setADCPropsWindow.setWindowModality(Qt.WindowModal)
	
	vLayout = QVBoxLayout()
	#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboReason.addItems(["rise","fall"])		
	#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboAction.addItems(["on", "off", "inv"])
	
	#self.comboPortNum1 = QtWidgets.QComboBox()
	#self.comboActionPort = QtWidgets.QComboBox()		
	#portList = ["1","2","3","4","5","6","7","8"]
	#self.comboPortNum1.addItems(portList)			
	#self.comboActionPort.addItems(portList)
	
	#print("add c")
	comboPortNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,12) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboPortNum.addItems(scrpitNumArr)	

	#print("add kk")
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
	#self.lineEditTimeOut.setMinimum(3)
	#self.lineEditTimeOut.setMaximum(0xffff)
			
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер порта:"))
	layout1.addWidget(comboPortNum)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Мин T1:"))
	minBorderT1LineEdit = QLineEdit("1")
	layout1.addWidget(minBorderT1LineEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Мин T2:"))	
	minBorderT2LineEdit = QLineEdit("2")	
	layout1.addWidget(minBorderT2LineEdit)	
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Период отправки данных, с:"))
	dataPeriodLineEdit = dataPeriod = QtWidgets.QComboBox()
	dataPeriod.addItem("0 (OFF)", 0)
	dataPeriod.addItem("1", 1)
	dataPeriod.addItem("2", 2)
	dataPeriod.addItem("5", 5)
	dataPeriod.addItem("10", 10)
	dataPeriod.setFixedWidth(60)
	#dataPeriod.setAlignment(Qt.AlignHCenter)
	layout1.addWidget(dataPeriod)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout4 = QHBoxLayout()
	layout4.addStretch(1)
	
	butWrite = QPushButton("write")
	layout4.addWidget(butWrite)
	
	butOk = QPushButton("close")
	layout4.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout4)

	dlg.setLayout(vLayout)

def handleSetDs18b20Params():
	showDs18b20Props()
	butWrite.clicked.connect(handleWriteDs18b20Props)
	dlg.exec()

def handleWriteDs18b20Props(self):
	#print("handleWriteDs18b20Props")
	pNum = int(ds18b20.comboPortNum.currentText())
	minT1 = int(ds18b20.minBorderT1LineEdit.text())
	minT2 = int(ds18b20.minBorderT2LineEdit.text())
	eventPeriod = int(ds18b20.dataPeriodLineEdit.currentData())
	print("write ds18b20  params: " + str(pNum) + " " + " " + str(minT1) + " " + str(minT2) + " " + str(eventPeriod))
	self.sendQuery(SET_PROP, [0x03, pNum, 
									(minT1>>8)&0xff, minT1&0xff, 
									(minT2>>8)&0xff, minT2&0xff, 
									(eventPeriod>>8)&0xff, eventPeriod&0xff])
									
def handleDs19b20ReadAll(self):
	for i in range(11) :
		parent.sendQuery(GET_PROP, [0x03, i+1], SENSORDS8_CLASS+i)