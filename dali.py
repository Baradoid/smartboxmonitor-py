from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton,QHeaderView,QTableWidgetItem, QLabel, QSlider, QWidget, QHBoxLayout
from PyQt6.QtWidgets import QMessageBox, QCheckBox
from utils import *
from PyQt6.QtCore import Qt

import butDimmerWdg

parent = None

DALI_MAX_VAL = 254
		
daliUnitsArr= []

		
def initTab(self):
	global parent
	global daliUnitsArr
	parent = self
	tableWidgetDali = self.ui.tableWidgetDali
	#print("dali init tab")
	
	self.ui.pushButtonDaliInit.clicked.connect(lambda: parent.sendQuery(SET_PROP, [0x74]))
	self.ui.pushButtonDaliScan.clicked.connect(lambda: parent.sendQuery(GET_PROP, [0x74]))
	
	self.ui.pushButtonDaliGetVals.clicked.connect(handleGetVals)
	self.ui.pushButtonDaliGetGroups.clicked.connect(handleGetGroupVals)
	
	tableWidgetDali.setColumnCount(4)
	header = tableWidgetDali.horizontalHeader()
	header.setSectionResizeMode( QtWidgets.QHeaderView.ResizeMode.ResizeToContents)
	header.setSectionResizeMode(0,QHeaderView.ResizeMode.Fixed)
	#header.setSectionResizeMode(2,QHeaderView.Fixed)
	#header.setSectionResizeMode(1,QHeaderView.ResizeToContents)
	#header.setSectionResizeMode(2,QHeaderView.ResizeToContents)
	#tableWidgetDali.horizontalHeader().setStretchLastSection(True)
	
	item = QTableWidgetItem("вкл")
	tableWidgetDali.setHorizontalHeaderItem(0, item);
	item = QTableWidgetItem("выкл")
	tableWidgetDali.setHorizontalHeaderItem(1, item);
	item = QTableWidgetItem("значение")
	tableWidgetDali.setHorizontalHeaderItem(2, item);
	item = QTableWidgetItem("член групп (15..0)")
	tableWidgetDali.setHorizontalHeaderItem(3, item);
	#item = QTableWidgetItem("uhegg")
	#tableWidgetDali.setHorizontalHeaderItem(4, item);
	#item = QTableWidgetItem("группы")
	#tableWidgetDali.setHorizontalHeaderItem(4, item);
	#for i in range(16):
	#	item = QTableWidgetItem(str(i))
	#	tableWidgetDali.setHorizontalHeaderItem(4+i, item);
		#tableWidgetDali.setColumnWidth(4+i, 5)

	groupNumArr = []
	for k in range(0,16) :
		groupNumArr += [str(k)]

	for i in range(tableWidgetDali.rowCount()) :
		tableWidgetDali.setVerticalHeaderItem(i, QTableWidgetItem(str(i)));
		
		btn = QLabel("")
		#btn.clicked.connect(callback_factory(handleWriteDepEna, i, 1))
		tableWidgetDali.setCellWidget(i, 4, btn)

		btn = QPushButton("set")
		#btn.setMaximumWidth(40)
		#btn.clicked.connect(callback_factory(handleShowSetDependenceDlg, i))
		#tableWidgetDali.setCellWidget(i, 10, btn)
		
		btn = QPushButton("вкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x75, i, 1]))
		tableWidgetDali.setCellWidget(i, 0, btn)
		
		btn = QPushButton("выкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x75, i, 0]))
		tableWidgetDali.setCellWidget(i, 1, btn)
		
		#btn = QSlider()
		#btn.setOrientation(QtCore.Qt.Orientation.Horizontal)
		#btn.setMaximum(254)
		#btn.setPageStep(50)		
		#btn.setFixedWidth(250)
		#btn.setTickPosition(QtWidgets.QSlider.TickPosition.TicksAbove)
		#btn.valueChanged.connect(lambda v, i=i:parent.sendQuery(SET_PROP, [0x76, i+1, v])) #lambda: self.handleDimmerSliderValueChanged(4, self.ui.horizontalSliderDimmer4, self.ui.checkBoxDimmerEna4)
		#tw = QTableWidgetItem();
		#btn.clicked.connect(callback_factory(handleWriteDepEna, i, 0))

		

		#layout1.addWidget(comboPortAdcNum)			
		#layout1.addWidget(portNumArr[0])
		#layout1.addWidget(portNumArr[1])
		#layout1.addWidget(portNumArr[2])
		#layout1.addStretch(1)
		h10  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x76, i, int(10*DALI_MAX_VAL/100.0)])
		h30  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x76, i, int(30*DALI_MAX_VAL/100.0)])
		h50  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x76, i, int(50*DALI_MAX_VAL/100.0)])
		h75  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x76, i, int(75*DALI_MAX_VAL/100.0)])
		h100 = lambda e,i=i:parent.sendQuery(SET_PROP, [0x76, i, int(100*DALI_MAX_VAL/100.0)])
		wdg = butDimmerWdg.init(h10,h30,h50,h75,h100)
		
		tableWidgetDali.setCellWidget(i, 2, wdg)
		
		layout1 = QHBoxLayout()
		layout1.setContentsMargins(0,0,0,0)
		#layout1.addWidget(QLabel("chNum"))

		#btn = QPushButton("set")
		#btn.setFixedWidth(50)
		#layout1.addWidget(btn)
		
		#comboGroupNum = QtWidgets.QComboBox()
		#comboGroupNum.addItems(groupNumArr)	
		#comboGroupNum.setCurrentIndex(0)	
		#layout1.addWidget(comboGroupNum)
		#btn.clicked.connect(lambda v, i=i, cg=comboGroupNum: parent.sendQuery(SET_PROP, [0x77, i+1, cg.currentIndex()]))
		
		btn = QPushButton("get")
		btn.setFixedWidth(50)
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(GET_PROP, [0x79, i]))
		layout1.addWidget(btn)

		cbArr = []
		for j in range(16):
			btn = QCheckBox("")
			cbArr += [btn]
			btn.setToolTip(str(15-j))
			btn.clicked.connect(lambda v, i=i, cg=j: parent.sendQuery(SET_PROP, [0x77 if v==True else 0x78, i, 15-cg]))
			#btn.setFixedWidth(10)
			#layout1.addWidget(btn) #QCheckBox(str(i)))
			layout1.addWidget(btn)
		
		daliUnitsArr += [cbArr]
		wdg = QWidget ()
		wdg.setLayout(layout1)
		tableWidgetDali.setCellWidget(i, 3, wdg)
		
		#layout1 = QHBoxLayout()
		#layout1.setContentsMargins(0,0,0,0)
		#wdg = QWidget ()
		#wdg.setLayout(layout1)
		#tableWidgetDali.setCellWidget(i,4,wdg)
	
	tableWidgetDaliGroup = self.ui.tableWidgetDaliGroup
	tableWidgetDaliGroup.setColumnCount(3)
	tableWidgetDaliGroup.setRowCount(len(groupNumArr))
	header = tableWidgetDaliGroup.horizontalHeader()
	header.setSectionResizeMode( QtWidgets.QHeaderView.ResizeMode.ResizeToContents)
	header.setSectionResizeMode(0,QHeaderView.ResizeMode.Fixed)
	item = QTableWidgetItem("вкл")
	tableWidgetDaliGroup.setHorizontalHeaderItem(0, item);
	item = QTableWidgetItem("выкл")
	tableWidgetDaliGroup.setHorizontalHeaderItem(1, item);
	item = QTableWidgetItem("значение")
	tableWidgetDaliGroup.setHorizontalHeaderItem(2, item);
	for i in range(tableWidgetDaliGroup.rowCount()) :
		tableWidgetDaliGroup.setVerticalHeaderItem(i, QTableWidgetItem(str(i)));
		
		btn = QLabel("")
		#btn.clicked.connect(callback_factory(handleWriteDepEna, i, 1))
		#tableWidgetDaliGroup.setCellWidget(i, 0, btn)

		btn = QPushButton("set")
		#btn.setMaximumWidth(40)
		#btn.clicked.connect(callback_factory(handleShowSetDependenceDlg, i))
		#tableWidgetDali.setCellWidget(i, 10, btn)
		
		btn = QPushButton("вкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x7a, i, 1]))
		tableWidgetDaliGroup.setCellWidget(i, 0, btn)
		
		btn = QPushButton("выкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x7a, i, 0]))
		tableWidgetDaliGroup.setCellWidget(i, 1, btn)
		
		#btn = QSlider()
		#btn.setOrientation(QtCore.Qt.Orientation.Horizontal)
		#btn.setMaximum(254)
		#btn.setPageStep(50)		
		#btn.setFixedWidth(250)
		#btn.setTickPosition(QtWidgets.QSlider.TickPosition.TicksAbove)
		#btn.valueChanged.connect(lambda v, i=i:parent.sendQuery(SET_PROP, [0x79, i+1, v])) #lambda: self.handleDimmerSliderValueChanged(4, self.ui.horizontalSliderDimmer4, self.ui.checkBoxDimmerEna4)
		#tw = QTableWidgetItem();
		#btn.clicked.connect(callback_factory(handleWriteDepEna, i, 0))
		#tableWidgetDaliGroup.setCellWidget(i, 2, btn)
		
		h10  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x7b, i, int(10*DALI_MAX_VAL/100.0)])
		h30  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x7b, i, int(30*DALI_MAX_VAL/100.0)])
		h50  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x7b, i, int(50*DALI_MAX_VAL/100.0)])
		h75  = lambda e,i=i:parent.sendQuery(SET_PROP, [0x7b, i, int(75*DALI_MAX_VAL/100.0)])
		h100 = lambda e,i=i:parent.sendQuery(SET_PROP, [0x7b, i, int(100*DALI_MAX_VAL/100.0)])
		wdg = butDimmerWdg.init(h10,h30,h50,h75,h100)
		tableWidgetDaliGroup.setCellWidget(i, 2, wdg)
		
	tableWidgetDaliGroup.setMaximumHeight(200)

	self.ui.pushButtonDaliUnlock.clicked.connect(handleUnlockButton)
	
def handleUnlockButton():
	txt = parent.ui.pushButtonDaliUnlock.text()
	if txt == "unlock" :
		tit = "Unlock dali reinitialisation"
		msg = "Внимание! Переиницализация шины Dali приведёт к перенумерации устройств на шине. Разблокировать?"
		msgBox = QMessageBox(parent);
		msgBox.setStandardButtons(QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
		msgBox.setDefaultButton(QMessageBox.StandardButton.No)
		msgBox.setIcon(QMessageBox.Icon.Warning)
		msgBox.setWindowTitle(tit)
		msgBox.setText(msg)
		msgBox.setWindowFlags(Qt.WindowType.Dialog | Qt.WindowType.CustomizeWindowHint | Qt.WindowType.WindowTitleHint | Qt.WindowType.CustomizeWindowHint)
		
		if msgBox.exec() ==  QMessageBox.StandardButton.Yes:
			parent.ui.pushButtonDaliInit.setEnabled(True)
			parent.ui.pushButtonDaliUnlock.setText("lock")
	elif txt == "lock" :
		parent.ui.pushButtonDaliInit.setEnabled(False)
		parent.ui.pushButtonDaliUnlock.setText("unlock")

def handleGetVals(self):
	for i in range(64) :
		parent.sendQuery(GET_PROP, [0x75, i])
		parent.sendQuery(GET_PROP, [0x76, i])

def handleGetGroupVals(self):
	for i in range(64) :
		#parent.sendQuery(GET_PROP, [0x77, i+1])
		#parent.sendQuery(GET_PROP, [0x78, i+1])
		parent.sendQuery(GET_PROP, [0x79, i])
		
def parseDimmerGet(self, sqi):
	global daliUnitsArr
	if (sqi.cmd == 0x77) or (sqi.cmd == 0x79):
		unitNum = int.from_bytes(sqi.body[0], 'little')
		val = int.from_bytes(sqi.body[1:3], 'big')
		print("Dali unit " + str(unitNum)+ " groups: " + hex(val))
		for i in range(16):
			if val&0x1:
				daliUnitsArr[unitNum-1][15-i].setChecked(True)
			val >>=1