GET_PROP = 0
SET_PROP = 1
EVENT_PROP = 2

def callback_factory(v, m, c = None, d = None):
	if d != None:
		return lambda: v(m, c, d)
	elif c != None:
		return lambda: v(m, c)
	else:
		return lambda: v(m)

