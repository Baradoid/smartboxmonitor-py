
GET_PROP = 0
SET_PROP = 1

parent = None
releCheckBoxes = []
def initButtons(self):
	global releCheckBoxes, parent
	parent = self
	self.ui.pushButtonRele1On.clicked.connect(lambda: handleSendReleCommand([0,0x01]))
	self.ui.pushButtonRele2On.clicked.connect(lambda: handleSendReleCommand([1,0x01]))
	self.ui.pushButtonRele3On.clicked.connect(lambda: handleSendReleCommand([2,0x01]))
	self.ui.pushButtonRele4On.clicked.connect(lambda: handleSendReleCommand([3,0x01]))
	self.ui.pushButtonRele5On.clicked.connect(lambda: handleSendReleCommand([4,0x01]))
	self.ui.pushButtonRele6On.clicked.connect(lambda: handleSendReleCommand([5,0x01]))
	self.ui.pushButtonRele7On.clicked.connect(lambda: handleSendReleCommand([6,0x01]))
	self.ui.pushButtonRele8On.clicked.connect(lambda: handleSendReleCommand([7,0x01]))
	self.ui.pushButtonRele9On.clicked.connect(lambda: handleSendReleCommand([8,0x01]))
	self.ui.pushButtonRele10On.clicked.connect(lambda: handleSendReleCommand([9,0x01]))
	self.ui.pushButtonRele11On.clicked.connect(lambda: handleSendReleCommand([10,0x01]))
	self.ui.pushButtonRele12On.clicked.connect(lambda: handleSendReleCommand([11,0x01]))
	self.ui.pushButtonRele13On.clicked.connect(lambda: handleSendReleCommand([12,0x01]))
	self.ui.pushButtonRele14On.clicked.connect(lambda: handleSendReleCommand([13,0x01]))
	self.ui.pushButtonRele15On.clicked.connect(lambda: handleSendReleCommand([14,0x01]))
	self.ui.pushButtonRele16On.clicked.connect(lambda: handleSendReleCommand([15,0x01]))
	self.ui.pushButtonRele17On.clicked.connect(lambda: handleSendReleCommand([16,0x01]))
	self.ui.pushButtonRele18On.clicked.connect(lambda: handleSendReleCommand([17,0x01]))
	self.ui.pushButtonRele19On.clicked.connect(lambda: handleSendReleCommand([18,0x01]))
	self.ui.pushButtonRele20On.clicked.connect(lambda: handleSendReleCommand([19,0x01]))
	self.ui.pushButtonRele21On.clicked.connect(lambda: handleSendReleCommand([20,0x01]))
	self.ui.pushButtonRele22On.clicked.connect(lambda: handleSendReleCommand([21,0x01]))
	
	self.ui.pushButtonRele1Off.clicked.connect(lambda: handleSendReleCommand([0,0x00]))
	self.ui.pushButtonRele2Off.clicked.connect(lambda: handleSendReleCommand([1,0x00]))
	self.ui.pushButtonRele3Off.clicked.connect(lambda: handleSendReleCommand([2,0x00]))
	self.ui.pushButtonRele4Off.clicked.connect(lambda: handleSendReleCommand([3,0x00]))
	self.ui.pushButtonRele5Off.clicked.connect(lambda: handleSendReleCommand([4,0x00]))
	self.ui.pushButtonRele6Off.clicked.connect(lambda: handleSendReleCommand([5,0x00]))
	self.ui.pushButtonRele7Off.clicked.connect(lambda: handleSendReleCommand([6,0x00]))
	self.ui.pushButtonRele8Off.clicked.connect(lambda: handleSendReleCommand([7,0x00]))
	self.ui.pushButtonRele9Off.clicked.connect(lambda: handleSendReleCommand([8,0x00]))
	self.ui.pushButtonRele10Off.clicked.connect(lambda: handleSendReleCommand([9,0x00]))
	self.ui.pushButtonRele11Off.clicked.connect(lambda: handleSendReleCommand([10,0x00]))
	self.ui.pushButtonRele12Off.clicked.connect(lambda: handleSendReleCommand([11,0x00]))
	self.ui.pushButtonRele13Off.clicked.connect(lambda: handleSendReleCommand([12,0x00]))
	self.ui.pushButtonRele14Off.clicked.connect(lambda: handleSendReleCommand([13,0x00]))
	self.ui.pushButtonRele15Off.clicked.connect(lambda: handleSendReleCommand([14,0x00]))
	self.ui.pushButtonRele16Off.clicked.connect(lambda: handleSendReleCommand([15,0x00]))
	self.ui.pushButtonRele17Off.clicked.connect(lambda: handleSendReleCommand([16,0x00]))
	self.ui.pushButtonRele18Off.clicked.connect(lambda: handleSendReleCommand([17,0x00]))
	self.ui.pushButtonRele19Off.clicked.connect(lambda: handleSendReleCommand([18,0x00]))
	self.ui.pushButtonRele20Off.clicked.connect(lambda: handleSendReleCommand([19,0x00]))
	self.ui.pushButtonRele21Off.clicked.connect(lambda: handleSendReleCommand([20,0x00]))
	self.ui.pushButtonRele22Off.clicked.connect(lambda: handleSendReleCommand([21,0x00]))

	self.ui.pushButtonRele1Inv.clicked.connect(lambda: handleSendReleCommand([0,0x02]))
	self.ui.pushButtonRele2Inv.clicked.connect(lambda: handleSendReleCommand([1,0x02]))
	self.ui.pushButtonRele3Inv.clicked.connect(lambda: handleSendReleCommand([2,0x02]))
	self.ui.pushButtonRele4Inv.clicked.connect(lambda: handleSendReleCommand([3,0x02]))
	self.ui.pushButtonRele5Inv.clicked.connect(lambda: handleSendReleCommand([4,0x02]))
	self.ui.pushButtonRele6Inv.clicked.connect(lambda: handleSendReleCommand([5,0x02]))
	self.ui.pushButtonRele7Inv.clicked.connect(lambda: handleSendReleCommand([6,0x02]))
	self.ui.pushButtonRele8Inv.clicked.connect(lambda: handleSendReleCommand([7,0x02]))
	self.ui.pushButtonRele9Inv.clicked.connect(lambda: handleSendReleCommand([8,0x02]))
	self.ui.pushButtonRele10Inv.clicked.connect(lambda: handleSendReleCommand([9,0x02]))
	self.ui.pushButtonRele11Inv.clicked.connect(lambda: handleSendReleCommand([10,0x02]))
	self.ui.pushButtonRele12Inv.clicked.connect(lambda: handleSendReleCommand([11,0x02]))
	self.ui.pushButtonRele13Inv.clicked.connect(lambda: handleSendReleCommand([12,0x02]))
	self.ui.pushButtonRele14Inv.clicked.connect(lambda: handleSendReleCommand([13,0x02]))
	self.ui.pushButtonRele15Inv.clicked.connect(lambda: handleSendReleCommand([14,0x02]))
	self.ui.pushButtonRele16Inv.clicked.connect(lambda: handleSendReleCommand([15,0x02]))
	self.ui.pushButtonRele17Inv.clicked.connect(lambda: handleSendReleCommand([16,0x02]))
	self.ui.pushButtonRele18Inv.clicked.connect(lambda: handleSendReleCommand([17,0x02]))
	self.ui.pushButtonRele19Inv.clicked.connect(lambda: handleSendReleCommand([18,0x02]))
	self.ui.pushButtonRele20Inv.clicked.connect(lambda: handleSendReleCommand([19,0x02]))
	self.ui.pushButtonRele21Inv.clicked.connect(lambda: handleSendReleCommand([20,0x02]))
	self.ui.pushButtonRele22Inv.clicked.connect(lambda: handleSendReleCommand([21,0x02]))
	
	self.ui.pushButtonRele1Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 0]))
	self.ui.pushButtonRele2Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 1]))
	self.ui.pushButtonRele3Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 2]))
	self.ui.pushButtonRele4Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 3]))
	self.ui.pushButtonRele5Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 4]))
	self.ui.pushButtonRele6Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 5]))
	self.ui.pushButtonRele7Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 6]))
	self.ui.pushButtonRele8Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 7]))
	self.ui.pushButtonRele9Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 8]))
	self.ui.pushButtonRele10Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 9]))
	self.ui.pushButtonRele11Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 10]))
	self.ui.pushButtonRele12Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 11]))
	self.ui.pushButtonRele13Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 12]))
	self.ui.pushButtonRele14Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 13]))
	self.ui.pushButtonRele15Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 14]))
	self.ui.pushButtonRele16Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 15]))
	self.ui.pushButtonRele17Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 16]))
	self.ui.pushButtonRele18Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 17]))
	self.ui.pushButtonRele19Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 18]))
	self.ui.pushButtonRele20Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 19]))
	self.ui.pushButtonRele21Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 20]))
	self.ui.pushButtonRele22Read.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x05, 21]))
	
	releCheckBoxes += [self.ui.checkBoxRele1State]
	releCheckBoxes += [self.ui.checkBoxRele2State]
	releCheckBoxes += [self.ui.checkBoxRele3State]
	releCheckBoxes += [self.ui.checkBoxRele4State]
	releCheckBoxes += [self.ui.checkBoxRele5State]
	releCheckBoxes += [self.ui.checkBoxRele6State]
	releCheckBoxes += [self.ui.checkBoxRele7State]
	releCheckBoxes += [self.ui.checkBoxRele8State]
	releCheckBoxes += [self.ui.checkBoxRele9State]
	releCheckBoxes += [self.ui.checkBoxRele10State]
	releCheckBoxes += [self.ui.checkBoxRele11State]
	releCheckBoxes += [self.ui.checkBoxRele12State]
	releCheckBoxes += [self.ui.checkBoxRele13State]
	releCheckBoxes += [self.ui.checkBoxRele14State]
	releCheckBoxes += [self.ui.checkBoxRele15State]
	releCheckBoxes += [self.ui.checkBoxRele16State]
	releCheckBoxes += [self.ui.checkBoxRele17State]
	releCheckBoxes += [self.ui.checkBoxRele18State]
	releCheckBoxes += [self.ui.checkBoxRele19State]
	releCheckBoxes += [self.ui.checkBoxRele20State]
	releCheckBoxes += [self.ui.checkBoxRele21State]
	releCheckBoxes += [self.ui.checkBoxRele22State]

	#print("checkBoxes: " + str(self.releCheckBoxes))
	#for  cb in self.releCheckBoxes :
	#	print(str(cb))
	self.ui.pushButtonRelayTest.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x6]))

def relayRush1Test(self):
	for i in range(22):
		#self.sendQuery(SET_PROP, [0x05, i+1, 2, 0, 0])
		parent.sendQuery(GET_PROP, [0x05, i])
		
def relayRush2Test(self):
	for i in range(22):
		parent.sendQuery(SET_PROP, [0x05, i, 2, 0, 0])
		parent.sendQuery(GET_PROP, [0x05, i])
def relayAllOff(self):
	for i in range(22):
		parent.sendQuery(SET_PROP, [0x05, i, 0, 0, 0])
		#self.sendQuery(GET_PROP, [0x05, i+1])
		
	
def handleSendReleCommand(cmd) :
	releDelay = 0
	if(parent.ui.radioButtonRelayDelay0ms.isChecked() == True) :
		releDelay = 0
	if(parent.ui.radioButtonRelayDelay100ms.isChecked() == True) :
		releDelay = 1
	if(parent.ui.radioButtonRelayDelay1s.isChecked() == True) :
		releDelay = 10
	if(parent.ui.radioButtonRelayDelay3s.isChecked() == True) :
		releDelay = 30
	#if(self.ui.radioButtonRelayDelay7s.isChecked() == True) :
	#	releDelay = 70
	
	parent.appendLogString("Rele delay: "+ str(releDelay))
	c = [0x05] + cmd + [(releDelay>>8)&0xff, releDelay&0xff]
	#print(c)

	parent.sendQuery(SET_PROP, c)

		
		