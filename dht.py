import struct

from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget
from PyQt6.QtWidgets import QButtonGroup
from PyQt6.QtWidgets import QTableWidget, QTableWidgetItem

from utils import *

DHT_CLASS = 	  0x01010500

dlg = None
butWrite = None
comboDhtPortNum = None
periodComboBox = None
minBorderT1LineEdit = None
minBorderT2LineEdit = None
minBorderRh1LineEdit = None
minBorderRh2LineEdit = None

dataPeriodLineEdit = None
parent = None
def init(self):
	global parent
	parent = self
	
def showDhtProps():

	global dlg, butWrite, comboDhtPortNum, bg
	global periodComboBox
	global minBorderU1LineEdit, minBorderU2LineEdit
	global dataPeriodLineEdit
	global minBorderT1LineEdit,minBorderT2LineEdit
	global minBorderRh1LineEdit,minBorderRh2LineEdit
	
	portNumArr = []
	dlg = QDialog(parent)
	dlg.setWindowTitle("DHT params")
	#self.setADCPropsWindow.setWindowModality(Qt.WindowModal)
	#self.setModal(True)
		
	vLayout = QVBoxLayout()
	#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboReason.addItems(["rise","fall"])		
	#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboAction.addItems(["on", "off", "inv"])
	
	#self.comboPortNum1 = QtWidgets.QComboBox()
	#self.comboActionPort = QtWidgets.QComboBox()		
	#portList = ["1","2","3","4","5","6","7","8"]
	#self.comboPortNum1.addItems(portList)			
	#self.comboActionPort.addItems(portList)
	
	comboDhtPortNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,12) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboDhtPortNum.addItems(scrpitNumArr)	

	#bg.buttonClicked.connect(onRadioButtonClicked)
	# '''curPortNum = int(b.text())'''
	
	#print(scrpitNumArr)
	#comboPortAdcNum.addItems(scrpitNumArr)	

	#print("add kk")
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
	#self.lineEditTimeOut.setMinimum(3)
	#self.lineEditTimeOut.setMaximum(0xffff)
			
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер порта:"))
	#layout1.addWidget(comboPortAdcNum)			
	layout1.addWidget(comboDhtPortNum)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница T1:"))
	minBorderT1LineEdit = lindEdit = QtWidgets.QLineEdit("500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница T2:"))
	minBorderT2LineEdit = lindEdit = QtWidgets.QLineEdit("500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница RH1:"))
	minBorderRh1LineEdit = lindEdit = QtWidgets.QLineEdit("500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница RH2:"))
	minBorderRh2LineEdit = lindEdit = QtWidgets.QLineEdit("500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Период отправки данных, с:"))
	dataPeriodLineEdit = dataPeriod = QtWidgets.QComboBox()
	dataPeriod.addItem("0 (OFF)", 0)
	dataPeriod.addItem("1", 1)
	dataPeriod.addItem("2", 2)
	dataPeriod.addItem("5", 5)
	dataPeriod.addItem("10", 10)
	dataPeriod.setFixedWidth(60)
	#dataPeriod.setAlignment(Qt.AlignHCenter)
	layout1.addWidget(dataPeriod)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout4 = QHBoxLayout()
	layout4.addStretch(1)
	
	butWrite = QPushButton("write")
	layout4.addWidget(butWrite)
	
	butOk = QPushButton("close")
	layout4.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout4)

	dlg.setLayout(vLayout)
	#dlg.exec() 
	

def setTableCell(table, row, cell, text):
	twi = QTableWidgetItem(text)
	twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
	table.setItem(row, cell, twi)
	
def parseDhtEvents(self, sqi):
	#print("dht event " + str(sqi.body))
	portNum = int.from_bytes(sqi.body[0], 'little')
	state = int.from_bytes(sqi.body[1], 'little')
	twi = None
	if state != 0 :
		#rh = int.from_bytes(strArr[23:24], 'little')
		#temp = int.from_bytes(strArr[23:25], 'little')
		rh = struct.unpack('>h', sqi.body[3:5])[0]
		temp = struct.unpack('>h', sqi.body[4:6])[0]
		#print("dht temp " + hex(temp/10.))
		twi = QTableWidgetItem("EE")				
		#setTableCell(self.ui.tableWidgetDht, portNum-1, 6, str(rh/10.))
		#setTableCell(self.ui.tableWidgetDht, portNum-1, 7, str(temp/10.))				
	else :
		twi = QTableWidgetItem("")
	#twi.setTextAlignment(QtCore.Qt.AlignCenter)
	#self.ui.tableWidgetDht.setItem(portNum-1, 5, twi)
	
	#state = int.from_bytes(strArr[21], 'little')
	hum = struct.unpack('>h', sqi.body[2:4])[0]
	temp = struct.unpack('>h', sqi.body[4:6])[0]
	
	r = portNum
	setTableCell(self.ui.tableWidgetDht, r, 0, "DHT")
	#setTableCell(self.ui.tableWidgetDht, r, 6, hex(state))
	setTableCell(self.ui.tableWidgetDht, r, 2, str(hum/100))
	setTableCell(self.ui.tableWidgetDht, r, 4, str(temp/100))
	setTableCell(self.ui.tableWidgetPorts, r, 2, str(temp/100))
	setTableCell(self.ui.tableWidgetPorts, r, 0, "DHT")	

def parseDhtGet(self, sqi):
	if (sqi.cmd==0x04) or (sqi.ident&0xffffff00 ==  DHT_CLASS):
		#print("dht response")
		#porInd = ident&0x000000ff 
		if sqi.errCode != 0xFF:
			'''setTableCell(self.ui.tableWidgetPorts, porInd, 2, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 0, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 1, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 2, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 3, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 4, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 5, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 6, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 7, "")
			setTableCell(self.ui.tableWidgetDht, porInd, 9, "")'''
			return	
		
		
		portNum = int.from_bytes(sqi.body[0], 'little')
		#minT1 = struct.unpack('>h', strArr[11:13])[0]
		#minT2 = struct.unpack('>h', strArr[13:15])[0]
		#minRH1 = struct.unpack('>h', strArr[15:17])[0]
		#minRH2 = struct.unpack('>h', strArr[17:19])[0]
		period = struct.unpack('>B', sqi.body[1])[0]
		state = int.from_bytes(sqi.body[2], 'little')
		hum = struct.unpack('>h', sqi.body[3:5])[0]
		temp = struct.unpack('>h', sqi.body[5:7])[0]
		
		#print("DHT port:"+ str(portNum) + " period:"+str(period) + " hum:"+str(hum) + " t:"+str(temp) )
		
		r = portNum
		setTableCell(self.ui.tableWidgetPorts, r, 2, str(temp))
		setTableCell(self.ui.tableWidgetDht, r, 0, "DHT")
		#setTableCell(self.ui.tableWidgetDht, r, 1, str(minT1))
		#setTableCell(self.ui.tableWidgetDht, r, 2, str(minT2))
		#setTableCell(self.ui.tableWidgetDht, r, 3, str(minRH1))
		#setTableCell(self.ui.tableWidgetDht, r, 4, str(minRH2))
		setTableCell(self.ui.tableWidgetDht, r, 1, str(period))
		#setTableCell(self.ui.tableWidgetDht, r, 6, hex(state))
		setTableCell(self.ui.tableWidgetDht, r, 2, str(hum/100))
		setTableCell(self.ui.tableWidgetDht, r, 4, str(temp/100))
		
def handleDHTReadAll(self) :
	for i in range(12) :
		parent.sendQuery(GET_PROP, [0x04, i+1], DHT_CLASS+i)
		
def handleSetDHTParams() :
	#print("show dht")
	showDhtProps()
	butWrite.clicked.connect(handleWriteDhtParams)
	dlg.exec()
	
def handleWriteDhtParams(self):
	pNum = int(dht.comboDhtPortNum.currentText())
	#minT1 = int(dht.minBorderT1LineEdit.text())
	#minT2 = int(dht.minBorderT2LineEdit.text())
	#minRh1 = int(dht.minBorderRh1LineEdit.text())
	#minRh2 = int(dht.minBorderRh2LineEdit.text())
	eventPeriod = int(dht.dataPeriodLineEdit.currentData())
	#dataPeriod = int(adc.dataPeriodLineEdit.currentData())
	period = 0
	
	#print("write DHT params: " + str(pNum) + " " + str(minT1) + " " + str(minT2) + " " + str(minRh1)+ " " + str(minRh2) + " " + str(eventPeriod))
	#self.sendQuery(SET_PROP, [0x04, pNum, (minT1>>8)&0xff, minT1&0xff, 
	#										(minT2>>8)&0xff, minT2&0xff, 
	#										(minRh1>>8)&0xff, minRh1&0xff, 
	#										(minRh2>>8)&0xff, minRh2&0xff, 
	#										(eventPeriod>>8)&0xff, eventPeriod&0xff])

	print("write DHT params: " + str(pNum) + " " + str(eventPeriod))
	self.sendQuery(SET_PROP, [0x04, pNum, (eventPeriod>>8)&0xff, eventPeriod&0xff])
