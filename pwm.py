from utils import *
import dimmer

parent = None
def init(self):
	global parent
	parent = self
		
	self.ui.horizontalSliderPWM1.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x43, 0, v]))
	self.ui.horizontalSliderPWM2.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x43, 1, v]))
	self.ui.horizontalSliderPWM3.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x43, 2, v]))
	self.ui.horizontalSliderPWM4.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x43, 3, v]))
	self.ui.horizontalSliderPWM5.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x43, 4, v]))
	
	self.ui.checkBoxPWM1Ena.clicked.connect(lambda v: self.sendQuery(SET_PROP, [0x42, 0, v]))
	self.ui.checkBoxPWM2Ena.clicked.connect(lambda v: self.sendQuery(SET_PROP, [0x42, 1, v]))
	self.ui.checkBoxPWM3Ena.clicked.connect(lambda v: self.sendQuery(SET_PROP, [0x42, 2, v]))
	self.ui.checkBoxPWM4Ena.clicked.connect(lambda v: self.sendQuery(SET_PROP, [0x42, 3, v]))
	self.ui.checkBoxPWM5Ena.clicked.connect(lambda v: self.sendQuery(SET_PROP, [0x42, 4, v]))
	
	#self.ui.pushButtonPWM1ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x43, 1]))
	#self.ui.pushButtonPWM2ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x43, 2]))
	#self.ui.pushButtonPWM3ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x43, 3]))
	#self.ui.pushButtonPWM4ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x43, 4]))
	#self.ui.pushButtonPWM5ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x43, 5]))
	

	self.ui.pushButtonPWMSetRange_1.clicked.connect(lambda: dimmer.handlePwmDimmerSetRange(self, 0x41, 0, 255, 0, 255))
	self.ui.pushButtonPWMSetRange_2.clicked.connect(lambda: dimmer.handlePwmDimmerSetRange(self, 0x41, 1, 255, 0, 255))
	self.ui.pushButtonPWMSetRange_3.clicked.connect(lambda: dimmer.handlePwmDimmerSetRange(self, 0x41, 2, 255, 0, 255))
	self.ui.pushButtonPWMSetRange_4.clicked.connect(lambda: dimmer.handlePwmDimmerSetRange(self, 0x41, 3, 255, 0, 255))
	self.ui.pushButtonPWMSetRange_5.clicked.connect(lambda: dimmer.handlePwmDimmerSetRange(self, 0x41, 4, 255, 0, 255))
	
	self.ui.pushButtonPWMGetRange_1.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x41, 0]))
	self.ui.pushButtonPWMGetRange_2.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x41, 1]))
	self.ui.pushButtonPWMGetRange_3.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x41, 2]))
	self.ui.pushButtonPWMGetRange_4.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x41, 3]))
	self.ui.pushButtonPWMGetRange_5.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x41, 4]))
	
	self.ui.pushButtonPWMGetVals.clicked.connect(lambda: handlePWMGetVal(self))
	
def handlePWMGetVal(self):
	for ch in range(5): 
		self.sendQuery(GET_PROP, [0x42, ch])	
		self.sendQuery(GET_PROP, [0x43, ch])	
		
def handlePWMSliderValueChanged(self, pNum, sl, cb) :
	val = sl.value()
	bEna = 0
	if cb.isChecked() == True :
		bEna = 1
	#print("PWM slider val changed " + str(id) + " v:" + str(val))
	self.sendQuery(SET_PROP, [0x43, pNum, bEna, val])
	#self.sliderTt.hide()
	val = sl.value()
	self.sliderTt.setText(str(sl.value()))
	#print(str(sl.frameGeometry().x()), str(sl.frameGeometry().y()))
	#self.sliderTt.move(sl.rect().x(), sl.rect().y()+20)
	self.sliderTt.move(sl.mapToGlobal(QPoint(sl.frameGeometry().left(),sl.frameGeometry().top())))
	self.sliderTt.adjustSize()
	#self.sliderTt.show()
	
def parsePWMGet(self, sqi):
	#print("PWM get message ch: " + str(ch)  + " val:" + str(val))
	if sqi.cmd == 0x42:
		ch =  int.from_bytes(sqi.body[0], byteorder='little')
		ena = int.from_bytes(sqi.body[1], byteorder='little')
		#print("PWM get message ch: " + str(ch)  + " val:" + str(val))
		if ch==0:
			self.ui.checkBoxPWM1Ena.setChecked(ena==1)			
		elif ch==1:
			self.ui.checkBoxPWM2Ena.setChecked(ena==1)
		elif ch==2:
			self.ui.checkBoxPWM3Ena.setChecked(ena==1)
		elif ch==3:
			self.ui.checkBoxPWM4Ena.setChecked(ena==1)
		elif ch==4:
			self.ui.checkBoxPWM5Ena.setChecked(ena==1)
	elif sqi.cmd == 0x43:
		ch =  int.from_bytes(sqi.body[0], byteorder='little')
		val = int.from_bytes(sqi.body[1], byteorder='little')
		#print("PWM get message ch: " + str(ch) + " ena:" + str(ena) + " val:" + str(val))
		if ch==0:
			#self.ui.checkBoxPWM1Ena.setChecked(ena==1)
			self.ui.horizontalSliderPWM1.setValue(val)
		elif ch==1:
			#self.ui.checkBoxPWM2Ena.setChecked(ena==1)
			self.ui.horizontalSliderPWM2.setValue(val)
		elif ch==2:
			#self.ui.checkBoxPWM3Ena.setChecked(ena==1)
			self.ui.horizontalSliderPWM3.setValue(val)
		elif ch==3:
			#self.ui.checkBoxPWM4Ena.setChecked(ena==1)
			self.ui.horizontalSliderPWM4.setValue(val)
		elif ch==4:
			#self.ui.checkBoxPWM5Ena.setChecked(ena==1)
			self.ui.horizontalSliderPWM5.setValue(val)	
	