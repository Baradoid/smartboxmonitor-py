from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QDialog, QLabel, QSlider, QPushButton,QCheckBox
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QHeaderView
from PyQt6.QtWidgets import QTableWidgetItem
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QAbstractSlider
import butDimmerWdg

from utils import *

import functools 
class CheckBoxStyle(QtWidgets.QProxyStyle):
    def subElementRect(self, element, option, widget=None):
        r = super().subElementRect(element, option, widget)
        if element == QtWidgets.QStyle.SE_ItemViewItemCheckIndicator:
            r.moveCenter(option.rect.center())
        return r

		
dimmerEnaBtList = []
dimmerDiscrBtList = []
lughtUpTwList = []
dimmerSliderList = []
parent = None
def init(self):
	global parent, dimmerSliderList
	parent = self
		
	'''self.ui.checkBoxDimmerEna1.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x33, 1, b]))
	self.ui.checkBoxDimmerEna2.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x33, 2, b]))
	self.ui.checkBoxDimmerEna3.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x33, 3, b]))
	self.ui.checkBoxDimmerEna4.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x33, 4, b]))
	self.ui.checkBoxDimmerEna5.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x33, 5, b]))'''

	'''self.ui.horizontalSliderDimmer1.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x34, 1, v]))
	self.ui.horizontalSliderDimmer2.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x34, 2, v]))
	self.ui.horizontalSliderDimmer3.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x34, 3, v]))
	self.ui.horizontalSliderDimmer4.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x34, 4, v]))
	self.ui.horizontalSliderDimmer5.valueChanged.connect(lambda v: self.sendQuery(SET_PROP, [0x34, 5, v]))'''

	#self.ui.pushButtonDimmer1ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x34, 1]))
	#self.ui.pushButtonDimmer2ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x34, 2]))
	#self.ui.pushButtonDimmer3ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x34, 3]))
	#self.ui.pushButtonDimmer4ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x34, 4]))
	#self.ui.pushButtonDimmer5ReadVal.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x34, 5]))
	
	'''self.ui.checkBoxDimmer1Discr.clicked.connect(lambda e: self.sendQuery(SET_PROP, [0x36, 1, e]))
	self.ui.checkBoxDimmer2Discr.clicked.connect(lambda e: self.sendQuery(SET_PROP, [0x36, 2, e]))
	self.ui.checkBoxDimmer3Discr.clicked.connect(lambda e: self.sendQuery(SET_PROP, [0x36, 3, e]))
	self.ui.checkBoxDimmer4Discr.clicked.connect(lambda e: self.sendQuery(SET_PROP, [0x36, 4, e]))
	self.ui.checkBoxDimmer5Discr.clicked.connect(lambda e: self.sendQuery(SET_PROP, [0x36, 5, e]))'''
	
	self.ui.pushButtonDimmerGetVals.clicked.connect(lambda: handleDimmerGetVals(self))

			
	'''self.ui.pushButtonDimmerSetRange_1.clicked.connect(lambda: handlePwmDimmerSetRange(self, 0x32, 1, 100, 0, 100))
	self.ui.pushButtonDimmerSetRange_2.clicked.connect(lambda: handlePwmDimmerSetRange(self, 0x32, 2, 100, 0, 100))
	self.ui.pushButtonDimmerSetRange_3.clicked.connect(lambda: handlePwmDimmerSetRange(self, 0x32, 3, 100, 0, 100))
	self.ui.pushButtonDimmerSetRange_4.clicked.connect(lambda: handlePwmDimmerSetRange(self, 0x32, 4, 100, 0, 100))
	self.ui.pushButtonDimmerSetRange_5.clicked.connect(lambda: handlePwmDimmerSetRange(self, 0x32, 5, 100, 0, 100))'''
	
	#self.ui.pushButtonDimmerGetRange_1.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x32, 1]))
	#self.ui.pushButtonDimmerGetRange_2.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x32, 2]))
	#self.ui.pushButtonDimmerGetRange_3.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x32, 3]))
	#self.ui.pushButtonDimmerGetRange_4.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x32, 4]))
	#self.ui.pushButtonDimmerGetRange_5.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x32, 5]))

	#self.ui.pushButtonDimmerDelaySet.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x31, QInputDialog.getInt(self, "Get delay","Seconds:", 28, 0, 100, 1)[0]]))
	#self.ui.pushButtonDimmerDelayGet.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x31]))

	tableWidgetDim = self.ui.tableWidgetDimmer
	tableWidgetDim.setRowCount(5)
	tableWidgetDim.setColumnCount(10)
	header = tableWidgetDim.horizontalHeader()
	header.setSectionResizeMode( QtWidgets.QHeaderView.ResizeMode.ResizeToContents)
	#header.setSectionResizeMode(0,QHeaderView.ResizeMode.Fixed)

	item = QTableWidgetItem("get")
	tableWidgetDim.setHorizontalHeaderItem(0, item);	
	item = QTableWidgetItem("вкл")
	tableWidgetDim.setHorizontalHeaderItem(1, item);
	item = QTableWidgetItem("выкл")
	tableWidgetDim.setHorizontalHeaderItem(2, item);
	item = QTableWidgetItem("дискр")
	tableWidgetDim.setHorizontalHeaderItem(3, item);
	item = QTableWidgetItem("min")
	tableWidgetDim.setHorizontalHeaderItem(4, item);
	item = QTableWidgetItem("max")
	tableWidgetDim.setHorizontalHeaderItem(5, item);
	item = QTableWidgetItem("set minmax")
	tableWidgetDim.setHorizontalHeaderItem(6, item);
	item = QTableWidgetItem("lightup,ms")
	tableWidgetDim.setHorizontalHeaderItem(7, item);
	item = QTableWidgetItem("set lightup")
	tableWidgetDim.setHorizontalHeaderItem(8, item);
	item = QTableWidgetItem("значение")
	tableWidgetDim.setHorizontalHeaderItem(9, item);
	
	wdg = [None, None, None, None, None]
	for i in range(5) :	
		btn = QPushButton("get")
		btn.clicked.connect(lambda v, i=i: handleDimmerGetVal(self, i))
		tableWidgetDim.setCellWidget(i, 0, btn)        
    
		btn = QPushButton("вкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x33, i, 1]))
		tableWidgetDim.setCellWidget(i, 1, btn)        
        
		btn = QPushButton("выкл")
		btn.clicked.connect(lambda v, i=i: parent.sendQuery(SET_PROP, [0x33, i, 0]))
		tableWidgetDim.setCellWidget(i, 2, btn)
		
		#btn = QCheckBox()
		#btn.clicked.connect(lambda e, i=i: self.sendQuery(SET_PROP, [0x33, i+1, e]))
		
		#tableWidgetDim.setCellWidget(i, 0, btn)
		dimmerEnaBtList.append(btn)
		#tableWidgetDim.item(i,0).setTextAlignment(Qt.AlignHCenter)
		#item.setTextAlignment(Qt.AlignHCenter)
		
		
		btn = QCheckBox()
		#btn.clicked.connect(callback_factory(handleWriteDaliEna, 0x75, i, 1))
		btn.clicked.connect(lambda e, i=i: self.sendQuery(SET_PROP, [0x36, i, e]))
		tableWidgetDim.setCellWidget(i, 3, btn)
		dimmerDiscrBtList.append(btn)
		
		btn = QPushButton("set")
		btn.clicked.connect(lambda e,i=i: handlePwmDimmerSetRange(self, 0x32, i, 100, 0, 100))
		tableWidgetDim.setCellWidget(i, 6, btn)
		
		tw = QTableWidgetItem()  #
		tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
		self.ui.tableWidgetDimmer.setItem(i, 7, tw)
		lughtUpTwList.append(tw)
		
		btn = QPushButton("set")
		#btn.clicked.connect(lambda e,i=i: handlePwmDimmerSetRange(self, 0x32, i+1, 100, 0, 100))
		tableWidgetDim.setCellWidget(i, 8, btn)
			
		btn = QSlider()
		btn.setOrientation(QtCore.Qt.Orientation.Horizontal)
		btn.setMaximum(100)
		btn.setPageStep(25)		
		btn.setFixedWidth(250)
		btn.setTickPosition(QtWidgets.QSlider.TickPosition.TicksAbove)
		#btn.valueChanged.connect(lambda v, i=i: self.sendQuery(SET_PROP, [0x34, i+1, v]) )
		btn.sliderReleased.connect(lambda i=i,btn=btn: self.sendQuery(SET_PROP, [0x34, i, btn.value()]) )
		#btn.sliderMoved.connect(lambda i=i: self.sendQuery(SET_PROP, [0x34, i+1, btn.value()]) )
		#btn.setTracking(False)
		btn.sliderReleased.connect(lambda btn=btn: print("sl act: realesed " + str(btn.value())) )
		#btn.actionTriggered.connect(lambda e: print("sl act:" + str(e)) if e!=QAbstractSlider.SliderAction.SliderMove else print("") )
		btn.actionTriggered.connect(lambda e, btn=btn: print("sl act:" + str(e) + " " + str(btn.value())) if(e!=7 and e!=2 and e!=1) else None )
		btn.actionTriggered.connect(lambda e, btn=btn, i=i: self.sendQuery(SET_PROP, [0x34, i, btn.value()]) if(e!=7 and e!=2 and e!=1) else None )
		
		#tableWidgetDim.setCellWidget(i, 7, btn)
		
		
		h10  = lambda e,i=i: parent.sendQuery(SET_PROP, [0x34, i, 10])
		h30  = lambda e,i=i: parent.sendQuery(SET_PROP, [0x34, i, 30])
		h50  = lambda e,i=i: parent.sendQuery(SET_PROP, [0x34, i, 50])
		h75  = lambda e,i=i: parent.sendQuery(SET_PROP, [0x34, i, 75])
		h100 = lambda e,i=i: parent.sendQuery(SET_PROP, [0x34, i, 100])
		
		w = butDimmerWdg.init(h10,h30,h50,h75,h100)
		tableWidgetDim.setCellWidget(i, 9, w)
		wdg[i] = w
		dimmerSliderList.append(btn)
	#print(str(wdg))
		
def handleDimmerSliderValueChanged(self, pNum, sl, cb) :
	val = sl.value()
	bEna = 0
	if cb.isChecked() == True :
		bEna = 1
	#print("Dimmer slider val changed " + str(id) + " v:"  + str(val))
	#self.sendQuery(SET_PROP, [0x2e, id, val])
	#self.sendQuery(SET_PROP, [0x34, pNum, bEna, val])
	sendDimmerState(self, pNum, bEna, val)
	
def sendDimmerState(self, pNum, bEna, val):
	self.sendQuery(SET_PROP, [0x34, pNum, bEna, val])

def handleDimmerGetVal(self, id):
	self.sendQuery(GET_PROP, [0x32, id])
	self.sendQuery(GET_PROP, [0x33, id])
	self.sendQuery(GET_PROP, [0x34, id])
	self.sendQuery(GET_PROP, [0x35, id])
	self.sendQuery(GET_PROP, [0x36, id])
    
def handleDimmerGetVals(self):
	for i in range(5): 
		handleDimmerGetVal(self, i)
	
def handlePwmDimmerSetRange(self, cmdNum, portNum, minMax, maxMin, maxMax) :
	
	dlg = QDialog(self)
	dlg.setWindowTitle("set range ch " + str(portNum))
	
	vLayout = QVBoxLayout()
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("chNum"))
	
	comboPortNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,6) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboPortNum.addItems(scrpitNumArr)	
	comboPortNum.setCurrentIndex(portNum-1)	
	layout1.addWidget(comboPortNum)
	layout1.addStretch(1)		
	vLayout.addLayout(layout1)
	
	slMin = QSlider(Qt.Orientation.Horizontal, dlg)
	slMin.setMaximum(minMax)
	
	slMax = QSlider(Qt.Orientation.Horizontal, dlg)
	slMax.setMinimum(maxMin)
	slMax.setMaximum(maxMax)
	
	minL = QLabel("0")
	maxL = QLabel("0")
	slMin.valueChanged.connect(lambda v: minL.setText(str(v)))
	slMax.valueChanged.connect(lambda v: maxL.setText(str(v)))
	slMax.setValue(maxMax)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("min"))
	layout1.addWidget(minL)
	layout1.addWidget(slMin)	
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("max"))
	layout1.addWidget(maxL)
	layout1.addWidget(slMax)		
	vLayout.addLayout(layout1)

	cancelBut = QPushButton("close")
	cancelBut.clicked.connect(dlg.close)
	writeBut = QPushButton("write")
	#writeBut.clicked.connect(dlg.close)
	#pNum = int(ds18b20.comboPortNum.currentText())
	writeBut.clicked.connect(lambda: self.sendQuery(SET_PROP, [cmdNum, int(comboPortNum.currentText()), slMin.value(), slMax.value()]))
			
	layout1 = QHBoxLayout()
	layout1.addWidget(cancelBut)
	layout1.addWidget(writeBut)		
	vLayout.addLayout(layout1)
	
	dlg.setLayout(vLayout)
	dlg.exec()
	
	#min = self.ui.horizontalSliderPWMMin.value()
	#max = self.ui.horizontalSliderPWMMax.value()
	#print("min:"+str(min) + " max:"+str(max))
	#self.sendQuery(SET_PROP, [cmdNum, portNum, min, max])

def parseDimmerEvent(sqi):
	#print("dimmer event " + hex(sqi.cmd))
	pass

def parseDimmerGet(self, sqi):
	#print("dimmer get resp " + hex(sqi.cmd))
	if sqi.cmd == 0x32:
		chId =  int.from_bytes(sqi.body[0], byteorder='little')
		min = int.from_bytes(sqi.body[1], 'little')
		max = int.from_bytes(sqi.body[2], 'little')
		#chId = ch -1
		if(chId< 5):
			#print("dimmer get min max " + str(min) + " " + str(max))
			tw = QTableWidgetItem(str(min))  #
			tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
			self.ui.tableWidgetDimmer.setItem(chId, 4, tw)
			
			tw = QTableWidgetItem(str(max))  #
			tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
			self.ui.tableWidgetDimmer.setItem(chId, 5, tw)

	elif sqi.cmd == 0x33:
		chId =  int.from_bytes(sqi.body[0], byteorder='little')
		ena = int.from_bytes(sqi.body[1], byteorder='little')
		#chId = ch -1
		if chId < len(dimmerEnaBtList) :
			dimmerEnaBtList[chId].setChecked(ena==1)
	elif sqi.cmd == 0x34:
		chId =  int.from_bytes(sqi.body[0], byteorder='little')
		#ena = int.from_bytes(sqi.body[1], byteorder='little')
		val = int.from_bytes(sqi.body[1], byteorder='little')
		#print("dimmer get message ch: " + str(ch) + " ena:" + str(ena) + " val:" + str(val))
		
		#chId = ch -1
		if chId < len(dimmerSliderList) :
			dimmerSliderList[chId].setValue(val)
	elif sqi.cmd == 0x35:
		chId =  int.from_bytes(sqi.body[0], byteorder='little')
		lu = int.from_bytes(sqi.body[1], 'little')*100
		#chId = ch -1
		if(chId< 5):
			#print("dimmer lu " + str(lu) )
			lughtUpTwList[chId].setText(str(lu))
			
	elif sqi.cmd == 0x36:
		chId = int.from_bytes(sqi.body[0], 'little')
		state = int.from_bytes(sqi.body[1], 'little')==1
		#chId = ch -1
		if chId < len(dimmerDiscrBtList) :
			dimmerDiscrBtList[chId].setChecked(state==1)