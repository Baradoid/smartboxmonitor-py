from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget
from PyQt6.QtWidgets import QTableWidget, QTableWidgetItem, QAbstractItemView
from PyQt6.QtGui import QColor, QBrush, QTextDocument, QTextCursor

tableWidget = None
def initTableWidgetCmds(self):
	self.tableWidget = QTableWidget(256, 4, self)
	self.tableWidget.verticalHeader().setVisible(False)
	self.tableWidget.setSelectionMode(QAbstractItemView.SelectionMode.NoSelection)
	self.tableWidget.setHorizontalHeaderLabels(["dec", "hex", "type", "description"])
	for ri in range(0,255) :
		twi = QTableWidgetItem(str(ri))
		twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
		#twi.setEditable(True)
		#twi.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
		#twi.lineEdit().setReadOnly(True)		
		#comboPortNum1.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
		#comboPortNum1.lineEdit().setReadOnly(True)		
		twi.setFlags(twi.flags() &  ~QtCore.Qt.ItemFlag.ItemIsEditable);
		self.tableWidget.setItem(ri, 0, twi)
		
		twi = QTableWidgetItem(hex(ri))
		twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
		twi.setFlags(twi.flags() &  ~QtCore.Qt.ItemFlag.ItemIsEditable);
		self.tableWidget.setItem(ri, 1, twi)

		if ri < 31:
			twi = QTableWidgetItem("XController")
			twi.setBackground(QColor(226,239,217))
		elif ri < 46:
			twi = QTableWidgetItem("SensorDS8")
			twi.setBackground(QColor(254,242,203))
		elif ri < 61:
			twi = QTableWidgetItem("Dimmer")
			twi.setBackground(QColor(222,232,246))
		elif ri < 76:
			twi = QTableWidgetItem("PWM")
			twi.setBackground(QColor(180,198,231))
		elif ri < 101:
			twi = QTableWidgetItem("IRGate")
			twi.setBackground(QColor(248,223,253))
		elif ri < 116:
			twi = QTableWidgetItem("GSM")
			twi.setBackground(QColor(213,251,250))
		elif ri >=240:
			twi = QTableWidgetItem("Общая")
			twi.setBackground(QColor(253,216,210))

		else :
			twi = QTableWidgetItem("")
		
		twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
		twi.setFlags(twi.flags() &  ~QtCore.Qt.ItemFlag.ItemIsEditable);
		self.tableWidget.setItem(ri, 2, twi)
		
		twi = None
		
		if ri == 1 : twi = QTableWidgetItem("Настройка входа как DISCRETE")
		if ri == 2 : twi = QTableWidgetItem("Настройка входа как ADC")
		if ri == 3 : twi = QTableWidgetItem("Настройка входа как DS18B20")
		if ri == 4 : twi = QTableWidgetItem("Настройка входа как SHT")
		if ri == 5 : twi = QTableWidgetItem("Настройка выхода как RELAY")
		if ri == 6 : twi = QTableWidgetItem("test RELAY")
		if ri == 7 : twi = QTableWidgetItem("получить состояние всех реле")
		if ri == 10 : twi = QTableWidgetItem("настройка входа как iButton")
		if ri == 11 : twi = QTableWidgetItem("Запустить процедуру записи ключа iButton со считывателя в EEPROM")
		if ri == 12 : twi = QTableWidgetItem("Очистить iButton в EEPROM")
		if ri == 13 : twi = QTableWidgetItem("Записать или прочитать ключ iButton в EEPROM")
		if ri == 15 : twi = QTableWidgetItem("Прочитать-сохранить зависимость в EEPROM")
		if ri == 16 : twi = QTableWidgetItem("Очистить выбранную зависимость в EEPROM")
		if ri == 17 : twi = QTableWidgetItem("Очистить все зависимости в EEPROM")
		if ri == 18 : twi = QTableWidgetItem("Установить все входы как DISCRETE")
		
		if ri == 31 : twi = QTableWidgetItem("Разрешение датчиков")
		if ri == 32 : twi = QTableWidgetItem("Таймаут температуры для события (для всех каналов)")
		if ri == 33 : twi = QTableWidgetItem("Текущая температура")
		if ri == 34 : twi = QTableWidgetItem("Установить порог температуры (граница срабатывания реле)")
		if ri == 35 : twi = QTableWidgetItem("Задержка переключения реле после перехода порога")
		if ri == 36 : twi = QTableWidgetItem("Вкл.Выкл каналов")
		if ri == 37 : twi = QTableWidgetItem("Максимальное значение работающих одновременно каналов")
		if ri == 38 : twi = QTableWidgetItem("Максимальный порог температуры")
		if ri == 39 : twi = QTableWidgetItem("Тест RELAY (поочередно включает каждое реле)")
		if ri == 40 : twi = QTableWidgetItem("Текущее состояние реле")
		
		if ri == 48 : twi = QTableWidgetItem("Сохранить значение яркости в EEPROM")
		if ri == 49 : twi = QTableWidgetItem("Задержка в сек. перед отправкой текущих значений")
		if ri == 50 : twi = QTableWidgetItem("Минимальное и максимальное значение яркости")
		if ri == 52 : twi = QTableWidgetItem("Включить, выключить, значение яркости канала")

		if ri == 63 : twi = QTableWidgetItem("Сохранить значение яркости в EEPROM")
		if ri == 64 : twi = QTableWidgetItem("Задержка в сек. перед отправкой текущих значений")
		if ri == 65 : twi = QTableWidgetItem("Минимальное и максимальное значение яркости")
		if ri == 67 : twi = QTableWidgetItem("Включить, выключить, значение яркости канала")


		if ri == 248 : twi = QTableWidgetItem("WiFi watchdog")
		if ri == 249 : twi = QTableWidgetItem("Разрешение датчика DS18B20")
		if ri == 250 : twi = QTableWidgetItem("Температура контроллера")
		if ri == 251 : twi = QTableWidgetItem("Серийный номер контроллера")
		if ri == 252 : twi = QTableWidgetItem("RTC (Время) контроллера")
		if ri == 253 : twi = QTableWidgetItem("Версия прошивки контроллера, дата, тип контроллера")
		if ri == 254 : twi = QTableWidgetItem("UPTIME контроллера")
		if ri == 255 : twi = QTableWidgetItem("Перезапустить контроллер")

		
		if twi != None :
			twi.setFlags(twi.flags() &  ~QtCore.Qt.ItemFlag.ItemIsEditable);
			self.tableWidget.setItem(ri, 3, twi)
		
		
	
	self.tableWidget.resizeColumnsToContents()
	self.tableWidget.hide()
	self.ui.widget.layout().addWidget(self.tableWidget)
	
def handleShiftIface(self):
	#print("lala")
	geom = self.geometry() 
	
	#print(self.tableWidget.geometry())
	if(self.ui.pushButtonIfaceShift.text() == ">") :
		self.ui.pushButtonIfaceShift.setText("<")
		#self.resize(self.screenShape.width(), self.screenShape.height())
		self.setGeometry(geom.left(), geom.top(), geom.width()+450, geom.height())
		self.tableWidget.show()
	else :
		self.ui.pushButtonIfaceShift.setText(">")			
		#self.ui.widget.layout().removeWidget(self.tableWidget)
		self.tableWidget.hide()
		self.setGeometry(geom.left(), geom.top(), geom.width()-450, geom.height())		
	
def handleIfaceColor(self, bColor) :
	if bColor :
		self.ui.groupBoxDimmer.setStyleSheet("background-color: rgb(222,232,246)")
		self.ui.groupBoxPWM.setStyleSheet("background-color: rgb(180,198,231)")
		self.ui.widgetCommonCmd.setStyleSheet("background-color: rgb(253,216,210)")
		self.ui.groupBoxRelay.setStyleSheet("background-color: rgb(226,239,217)")
		#self.ui.groupBoxDs19b20.setStyleSheet("background-color: rgb(254,242,203)")
		self.ui.groupBoxDiscrete.setStyleSheet("background-color: rgb(226,239,217)")
		self.ui.groupBoxScripts.setStyleSheet("background-color: rgb(226,239,217)")
		self.ui.groupBoxDht.setStyleSheet("background-color: rgb(226,239,217)")
	else :
		self.ui.groupBoxDimmer.setStyleSheet("")
		self.ui.groupBoxPWM.setStyleSheet("")
		self.ui.widgetCommonCmd.setStyleSheet("")
		self.ui.groupBoxRelay.setStyleSheet("")
		#self.ui.groupBoxDs19b20.setStyleSheet("")
		self.ui.groupBoxDiscrete.setStyleSheet("")
		self.ui.groupBoxScripts.setStyleSheet("")
		self.ui.groupBoxDht.setStyleSheet("")
	