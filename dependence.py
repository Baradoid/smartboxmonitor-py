from PyQt6 import QtWidgets, QtCore, QtGui
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget
from PyQt6.QtWidgets import QTableWidget, QTableWidgetItem, QAbstractItemView
from PyQt6.QtWidgets import QButtonGroup, QGroupBox

from utils import callback_factory
from utils import *
import portMode

DEPENDENS_COUNT = 100

PORT_COUNT = portMode.INPUT_PORT_COUNT

dlg = None
butWrite = None
comboDhtPortNum = None
periodComboBox = None
minBorderU1LineEdit = None
minBorderU2LineEdit = None
dataPeriodLineEdit = None

parent = None
comboPortScriptNum = None
comboPortNum1 = None
comboActionPort = None
depQuerySended = False
depNum = 0
currentDepNum = 0

portMode = [0 for i in range(portMode.INPUT_PORT_COUNT)]


comboAction = None
comboReason = None
comboDiscrReasonProp = None
reasonADCThr = None
reasonLineEditThr = None
reasonPushCountSpinBox = None

addScriptWindow = None
comboPortNum1 = None
#comboReason = None
comboActionPort = None
#comboAction = None
comboPortScriptNum = None
lineEditTimeOut = None

actionDimSpinVal = None
actionPwmSpinVal = None

parent = None
tableWidgetDepTable = None

reasonLayout = None
actionLayout = None

reasonLblMin = None
reasonLblMax = None

reasonStr = ["0 (off)","1 (ADC<)","2 (ADC>)","3 (DS18b20 t<)","4 (DS18b20 t>)","5 (DHT t<)","6 (DHT t>)","7 (DHT rh<)","8 (DHT rh>)","9 (DISCRETE)","10 (DISCRETE push count)","11 (DISCRETE push duration)"]
reasonPropStr = ["rise 1", "fall 2", "both 3"]
#actionStr = ["0 ", "1 (on)", "2 (off)", "3 (inv)"]
actionStr = ["0 (off)", "1 (Rele)", "2 (DimmerEna)", "3 (DimmerBright)", "4 (PWMEna)", "5 (PWMBright)"]
actionStr += ["6 (DaliUnitEna)", "7 (DaliUnitBright)", "8 (DaliGroupEna)", "9 (DaliGroupBright)"]
releActionPropStr = ["0 (off)", "1 (on)", "2 (inv)"]
#dimPwmActionPropStr = ["0 (off)", "1 (on)"]

discrPushMinMaxWdg = None

def init(prnt):
	global parent, tableWidgetDepTable
	global comboReason
	parent = prnt
	tableWidgetDepTable = prnt.ui.tableWidgetDepTable
	tableWidgetDepTable.setRowCount(DEPENDENS_COUNT)
	tableWidgetDepTable.setColumnCount(12)
	combo_box_options = ["Option 1","Option 2","Option 3"]
	comboReason = QtWidgets.QComboBox()
	comboReason.addItems(["rise","fall"])
		
	for i in range(0, 10) :
		comboPortNum1 = QtWidgets.QComboBox()
		comboPortNum2 = QtWidgets.QComboBox()
		portList = ["1","2","3","4","5","6","7","8"]
		comboPortNum1.addItems(portList)
		#comboPortNum1.setEditable(True)
		#comboPortNum1.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
		#comboPortNum1.lineEdit().setReadOnly(True)			
		comboPortNum2.addItems(portList)
		#comboPortNum2.setEditable(True)
		#comboPortNum2.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
		#comboPortNum2.lineEdit().setReadOnly(True)

		
		#print(ll)
		#setAlignment(QtCore.Qt.AlignCenter)

		#self.ui.tableWidgetScriptTable.setCellWidget(i, 0, comboPortNum1)
		#self.ui.tableWidgetScriptTable.setCellWidget(i, 2, comboPortNum2)

		#self.ui.tableWidgetScriptTable.setCellWidget(i,1,comboReason)
		comboAction = QtWidgets.QComboBox()
		comboAction.addItems(["on", "off", "inv"])
		#self.ui.tableWidgetScriptTable.setCellWidget(i,3,comboAction)
		twi = QTableWidgetItem("3")
		twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
		#self.ui.tableWidgetScriptTable.setItem(i,4, twi)
	tableWidgetDepTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
	#self.ui.tableWidgetScriptTable.resizeColumnsToContents()
	
	for i in range(tableWidgetDepTable.rowCount()) :
		btn = QPushButton("set")
		#btn.setMaximumWidth(40)
		btn.clicked.connect(callback_factory(handleShowSetDependenceDlg, i))
		tableWidgetDepTable.setCellWidget(i, 11, btn)
		
		btn = QPushButton("вкл")
		btn.clicked.connect(callback_factory(handleWriteDepEna, i, 1))
		tableWidgetDepTable.setCellWidget(i, 1, btn)
		
		btn = QPushButton("выкл")
		btn.clicked.connect(callback_factory(handleWriteDepEna, i, 0))
		tableWidgetDepTable.setCellWidget(i, 2, btn)
	
	tableWidgetDepTable.verticalHeader().setDefaultSectionSize(tableWidgetDepTable.verticalHeader().minimumSectionSize())
	
	tableWidgetDepTable.horizontalHeaderItem(0).setToolTip("состояние зависимости");
	tableWidgetDepTable.horizontalHeaderItem(3).setToolTip("порт причины");
	tableWidgetDepTable.horizontalHeaderItem(4).setToolTip("код причины");
	tableWidgetDepTable.horizontalHeaderItem(5).setToolTip("порог причины");
	tableWidgetDepTable.horizontalHeaderItem(6).setToolTip("порт действия");
	tableWidgetDepTable.horizontalHeaderItem(7).setToolTip("код действия");
	tableWidgetDepTable.horizontalHeaderItem(8).setToolTip("значение действия");
	
	#tw = QTableWidgetItem("test")  #
	#tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	#tw.setBackground(Qt.green)
	#tableWidgetDepTable.setItem(1, 0, tw)	
	
	#tw = QTableWidgetItem("test")  #
	#tw.setTextAlignment(Qt.AlignCenter)
	#tw.setBackground(Qt.lightGray)
	#tableWidgetDepTable.setItem(2, 0, tw)	

	#tw = QTableWidgetItem("test")  #
	#tw.setTextAlignment(Qt.AlignCenter)
	#tw.setBackground(Qt.gray)
	#tableWidgetDepTable.setItem(3, 0, tw)	

	#tw = QTableWidgetItem("test")  #
	#tw.setTextAlignment(Qt.AlignCenter)
	#tw.setBackground(Qt.darkGray)
	#tableWidgetDepTable.setItem(4, 0, tw)	
	
	tableWidgetDepTable.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Fixed)
	tableWidgetDepTable.setColumnWidth(0,40)
	tableWidgetDepTable.setColumnWidth(3,20)
	tableWidgetDepTable.setColumnWidth(4,120)
	tableWidgetDepTable.setColumnWidth(5,40)
	tableWidgetDepTable.setColumnWidth(7,100)
	tableWidgetDepTable.setColumnWidth(10,60)
	tableWidgetDepTable.setColumnWidth(11,60)

def handleWriteDepEna(pInd, e):
	parent.sendQuery(SET_PROP, [0x10, pInd, e])

sendAddScriptCB = None
def addSendAddScriptCB(cb):
	global sendAddScriptCB
	sendAddScriptCB = cb

createReadDepMsgCB = None
def addCreateReadDepMsgCB(cb):
	global createReadDepMsgCB
	createReadDepMsgCB = cb
	
def handleCreateAddScriptMessage(depId) :
	global depQuerySended, depNum, comboPortNum1, comboReason
	global sendAddScriptCB
	global lineEditTimeOut
	global reasonStr
	depNum = depId
	if depId == -1:
		depNum = int(comboPortScriptNum.currentText())
		
	try:
		portInd = int(comboPortNum1.currentText())-1
		reasonCode = 0 #int(self.comboReason.currentText())
		reasonId = comboReason.currentIndex()
		
		#reasonCode = int(comboReason.currentText())
		reasonCodeInd = comboReason.currentIndex()	
		actionPort = int(comboActionPort.currentText())-1
		actionCode = comboAction.currentText()
		actionCodeInd = comboAction.currentIndex()	
		actionTimeout = lineEditTimeOut.value()
		tag  = lineEditTag.value()
		#print("actionTimeout " + str(actionTimeout ))
		'''if actionCode == "on" :
			actionCode = 1
		elif actionCode == "off" :
			actionCode = 0
		elif actionCode == "inv" :
			actionCode = 2
		elif actionCode == "timeout" :
			actionCode = int(to)'''
		reasonCode = reasonCodeInd
		actionCode = actionCodeInd  #if actionCodeInd < 3  else to
		#print("reasonCode:" + str(reasonCode))
		reasonThr = reasonADCThr.value()
		
		if reasonCodeInd >= 12:
			reasonCode = reasonCodeInd+10-12 			
		elif reasonCodeInd == 11:
			reasonCode = 11
			reasonThr = ((reasonRangeMin.value()&0xff)<<8) | (reasonRangeMax.value()&0xff)
		elif reasonCodeInd == 10:
			reasonCode = 10
			reasonThr = reasonPushCountSpinBox.value()
		elif reasonCode >= 9 :
			reasonThr = reasonCode - 9 + 1
			reasonCode = 9
			reasonThr = comboDiscrReasonProp.currentIndex()+1
			
		
		actionProp = lineEditActionProp.value()
		if actionCode == 1 :
			actionProp = releComboActionProp.currentIndex()
		if (actionCode == 2) or (actionCode == 4) :
			actionProp = dimPwmComboActionProp.currentIndex()
		if (actionCode == 3):
			actionProp = actionDimSpinVal.value()
		if (actionCode == 5) :
			actionProp = actionPwmSpinVal.value()

		prStr = "depNum:" + str(depNum) + " create msg port:" + str(portInd) + " reas:" + str(reasonCode) + " reas thr:" + hex(reasonThr) + " min:" + str(reasonRangeMin.value()) + " max:" + str(reasonRangeMax.value())
		prStr += "  actionPort:" + str(actionPort) + " actionCode:" + str(actionCode) + " actionProp:" + str(actionProp)
		#reasonThr = 
		print(prStr)
		if sendAddScriptCB != None :
			sendAddScriptCB(depNum, portInd, reasonCode, reasonThr, actionPort, actionCode, actionProp, actionTimeout, tag)
		#self.sendQuery(SET_PROP, [0x0f, depNum, portNum, reasonCode, actionPort, (actionCode>>8)&0xff, actionCode&0xff,])
	except Exception as e:
		print("one or more val not set: " + str(e))

def handleCreateReadDepMessage(self) :
	global depQuerySended, depNum
	depNum = int(comboPortScriptNum.currentText())
	prStr = "read depNum:" + str(currentDepNum) 		
	print(prStr)
	depQuerySended = True
	#parent.sendQuery(GET_PROP, [0x0f, depNum])
	if createReadDepMsgCB != None :
		createReadDepMsgCB(currentDepNum)

reasonPortArr = [0 for i in range(DEPENDENS_COUNT)]
reasonCodeArr = [0 for i in range(DEPENDENS_COUNT)]
reasonThreshArr=[0 for i in range(DEPENDENS_COUNT)]
actionPortArr = [0 for i in range(DEPENDENS_COUNT)]
actionCodeArr = [0 for i in range(DEPENDENS_COUNT)]
actionPropArr = [0 for i in range(DEPENDENS_COUNT)]
actionTimeOutArr = [0 for i in range(DEPENDENS_COUNT)]
tagArr =  [0 for i in range(DEPENDENS_COUNT)]

def setDependence(self, depInd, reasonPortInd,reasonCode,reasonThr, actionPortInd,actionCode,actionProp, actionTimeout, bEna, tag):
	global reasonStr
	global actionPropArr
	#print("setDependence: " + str(actionCode) + " prop:"+ str(actionProp) + " ena:"+str(bEna))
	
	reasonPortNum = reasonPortInd + 1
	#tw = QTableWidgetItem(str(bEna))  #
	#tw.setTextAlignment(Qt.AlignCenter)
	#self.ui.tableWidgetDepTable.setItem(depNum-1, 0, tw)		

	#tableWidgetDepTable.horizontalHeader().
	#QTableWidgetItem *col1 = new QTableWidgetItem("Column 1");
	#col1->setForeground(Qt::white);
	#col1->setBackground(QColor("lightblue"));
	#col1->setFont(QFont("arial", 12));
	#tableWidget->setHorizontalHeaderItem(0, col1); // tableWidget->setVerticalHeaderItem(0, col1);
	tw = QTableWidgetItem(str(depInd))  #
	#tw.setForeground(Qt.green)
	tw.setBackground(Qt.GlobalColor.green if bEna else Qt.GlobalColor.lightGray)
	#tw.setBackground(QColor(1*50, 1*30, 200-1*40))
	
	tableWidgetDepTable.setVerticalHeaderItem(depInd, tw)	
	
	tw = QTableWidgetItem("> E <" if bEna else "D")  #	
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	#tw.setMaximumWidth(30)
	#if bEna:
		#tw.setBackground(Qt.darkGreen)
	tableWidgetDepTable.setItem(depInd, 0, tw)	
	
	tw = QTableWidgetItem(str(reasonPortNum))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 3, tw)		
	
	reasonStrStr = reasonStr[reasonCode] if reasonCode < 12 else "unk" #"to:"+str(actionCode/10.)+"s"
	tw = QTableWidgetItem(reasonStrStr)  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 4, tw)
	
	tw = QTableWidgetItem(str(reasonThr))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 5, tw)
	
	tw = QTableWidgetItem(str(actionPortInd+1))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 6, tw)
	
	
	#actionStrStr = reasonStr[actionCode] if actionCode < 11 else "unk" #"to:"+str(actionCode/10.)+"s"
	actionStrStr = actionStr[actionCode] if actionCode < len(actionStr) else "unk" #"to:"+str(actionCode/10.)+"s"
	
	tw = QTableWidgetItem(actionStrStr)  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 7, tw)

	tw = QTableWidgetItem(str(actionProp))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 8, tw)
	
	tw = QTableWidgetItem(str(actionTimeout))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 9, tw)
	
	tw = QTableWidgetItem(str(tag))  #
	tw.setTextAlignment(Qt.AlignmentFlag.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 10, tw)
	
	#self.ui.tableWidgetDepTable.resizeRowToContents(depNum-1)
	#self.ui.tableWidgetDepTable.resizeColumnsToContents()
	
	reasonPortArr[depInd] = reasonPortInd
	reasonCodeArr[depInd] = reasonCode
	actionPortArr[depInd] = actionPortInd
	actionCodeArr[depInd] = actionCode
	actionTimeOutArr[depInd] = actionTimeout
	reasonThreshArr[depInd] = reasonThr
	actionPropArr[depInd] = actionProp
	tagArr[depInd] = tag
	#updateComboAction()
	#updateComboReason()

	
def setDependenceEna(self, depInd, bEna):
	tw = QTableWidgetItem(str(bEna))  #
	tw.setTextAlignment(Qt.AlignCenter)
	self.ui.tableWidgetDepTable.setItem(depInd, 0, tw)		
	
def setPortMode(portId, mode):
	global portMode
	portMode[portId] = mode
	#print("deps " + str(portId) + "  mode: " + str(portMode[portId]))
			
def handleCurrentPortChanged(pId):
	global portMode, comboAction, comboReason
	#print(str(pId))
	reasonList = ["0", "1", "2", "3", "4"]
	depId = pId
	'''if portMode[pId]==0 : 
		modeStr = "D"
		reasonList = ["rise", "fall", "ever"]
	elif portMode[pId]==1:
		modeStr = "DS"
		reasonList = ["toA", "toB", "toC"]
	elif portMode[pId]==2: 
		modeStr = "IB"
		reasonList = ["known", "unknown", "ever"]
	elif portMode[pId]==3: 
		modeStr = "DHT"
		reasonList = ["toA", "toB", "toC", "16", "32", "64"]
	elif portMode[pId]==4: 
		modeStr = "ADC"
		reasonList = ["toA", "toB", "toC"]'''
	
	#comboReason.clear()
	#comboReason.addItems(reasonList)
	
	reasonPort = reasonPortArr[depId]
	reasonCode = reasonCodeArr[depId]
	actionPort = actionPortArr[depId]
	actionCode = actionCodeArr[depId]
	
	comboPortNum1.setCurrentIndex(reasonPort-1)
	comboReason.setCurrentIndex(reasonCode)
	comboActionPort.setCurrentIndex(actionPort-1)
	comboAction.setCurrentIndex(actionCode)
	

def handleShowSetDependenceDlg(depId=-1) :
	#print("set deps:" + str(depId))
	global comboPortScriptNum, dlg, comboPortNum1, comboActionPort
	global PORT_COUNT
	global comboAction, comboReason, comboDiscrReasonProp
	global lineEditTimeOut
	global reasonStr, actionStr
	global reasonADCThr
	global currentDepNum
	global lineEditTag
	global reasonRangeMin, reasonRangeMax
	global lineEditActionProp
	global releComboActionProp
	global dimPwmComboActionProp
	global actionLayout
	global actionDimSpinVal, actionPwmSpinVal
	global reasonLayout
	global reasonLineEditThr
	global discrPushMinMaxWdg
	global reasonLblMin, reasonLblMax
	global reasonPushCountSpinBox
	global actionPropArr
	
	currentDepNum = depId
	dlg = QDialog(parent)
	dlg.setWindowTitle("Конструктор зависимости")
	dlg.setWindowFlag(Qt.WindowType.WindowContextHelpButtonHint, False)
	#self.setModal(True)
	
	vLayout = QVBoxLayout()
	comboReason = QtWidgets.QComboBox(dlg)	
	comboReason.clear()
	#print("!!!! deb:" + actionStr)
	comboReason.addItems(reasonStr)
	#comboReason.setAlignment(Qt.AlignHCenter)
	comboReason.setMaximumWidth(180)
	
	comboDiscrReasonProp = QtWidgets.QComboBox()	
	comboDiscrReasonProp.addItems(reasonPropStr)
	#comboReason.setAlignment(Qt.AlignHCenter)
	comboDiscrReasonProp.setMaximumWidth(50)
	
	reasonPort = 0
	reasonCode = 0
	actionPort = 0
	actionCode = 0
	actionTO = 0
	tag = 0
	
	#if depId != -1:
	#	depId = 0
	
	reasonPort = reasonPortArr[depId]
	reasonCode = reasonCodeArr[depId]
	reasonThr = reasonThreshArr[depId]
	actionPort = actionPortArr[depId]
	actionCode = actionCodeArr[depId]
	actionProp = actionPropArr[depId]
	actionTO = actionTimeOutArr[depId]
	tag = tagArr[depId]
	print("deps:rp " + str(reasonPort) + " rc: " + str(reasonCode) + " rt: " + str(reasonThr))
	print("deps:ap: " + str(actionPort) + " ac: " + str(actionCode) + " ap: " + str(actionProp) + " at"+ str(actionTO) )

	comboReason.setCurrentIndex(reasonCode)
	comboReason.currentIndexChanged.connect(updateComboReason)
	
	reasonADCThr = QtWidgets.QSpinBox()
	reasonADCThr.setRange(-32768, 32767);
	reasonADCThr.setValue(reasonThr)
	reasonADCThr.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	reasonADCThr.setMaximumWidth(80)
	
	reasonLblMin = QLabel("min,ms*100")
	reasonRangeMin = QtWidgets.QSpinBox()
	reasonRangeMin.setRange(1, 100);
	reasonRangeMin.setValue(reasonThr)
	reasonRangeMin.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	
	reasonLblMax = QLabel("max,ms*100")
	reasonRangeMax = QtWidgets.QSpinBox()
	reasonRangeMax.setRange(1, 100);
	reasonRangeMax.setValue(reasonThr)
	reasonRangeMax.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	
	
	reasonLineEditThr = QtWidgets.QLineEdit()
	#reasonLineEditThr.setInputMask("[0-4] \. \d")
	reasonLineEditThr.setMaximumWidth(120)
	reasonLineEditThr.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	
	
	reasonPushCountSpinBox = QtWidgets.QSpinBox()
	reasonPushCountSpinBox.setRange(1, 10);
	reasonPushCountSpinBox.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	reasonPushCountSpinBox.setMaximumWidth(80)
	#print("rp:" + str(reasonPort) +  "  rc:" + str(reasonCode) + "  rt:" + str(reasonThr)  +  " ap:" + str(actionPort)+  "  ac:" + str(actionCode))
	
	comboAction = QtWidgets.QComboBox()
	comboAction.addItems(actionStr)
	comboAction.setCurrentIndex(actionCode)
	comboAction.currentIndexChanged.connect(updateComboAction)
		
	releComboActionProp = QtWidgets.QComboBox()
	releComboActionProp.addItems(releActionPropStr)
	releComboActionProp.setCurrentIndex(actionProp)
	
	dimPwmComboActionProp = QtWidgets.QComboBox()
	dimPwmComboActionProp.addItems(releActionPropStr)
	dimPwmComboActionProp.setCurrentIndex(actionCode)
	
	comboPortNum1 = QtWidgets.QComboBox()
	comboActionPort = QtWidgets.QComboBox()		
	portList = [0]*PORT_COUNT
	for i in range(PORT_COUNT):
		portList[i] = str(i+1)
	comboPortNum1.addItems(portList)			
	
	comboPortNum1.setCurrentIndex(reasonPort)
	#comboPortNum1.currentIndexChanged.connect(handleCurrentPortChanged)
	
	comboActionPort.addItems(portList)
	comboActionPort.setCurrentIndex(actionPort)
	
	comboPortScriptNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,60) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboPortScriptNum.addItems(scrpitNumArr)	
	comboPortScriptNum.currentIndexChanged.connect(handleCurrentPortChanged)
	
	actionDimSpinVal =  QtWidgets.QSpinBox()
	actionDimSpinVal.setRange(0, 100)
	actionDimSpinVal.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	actionDimSpinVal.setMaximumWidth(60)
	actionPwmSpinVal = QtWidgets.QSpinBox()
	actionPwmSpinVal.setRange(0, 255)
	actionPwmSpinVal.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	actionPwmSpinVal.setMaximumWidth(60)
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	lineEditTimeOut = QSpinBox(dlg)
	lineEditTimeOut.setMinimum(0)
	lineEditTimeOut.setMaximum(0xffff)
	lineEditTimeOut.setValue(actionTO)
	lineEditTimeOut.setMaximumWidth(60)
	lineEditTimeOut.setSingleStep(100)
		
	lineEditTag = QSpinBox(dlg)
	lineEditTag.setMinimum(0)
	lineEditTag.setMaximum(0xffff)
	lineEditTag.setValue(tag)
	
	lineEditActionProp= QSpinBox()
	lineEditActionProp.setMinimum(0)
	lineEditActionProp.setMaximum(0xffff)
	lineEditActionProp.setValue(tag)
	lineEditActionProp.setMaximumWidth(60)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер зависимости:"))
	if depId == -1 :
		layout1.addWidget(comboPortScriptNum)			
		handleCurrentPortChanged(0)
	else :
		layout1.addWidget(QLabel(str(depId+1)))
		#handleCurrentPortChanged(depId)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	
	layout = QHBoxLayout()
	layout.addWidget(QLabel("Порт,номер:"))
	layout.addWidget(comboPortNum1)
	layout.addWidget(QLabel("код:"))
	layout.addWidget(comboReason)
	layout.addWidget(QLabel("Значение:"))
	#layout.addWidget(reasonThreshold)
	layout.addWidget(reasonPushCountSpinBox)
	layout.addWidget(comboDiscrReasonProp)
	layout.addWidget(reasonLineEditThr)
	layout.addWidget(reasonLblMin)
	layout.addWidget(reasonRangeMin)
	layout.addWidget(reasonLblMax)
	layout.addWidget(reasonRangeMax)	
	layout.addStretch(1)
	layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignLeft)
	#vLayout.addLayout(layout)
	

	gb = QGroupBox("Причина")
	gbHLayout = layout
	gb.setLayout(gbHLayout)
	#gbHLayout.addLayout(layout)
	reasonLayout = gbHLayout
	#gbHLayout.addLayout(reasonLayout)
	
	#layout = QHBoxLayout()	
	
	#layout.addWidget(QLabel("min,ms*100:"))
	#layout.addWidget(reasonRangeMin)
	#layout.addWidget(QLabel("max,ms*100:"))
	#layout.addWidget(reasonRangeMax)
	#layout.addStretch(1)
	#gbVLayout.addLayout(layout)
		
	vLayout.addWidget(gb)
	
	
	#layout = QHBoxLayout()
	#line = QtWidgets.QFrame()
	#line.setFrameShape(QtWidgets.QFrame.HLine)
	#line.setFrameShadow(QtWidgets.QFrame.Sunken)
	#line.setObjectName("line")
	#layout.addWidget(line)
	#vLayout.addLayout(layout)
	
	gb = QGroupBox("Действие")
	gbVLayout = QVBoxLayout()
	gb.setLayout(gbVLayout)
	vLayout.addWidget(gb)	
	
	layout = QHBoxLayout()
	layout.addWidget(QLabel("код:"))
	layout.addWidget(comboAction)
	layout.addWidget(QLabel("Порт,номер:"))
	layout.addWidget(comboActionPort)
	
	layout.addWidget(QLabel("значение:"))
	#layout.addWidget(lineEditActionProp)	
	layout.addStretch(1)
	#vLayout.addLayout(layout)
	actionLayout = layout
	gbVLayout.addLayout(layout)
	
	layout.addWidget(releComboActionProp)
	layout.addWidget(lineEditActionProp)
	layout.addWidget(dimPwmComboActionProp)
	layout.addWidget(actionDimSpinVal)
	layout.addWidget(actionPwmSpinVal)
	
	layout = QHBoxLayout()
	#layout.addWidget(QLabel("реле скд:"))
	#layout.addWidget(releComboActionProp)
	#layout.addWidget(QLabel("дим/шим скд:"))
	#layout.addWidget(dimPwmComboActionProp)
	
	layout.addWidget(QLabel("Таймаут, мс:"))
	layout.addWidget(lineEditTimeOut)	
	layout.addStretch(1)	
	gbVLayout.addLayout(layout)
	
	layout = QHBoxLayout()
	#layout.addStretch(1)
	layout.addWidget(QLabel("tag:"))
	#layout.addWidget(comboActionPort)
	#layout.addWidget(QLabel("код:"))
	#layout.addWidget(comboAction)
	#layout.addWidget(QLabel("to:"))
	layout.addWidget(lineEditTag)		
	layout.addStretch(1)
	vLayout.addLayout(layout)
	
	updateComboReason()	
	updateComboAction()

	layout4 = QHBoxLayout()
	layout4.addStretch(1)
	
	butRead =  QPushButton("read ")
	layout4.addWidget(butRead)
	butRead.clicked.connect(handleCreateReadDepMessage)
	
	global butWrite
	butWrite = QPushButton("write")
	layout4.addWidget(butWrite)
	butWrite.clicked.connect(callback_factory(handleCreateAddScriptMessage, depId))
	
	butOk = QPushButton("OK")
	layout4.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout4)

	dlg.setLayout(vLayout)
	dlg.exec()
	
#def handleComboReasonChangeCurrent(ind):
def updateComboReason(ind=-1):
	global reasonLayout
	global comboDiscrReasonProp
	global reasonADCThr
	global reasonLineEditThr
	global comboReason
	
	if(ind == -1):
		ind = comboReason.currentIndex()
		'''if comboReason != None:
			ind = comboReason.currentIndex()
		else:
			return'''
	print("updateComboReason " + str(ind))
	comboDiscrReasonProp.hide()
	reasonADCThr.hide()
	#reasonRangeMin.setParent(None)
	#reasonRangeMax.setParent(None)
	reasonLineEditThr.hide()
	reasonLblMin.hide()
	reasonLblMax.hide()
	reasonRangeMin.hide()
	reasonRangeMax.hide()
	reasonPushCountSpinBox.hide()
		
	if (ind == 1) or (ind == 2): #ADC min #ADC max
		reasonLineEditThr.setValidator(QtGui.QIntValidator(-32768, 32767))
		reasonLineEditThr.setFixedWidth(100)
		reasonLineEditThr.show()		
		#reasonLayout.insertWidget(reasonLayout.count()-1, reasonLineEditThr)
		
	elif (ind == 3) or (ind == 4) or (ind == 5) or (ind == 6) : #18b20 min,max, DHT t min,max, , DHT rh min,max
		vl = QtGui.QDoubleValidator(-99.0, 99.0, 1)
		vl.setNotation(QtGui.QDoubleValidator.Notation.StandardNotation);
		reasonLineEditThr.setValidator(vl)
		reasonLineEditThr.setFixedWidth(100)
		reasonLineEditThr.show()	
		#reasonLayout.insertWidget(reasonLayout.count()-1, reasonLineEditThr)
	elif (ind == 7)  or (ind == 8):
		#print("hhh")
		#reasonLineEditThr.setWidth(40)
		vl = QtGui.QDoubleValidator(0.0, 99.0, 1)
		vl.setNotation(QtGui.QDoubleValidator.Notation.StandardNotation);
		reasonLineEditThr.setValidator(vl)
		reasonLineEditThr.setFixedWidth(100)
		reasonLineEditThr.show()	
		#reasonLayout.insertWidget(reasonLayout.count()-1, reasonLineEditThr)
	elif ind == 9: #rele
		#reasonLayout.insertWidget(reasonLayout.count()-1, comboDiscrReasonProp)
		comboDiscrReasonProp.setFixedWidth(70)
		comboDiscrReasonProp.show()
	elif ind == 10: #push count
		#reasonLayout.insertWidget(reasonLayout.count()-1, reasonLineEditThr)
		comboDiscrReasonProp.setFixedWidth(70)
		reasonPushCountSpinBox.show()
	elif ind == 11: #push duration
		#reasonLayout.insertWidget(reasonLayout.count()-1, discrPushMinMaxWdg)
		
		reasonLblMin.show()
		reasonLblMax.show()
		reasonRangeMin.show()
		reasonRangeMax.show()
	
		'''while reasonLayout.count():
			item = reasonLayout.itemAt(0)
			if isinstance(item, QtWidgets.QWidgetItem):
				print ("widget" + str(item))
				#reasonLayout.removeItem(item)
				#item.widget().close()
            # or
            # item.widget().setParent(None)
			elif isinstance(item, QtGui.QSpacerItem):
				print( "spacer " + str(item))
            # no need to do extra stuff
			else:
				print ("layout " + str(item))
				#self.clearLayout(item.layout())'''
			#reasonLayout.
			#reasonLayout.removeWidget()
		'''reasonLayout.addWidget(reasonRangeMin)
		reasonLayout.addWidget(QLabel("min,ms*100"))
		reasonLayout.addWidget(reasonRangeMax)
		reasonLayout.addWidget(QLabel("max,ms*100"))'''

		comboDiscrReasonProp.setFixedWidth(70)

	
#def handleComboActionChangeCurrent(ind):
def updateComboAction(ind = -1):
	global actionLayout, lineEditActionProp, releComboActionProp
	global dimPwmComboActionProp
	global actionDimSpinVal, actionPwmSpinVal
	
	if(ind == -1):
		ind = comboAction.currentIndex()		
		#if comboAction != None:
		#	ind = comboAction.currentIndex()		
		#else:
		#	return
	print("updateComboAction " + str(ind) + " actionLayout " + str(actionLayout))	
	lineEditActionProp.hide()
	releComboActionProp.hide()
	dimPwmComboActionProp.hide()
	actionDimSpinVal.hide()
	actionPwmSpinVal.hide()
	if actionLayout != None:
		if ind == 1: #rele
			#actionLayout.insertWidget(actionLayout.count()-1, releComboActionProp)
			releComboActionProp.show()
		elif ind == 2: #DimmerEna
			dimPwmComboActionProp.show()
		elif ind == 3: #DimmerBright
			actionDimSpinVal.show()
		elif ind == 4: #PWMEna
			dimPwmComboActionProp.show()
		elif ind == 5: #DimmerBright
			actionPwmSpinVal.show()
		elif ind == 6: #DaliUnitEna 
			#actionLayout.insertWidget(actionLayout.count()-1,releComboActionProp)			
			releComboActionProp.show()
		elif ind == 8: #DaliGroupEna 
			#actionLayout.insertWidget(actionLayout.count()-1,releComboActionProp)			
			releComboActionProp.show()
		else : 
			#print("remove both")
			actionLayout.addStretch(1)
			
def parseDependenceEvent(sqi):
	print("dimmer event " + hex(sqi.cmd))
	pass

def parseDependenceGet(self, sqi):
	if (sqi.cmd==0x0f) or (sqi.ident&0xffffff00 == DEPS_CLASS):
		#self.setDepTableVal(ident-DEPS_CLASS+1, strArr)
		#self.setDepTableVal(strArr)
		depInd = int.from_bytes(sqi.body[0], byteorder='little')
		reasonPort = int.from_bytes(sqi.body[1], byteorder='little')
		reasonCode = int.from_bytes(sqi.body[2], byteorder='little')
		reasonThr = int.from_bytes(sqi.body[3:5], byteorder='big')
		actionPort = int.from_bytes(sqi.body[5], byteorder='little')
		actionCode = int.from_bytes(sqi.body[6], byteorder='little')
		actionCodeProp = int.from_bytes(sqi.body[7], byteorder='little')
		actionTimeout =  int.from_bytes(sqi.body[8:10], byteorder='big')
		bEna = int.from_bytes(sqi.body[10], byteorder='big')
		tag = int.from_bytes(sqi.body[11:13], byteorder='big')
		#if DEBUG_PRINT:
		#print("setDepTable " + str(depNum) + " bEna:" + str(bEna) + " rp:" + str(reasonPort) + " rc:" + str(reasonCode) + " thr:" + str(reasonThr) + 
		#							" ap:" + str(actionPort) + " ac:" + str(actionCode) + " acTo:" + str(actionTimeout) + " tag:" + str(tag)) # + " : " + str(data)
		setDependence(self, depInd, reasonPort,reasonCode,reasonThr, actionPort,actionCode,actionCodeProp,actionTimeout, bEna, tag)		
def parseDependenceEvent(self, sqi):
	if (sqi.cmd==0x0f) or (sqi.ident&0xffffff00 == DEPS_CLASS):
		depNum = int.from_bytes(sqi.body[0], byteorder='little')
		reasonPort = int.from_bytes(sqi.body[1], byteorder='little')
		reasonCode = int.from_bytes(sqi.body[2], byteorder='little')
		reasonThr = int.from_bytes(sqi.body[3:5], byteorder='big')
		actionPort = int.from_bytes(sqi.body[5], byteorder='little')
		actionCode = int.from_bytes(sqi.body[6], byteorder='little')
		actionCodeProp = int.from_bytes(sqi.body[7], byteorder='little')
		actionTimeout =  int.from_bytes(sqi.body[8:10], byteorder='big')
		bEna = int.from_bytes(sqi.body[10], byteorder='big')
		tag =  int.from_bytes(sqi.body[11:13], byteorder='big')
		print("setDepTable " + str(depNum) + " bEna:" + str(bEna) + " rp:" + str(reasonPort) + " rc:" + str(reasonCode) + " thr:" + str(reasonThr) + 
								" ap:" + str(actionPort) + " ac:" + str(actionCode) + " acp:" + str(actionCodeProp) + " acTo:" + str(actionTimeout)) # + " : " + str(data)
		setDependence(self, depNum, reasonPort,reasonCode,reasonThr,actionPort,actionCode,actionCodeProp,actionTimeout, bEna, tag)


def readAllDeps() :
	for i in range(DEPENDENS_COUNT):
		parent.sendQuery(GET_PROP, [0x0F, i])			
