from PyQt6.QtWidgets import QPushButton,QWidget, QHBoxLayout

width = 35
def init(h10,h30,h50,h75,h100):
	wdg = QWidget()
	layout = QHBoxLayout()
	layout.setContentsMargins(10, 0, 10, 0)
	
	pb = QPushButton("10%")
	pb.setFixedWidth(width)
	pb.clicked.connect(h10)
	layout.addWidget(pb)
	
	pb = QPushButton("30%")
	pb.setFixedWidth(width)
	pb.clicked.connect(h30)
	layout.addWidget(pb)
	
	pb = QPushButton("50%")
	pb.setFixedWidth(width)
	pb.clicked.connect(h50)
	layout.addWidget(pb)
	
	pb = QPushButton("75%")
	pb.setFixedWidth(width)
	pb.clicked.connect(h75)
	layout.addWidget(pb)
	
	pb = QPushButton("100%")
	pb.setFixedWidth(width)
	pb.clicked.connect(h100)
	layout.addWidget(pb)
	
	wdg.setLayout(layout)
	return wdg