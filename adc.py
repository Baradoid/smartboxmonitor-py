from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget
from PyQt6.QtWidgets import QButtonGroup
from utils import *

ADC_CLASS = 	  0x01010200

dlg = None
butWrite = None
comboPortAdcNum = None
periodComboBox = None
minBorderU1LineEdit = None
minBorderU2LineEdit = None
dataPeriodLineEdit = None

curPortNum = 23
bg = None

parent = None
def init(self):
	global parent
	parent = self
	
def showAdcProps(chId = -1):
	global dlg, butWrite, comboPortAdcNum, bg
	global periodComboBox
	global minBorderU1LineEdit, minBorderU2LineEdit
	global dataPeriodLineEdit
	
	portNumArr = []
	dlg = QDialog(parent)
	dlg.setWindowTitle("ADC params")
	#self.setADCPropsWindow.setWindowModality(Qt.WindowModal)
	#self.setModal(True)
		
	vLayout = QVBoxLayout()
	#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboReason.addItems(["rise","fall"])		
	#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboAction.addItems(["on", "off", "inv"])
	
	#self.comboPortNum1 = QtWidgets.QComboBox()
	#self.comboActionPort = QtWidgets.QComboBox()		
	#portList = ["1","2","3","4","5","6","7","8"]
	#self.comboPortNum1.addItems(portList)			
	#self.comboActionPort.addItems(portList)
	
	#print("add c")
	#comboPortAdcNum = QtWidgets.QComboBox()
	#scrpitNumArr = []
	#for i in range(23,26) :
	#	scrpitNumArr += [str(i)]
	
	portNumArr.append(QRadioButton("23"))
	portNumArr.append(QRadioButton("24"))	
	portNumArr.append(QRadioButton("25"))	

	bg = QButtonGroup()
	bg.addButton(portNumArr[0])
	bg.addButton(portNumArr[1])
	bg.addButton(portNumArr[2])

	bg.buttonClicked.connect(onRadioButtonClicked)
	# '''curPortNum = int(b.text())'''
	
	iSelId = -1
	iSelId = chId - 22
	
	if (iSelId < 0) or  (iSelId > 2):
		iSelId = 0
	
	portNumArr[iSelId].setChecked(True)
	#print(scrpitNumArr)
	#comboPortAdcNum.addItems(scrpitNumArr)	

	#print("add kk")
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
	#self.lineEditTimeOut.setMinimum(3)
	#self.lineEditTimeOut.setMaximum(0xffff)
			
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер порта:"))
	#layout1.addWidget(comboPortAdcNum)			
	layout1.addWidget(portNumArr[0])
	layout1.addWidget(portNumArr[1])
	layout1.addWidget(portNumArr[2])
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("период выдачи, мс:"))
	periodComboBox = QtWidgets.QComboBox()
	periodComboBox.addItem("0", 0)
	periodComboBox.addItem("100", 100)
	periodComboBox.addItem("250", 250)
	periodComboBox.addItem("500", 500)
	periodComboBox.addItem("1000", 1000)
	layout1.addWidget(periodComboBox)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница U1:"))
	minBorderU1LineEdit = lindEdit = QtWidgets.QLineEdit("500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("минимальная граница U2:"))
	minBorderU2LineEdit = lindEdit = QtWidgets.QLineEdit("1500")
	lindEdit.setFixedWidth(60)
	lindEdit.setAlignment(Qt.AlignmentFlag.AlignHCenter)
	layout1.addWidget(lindEdit)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Период отправки данных, с:"))
	dataPeriodLineEdit = dataPeriod = QtWidgets.QComboBox()
	dataPeriod.addItem("0 (OFF)", 0)
	dataPeriod.addItem("1", 1)
	dataPeriod.addItem("2", 2)
	dataPeriod.addItem("5", 5)
	dataPeriod.addItem("10", 10)
	dataPeriod.setFixedWidth(60)
	#dataPeriod.setAlignment(Qt.AlignHCenter)
	layout1.addWidget(dataPeriod)
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout4 = QHBoxLayout()
	layout4.addStretch(1)
	
	butWrite = QPushButton("write")
	layout4.addWidget(butWrite)
	
	butOk = QPushButton("close")
	layout4.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout4)

	dlg.setLayout(vLayout)
	dlg.show() 

def onRadioButtonClicked(button):
	global curPortNum
	curPortNum = int(button.text())
	#print('Current: ' +str(curPortNum))
	
def handleSetADCParams(self, chId = -1):	
	#print( str(chId))
	showAdcProps(chId)
	butWrite.clicked.connect(handleSetAdcPropsWrite)
	dlg.exec()

def handleSetAdcPropsWrite():
	global curPortNum
	pNum = curPortNum
	#period = int(adc.periodComboBox.currentData())
	#minU1 = int(adc.minBorderU1LineEdit.text())
	#minU2 = int(adc.minBorderU2LineEdit.text())
	dataPeriod = int(dataPeriodLineEdit.currentData())
	#print("write ADC params: " + str(pNum) + " " + str(dataPeriod) + ''' " " + str(minU1) + " " + str(minU2) + " " + str(dataPeriod)''')
	parent.sendQuery(SET_PROP, [0x02, pNum, dataPeriod&0xff])
									#(minU1>>8)&0xff, minU1&0xff, 
									#(minU2>>8)&0xff, minU2&0xff, 
									#(dataPeriod>>8)&0xff, dataPeriod&0xff])
	'''if(self.radioButDiscrete.isChecked()) : 
		self.sendQuery(SET_PROP, [0x01, pNum, 0x00, 0x00,   0x00, 0x00,  0x00,0x01,  0x00,0x01,  0x00,0x01])
	elif(self.radioButDs18b20.isChecked()) : 
		self.sendQuery(SET_PROP, [0x03, pNum, 0x00, 0x00, 0x00, 0x00, 0x00,0x01])
	elif(self.radioButIButton.isChecked()) : 
		self.sendQuery(SET_PROP, [0x0A, pNum, 0x00, 0x00, 0x00, 0x00, 0x00,0x01, 0x00,0x01])
	elif(self.radioButDht.isChecked()) : 
		self.sendQuery(SET_PROP, [0x04, pNum, 0x00, 0x00,   0x00, 0x00,  0x00,0x01,  0x00,0x00,  0x00,0x01])
	else :
		print("select port mode!")'''
		
def handleUpdateADC(self):
	for i in range(3) :
		parent.sendQuery(GET_PROP, [0x02, i+22], ADC_CLASS+i)
