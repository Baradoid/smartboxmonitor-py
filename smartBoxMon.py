import sys
from smartBoxMonMainWnd import Ui_smartBoxMonMainWnd
import time
import copy 
import datetime

from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QDialog, QApplication, QMainWindow, QHeaderView, QLabel, QWidget, QSlider
from PyQt6.QtWidgets import QTableWidget, QTableWidgetItem, QAbstractItemView
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton
from PyQt6.QtNetwork import QUdpSocket, QHostAddress
from PyQt6.QtSerialPort import QSerialPort, QSerialPortInfo
from PyQt6.QtCore import QIODevice
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtCore import QSettings
from PyQt6.QtCore import QByteArray 
from PyQt6.QtCore import QPoint 
from PyQt6.QtCore import QThread, pyqtSignal
from PyQt6.QtNetwork import QUdpSocket, QHostAddress, QTcpSocket
from PyQt6.QtGui import QCursor, QIntValidator
from PyQt6.QtGui import QColor, QBrush, QTextDocument, QTextCursor
from PyQt6.QtCore import QRegularExpression
from PyQt6.QtGui import QRegularExpressionValidator 
from PyQt6.QtWidgets import QFileDialog
from xmodemNonblocking import XMODEM

import adc, ds18b20, dht, rele, cmdTable, sensorDs8, dali, dimmer, pwm, portMode
import discrete as dscr
import dependence as dep

from enum import Enum, auto

import struct

from Parser import processEvents, processGetData, parseInputData, setParserDebugPrint

from utils import *


DEBUG_PRINT = False
#class ParserState(Enum):
#        SfKey = auto()
#        SfLen = auto()
#        SfEnd = auto()

SERIALPORT = "com45"
SERIALPORT_BAUDRATE = 115200

#ser = serial.Serial(SERIALPORT, BAUDRATE)
#print(ser.name)         # check which port was really used
#while True: 
#	print(ser.readline())
#ser.write(b'hello')     # write a string

#ser.close()      

#udpSocket = QUdpSocket(self)
#		self.udpSocket.bind(QHostAddress.Any, 7755)
#		self.udpSocket.readyRead.connect(self.readPendingDatagrams)

settings = QSettings("murinets", "smartBoxMonitor-py")

comPort = QSerialPort()



DISCRETE_CLASS =  0x01010100
ADC_CLASS = 	  0x01010200
SENSORDS8_CLASS = 0x01010300
DHT_CLASS = 	  0x01010500
IBUT_CLASS = 	  0x01010A00
IBUT_EEPROM_CLASS = 0x01010D00

c21_CLASS = 	  0x01012100
c22_CLASS = 	  0x01012200
c23_CLASS = 	  0x01012300
c24_CLASS = 	  0x01012400
c26_CLASS = 	  0x01012600
c25_CLASS = 	  0x01012500

DEPS_CLASS =      0x01010f00
cF6_CLASS = 	  0x0101f600
cFB_CLASS = 	  0x0101fB00
cFD_CLASS = 	  0x0101fD00
cFE_CLASS = 	  0x0101fE00


class maketObj(object) :
	comPort = None
	#buttonLightsOn = ""
	#buttonDemoOn = ""
	lineEditStat = ""
	lineDemo =""
	lineLights =""
	groupBox = ""
	
#maketObjArr = [maketObj() for i in range(8)]

#for o in maketObjArr:
#	print("maketObjArr", o)

def on_serial_read(ind):
	#print(ind,">process bytes:")
	print(ind,">process bytes:", serialArr[ind].readAll())
	#print(ind,">process bytes:", serial.readAll())
	"""
	Called when the application gets data from the connected device.
	"""
	#self.process_bytes(bytes(self.serial.readAll()))
	



class FtThread(QThread):
	signal = pyqtSignal('PyQt_PyObject')
	bRunning = True
	def __init__(self, comName):
		QThread.__init__(self)
		self.git_url = ""
		#print(comName)

	def stop(self):
		bRunning = False
		join()
		
	def run(self):
		while self.bRunning :
			#print("thread run")
			self.sleep(1)
			


  
class SmartBoxMonApp(QMainWindow): #QMainWindow QDialog
	
	sliderTt = None
	#sendToComList = []
	msgId=1
	depQuerySended = False
	_socket = None
	modem = None
	lenWidth = 1
	
	def appendLogString(self, str) :
		#cf = self.ui.textEditLog.currentCharFormat()
		#cf.setForeground(QBrush(Qt.green))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#str = "<font color=\"DeepPink\">" + str + "</font><br>"
		self.ui.textEditLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)

		#cf.setForeground(QBrush(Qt.red))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#self.ui.textEditLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)
	

	def appendConnectLogString(self, str) :
		#cf = self.ui.textEditLog.currentCharFormat()
		#cf.setForeground(QBrush(Qt.green))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#str = "<font color=\"DeepPink\">" + str + "</font><br>"
		self.ui.textEditConnectLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)

		#cf.setForeground(QBrush(Qt.red))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#self.ui.textEditLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)
		
	def appendResponse(self, identStr, str) :
		#print("find response: " + identStr)
		#cf = self.ui.textEditLog.currentCharFormat()
		#cf.setForeground(QBrush(Qt.green))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#str = "<font color=\"DeepPink\">" + str + "</font><br>"
		self.ui.textEditLog.moveCursor(QTextCursor.MoveOperation.End)
		if self.ui.textEditLog.find(identStr, QTextDocument.FindFlag.FindBackward) :
			#print( "response finded" )
			self.ui.textEditLog.moveCursor(QTextCursor.MoveOperation.EndOfLine)			
			self.ui.textEditLog.insertHtml (" &nbsp;&nbsp;&nbsp; &lt;- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  " + str)
		else :
			self.ui.textEditLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)
			
		#self.ui.textEditLog.insertPlainText (QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)
		#cf.setForeground(QBrush(Qt.red))
		#self.ui.textEditLog.setCurrentCharFormat(cf)
		#self.ui.textEditLog.append(QTime.currentTime().toString("hh:mm:ss.zzz") + ">" + str)
	
	
	def __init__(self):
		super().__init__()
		self.ui = Ui_smartBoxMonMainWnd()
		self.ui.setupUi(self)
		#print(self.arguments())
		self.setWindowFlag(Qt.WindowType.WindowContextHelpButtonHint, False)
		self.setWindowFlag(Qt.WindowType.WindowMinimizeButtonHint, True)
		
		geometry = settings.value('geometry', bytes('', 'utf-8'))
		#print("restore: " + str(geometry))
		self.restoreGeometry(geometry)
		self.show()
		
		#self.ui.closeEvent.connect(lambda:  print("close"))
		#self.ui.closeEvent.connect(lambda:  settings.setValue('geometry', self.ui.saveGeometry()))
		#super(SmartBoxMonApp, self).closeEvent.connect(lambda:  settings.setValue('geometry', self.ui.saveGeometry()))

		
		self.udpSocket = QUdpSocket(self)
		self.udpSocket.bind(QHostAddress.SpecialAddress.Any, 7780)
		self.udpSocket.readyRead.connect(self.readPendingDatagrams)
		
		if len(sys.argv) > 1 :
			comName = sys.argv[i+1]
			self.serial = QSerialPort(self)
			self.port = comName
			print("comName:",comName)
			self.serial.setPortName(self.port)
			if self.serial.open(QIODevice.ReadWrite):
				self.serial.setBaudRate(SERIALPORT_BAUDRATE)
				self.serial.readyRead.connect(callback_factory(self.handleSerialReadyRead, i) )
				#self.serial.write(b'stat?\r\n');
				self.serial.write(b'?\r\n');
				#self.clear() # clear the text
			else:
				raise IOError("Cannot connect to device on port {}".format(self.port))
			
		self.timer = QTimer()
		self.timer.timeout.connect(self.handleTimer)
		#self.timer.start(1000)
		
		self.udpSocket = QUdpSocket(self)
		self.udpSocket.bind(QHostAddress.SpecialAddress.Any, 7755)
		self.udpSocket.readyRead.connect(self.readPendingDatagrams)
		self.appendLogString("Start")
		self.appendConnectLogString("Start")
		
		#self.ui.tableWidgetDs19b20.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
		#self.ui.tableWidgetDs19b20.horizontalHeaderItem(0).setTextAlignment(Qt.AlignHCenter)
		#self.ui.tableWidgetDs19b20.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
		
		self.ui.pushButtonDs19b20ReadAll.clicked.connect(ds18b20.handleDs19b20ReadAll) #lambda: self.sendQuery(GET_PROP, [0x21])
		self.ui.pushButtonUpdateDs19b20.clicked.connect(ds18b20.handleDs19b20ReadAll) #lambda: self.sendQuery(GET_PROP, [0x21])
		
		#self.ui.pushButtonDs19b20GetResolution.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x1f]))
		#self.ui.pushButtonDs19b20SetTimeout.clicked.connect(self.handleButtonDs19b20SetTimeout)

		self.ui.tableWidgetDiscrete.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
		self.ui.pushButtonDiscreteReadAll.clicked.connect(dscr.handleUpdateDiscrete)
		self.ui.pushButtonDiscreteSetPps.clicked.connect(dscr.handleSetDiscreteParams)

		self.ui.tableWidgetDht.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeMode.Stretch)
		self.ui.tableWidgetDht.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetDht.verticalHeader().minimumSectionSize())
		self.ui.pushButtonDhtReadAll.clicked.connect(dht.handleDHTReadAll)
		
		dep.init(self)
		dep.addSendAddScriptCB( lambda depNum,portNum,reasonCode,resonThr, actionPort,actionCode, actionProp, actionTimeout, tag: (
				self.sendQuery(SET_PROP, [0x0f, depNum, portNum, reasonCode, (resonThr>>8)&0xff, resonThr&0xff, 
												actionPort, actionCode, actionProp&0xff,
												(actionTimeout>>8)&0xff, actionTimeout&0xff,
												(tag>>8)&0xff, tag&0xff]))
			)
		dep.addCreateReadDepMsgCB( lambda depNum: self.sendQuery(GET_PROP, [0x0f, depNum]) )
		
		self.ui.pushButtonSetScript.clicked.connect(lambda: dep.handleShowSetDependenceDlg())
		
		#self.ui.tableWidgetRelay.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
		self.ui.pushButtonRelayReadAll.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x07]))
		self.ui.pushButtonRelayRush1Test.clicked.connect(rele.relayRush1Test)
		self.ui.pushButtonRelayRush2Test.clicked.connect(rele.relayRush2Test)
		self.ui.pushButtonRelayAllOff.clicked.connect(rele.relayAllOff)
		
		self.ui.pushButton_refreshCom.clicked.connect(self.handleComRefreshButton)
		self.ui.pushButtonComOpen.clicked.connect(self.handleComOpenCloseButton)
		
		#comPort.setBaudRate(QSerialPort.Baud19200)
		#comPort.setBaudRate(QSerialPort.BaudRate.Baud115200)
		comPort.setBaudRate(115200)
		#comPort.setBaudRate(QSerialPort.Baud57600)
		#comPort.setBaudRate(QSerialPort.Baud38400)
		#comPort.setBaudRate(QSerialPort.Baud9600)
		comPort.readyRead.connect(self.handleSerialReadyRead)
 
		#print("COM:" + str(settings.value("usbMain")))
		
		self.handleComRefreshButton()
		mainCom = settings.value("usbMain", "");
		self.appendConnectLogString("COM: restore com port num:\""+("n/a" if len(mainCom)==0 else mainCom)+ "\"")
		if(self.ui.comComboBox.count() == 0):
			self.appendConnectLogString("COM: no com ports in system");
			self.ui.statusbar.showMessage("COM: no com ports in system", 5000)
		else :
			for i in range(self.ui.comComboBox.count()) :
				if(self.ui.comComboBox.itemData(i) == mainCom) :
					self.ui.comComboBox.setCurrentIndex(i)
					if settings.value("comPortOpened", False, type=bool) :
						self.appendConnectLogString("COM: port {0} present. Try open.".format(mainCom));
						self.handleComOpenCloseButton()
					
		self.ui.pushButtonSendReboot.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xff]))
		self.ui.pushButtonGetFmwVrsn.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xfd], cFD_CLASS))
		self.ui.pushButtonGetUptime.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xfe], cFE_CLASS))
		self.ui.pushButtonGetTime.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xfc]))
		self.ui.pushButtonSetTime.clicked.connect(self.handleSetTime)
		self.ui.pushButtonGetDeviceIdentifer.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xfb], cFB_CLASS))
		self.ui.pushButtonSetAllPortsDiscrete.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x12]))
		self.initReleyButtons()
		rele.initButtons(self)
		
		self.sliderTt = QLabel()
		#self.sliderTt.setWindowFlags(QtCore.Qt.ItemDataRole.ToolTip)
		
		cmdTable.initTableWidgetCmds(self)
		self.ui.pushButtonIfaceShift.clicked.connect(lambda: cmdTable.handleShiftIface(self))
		
		colorIface = settings.value("colorIface", False, type=bool)
		cmdTable.handleIfaceColor(self, colorIface)
		self.ui.checkBoxColor.setChecked(colorIface)
		self.ui.checkBoxColor.stateChanged.connect(lambda st:(	 print(st == Qt.Checked), 
																settings.setValue("colorIface", st==Qt.Checked),
																cmdTable.handleIfaceColor(self, st==Qt.Checked) ) )		
																
		self.ui.pushButtonSetPortMode.clicked.connect(lambda v:portMode.show())
		self.ui.pushButtonDepDelAll.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x11]))
		#self.ui.pushButtonDepReadAll.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x0F, 3]))
		self.ui.pushButtonDepReadAll.clicked.connect(dep.readAllDeps)
		
		self.ui.pushButtonWriteDefaultParams.clicked.connect(lambda: self.sendQuery(SET_PROP, [0xf7]))
		
		self.ui.tableWidgetPorts.setRowCount(portMode.INPUT_PORT_COUNT)
		self.ui.tableWidgetPorts.setColumnCount(3)
		self.ui.tableWidgetPorts.setHorizontalHeaderLabels(('mode', '', 'val'))
		for i in range(self.ui.tableWidgetPorts.rowCount()) :
			twi = QTableWidgetItem("N/A")
			#print(str(i))
			twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
			#self.ui.tableWidgetPorts.setItem(i, 0, twi)
			
			btn = QPushButton("set")
			btn.setMaximumWidth(40)
			#btn.clicked.connect(callback_factory(portMode.handleSetPortMode, i+1))
			btn.clicked.connect(lambda v,i=i: portMode.show(i))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			self.ui.tableWidgetPorts.setCellWidget(i, 1, btn)
		self.ui.tableWidgetPorts.resizeColumnsToContents()
		self.ui.tableWidgetPorts.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
		self.ui.pushButtoGSetPortMode.clicked.connect(portMode.handleGetPortMode)
		
		#self.ui.pushButtonSetWfPortCount.clicked.connect(sensorDs8.handleSetWfPortCount)
		#self.ui.pushButtonGetWfPortCount.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x0]))
		
		#self.ui.tableWidgetSensorDS8.resizeColumnsToContents()
		
		#self.ui.tableWidgetSensorDS8.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
		#self.ui.pushButtonSensorDS8Update.clicked.connect(sensorDs8.handleUpdateSensorDS8)
		#self.ui.pushButtonSetMaxConcChannel.clicked.connect(sensorDs8.handleSetMaxConcurrentChannel)
		#self.ui.pushButtonGetMaxConcChannel.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x25]))	
		
		#for i in range(self.ui.tableWidgetSensorDS8.rowCount()) :
			#twi = QTableWidgetItem("N/A")
			#print(str(i))
			#twi.setTextAlignment(QtCore.Qt.AlignCenter)
			#self.ui.tableWidgetPorts.setItem(i, 0, twi)
			
			#btn = QPushButton("set")
			#btn.setMaximumWidth(40)
			#btn.clicked.connect(callback_factory(sensorDs8.handleSetSensorDs8Params, i))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			#self.ui.tableWidgetSensorDS8.setCellWidget(i, 4, btn)

			#btn = QPushButton("on")
			#btn.setMaximumWidth(60)
			#btn.setTextAlignment(QtCore.Qt.AlignCenter)
			#btn.clicked.connect(callback_factory(self.handleSetSensorDs8Params, i))
			#btn.clicked.connect(callback_factory(sensorDs8.handleWriteSensorDS8Ena, i, 1))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			#self.ui.tableWidgetSensorDS8.setCellWidget(i, 5, btn)
			#self.ui.tableWidgetSensorDS8.item(i,4).setTextAlignment(QtCore.Qt.AlignCenter)
			
			#btn = QPushButton("off")
			#btn.setMaximumWidth(60)
			#btn.setTextAlignment(QtCore.Qt.AlignCenter)
			#btn.clicked.connect(callback_factory(self.handleSetSensorDs8Params, i))
			#btn.clicked.connect(callback_factory(sensorDs8.handleWriteSensorDS8Ena, i, 0))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			#self.ui.tableWidgetSensorDS8.setCellWidget(i, 6, btn)
			#self.ui.tableWidgetSensorDS8.cell

			#btn = QPushButton("set")
			#btn.setMaximumWidth(60)
			#btn.setTextAlignment(QtCore.Qt.AlignCenter)
			#btn.clicked.connect(callback_factory(self.handleSetSensorDs8Params, i))
			#btn.clicked.connect(callback_factory(sensorDs8.handleWriteSensorMaxTempTresh, i))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			#self.ui.tableWidgetSensorDS8.setCellWidget(i, 8, btn)
			#self.ui.tableWidgetSensorDS8.cell
			
		
		self.ui.checkBoxDataRush.clicked.connect(self.handleCheckBoxDataRush)
		self.ui.horizontalSliderDataRush.valueChanged.connect(self.handleHorizontalSliderDataRush)
		
		self.ui.pushButtonUpdateADC.clicked.connect(adc.handleUpdateADC)
		
		self.ui.pushButtonUpdateDiscrete.clicked.connect(dscr.handleUpdateDiscrete)
		self.ui.pushButtonSetADC.clicked.connect(callback_factory(adc.handleSetADCParams, -1))
		
		
		self.ui.pushButtonUpdateADC2.clicked.connect(adc.handleUpdateADC)
		self.ui.tableWidgetADC.horizontalHeader().setVisible(True)
		
		#header = self.ui.tableWidgetADC.horizontalHeader().setStretchLastSection(True)
		self.ui.tableWidgetADC.resizeColumnsToContents()
		self.ui.tableWidgetADC.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetADC.verticalHeader().minimumSectionSize())
		for i in range(3) :
			twi = QTableWidgetItem(str(i+23))
			twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
			self.ui.tableWidgetADC.setItem(i, 0, twi)
			
			btn = QPushButton("set")
			btn.setMaximumWidth(60)
			#btn.setTextAlignment(QtCore.Qt.AlignCenter)
			#btn.clicked.connect(callback_factory(self.handleSetSensorDs8Params, i))
			btn.clicked.connect(callback_factory(adc.handleSetADCParams, 22+i))
			#btn.clicked.connect(self.handleSetPortMode(i))
			#btn.setText("set")
			self.ui.tableWidgetADC.setCellWidget(i, 3, btn)
			#self.ui.tableWidgetSensorDS8.cell
		
		#header.setResizeMode(QHeaderView.ResizeToContents)
		#header.setStretchLastSection(True)
		#header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
		#header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)
		#header.setSectionResizeMode(2, QtWidgets.QHeaderView.ResizeToContents)
		
		self.ui.pushButtonSetDiscrete.clicked.connect(dscr.handleSetDiscreteParams)
		self.ui.pushButtonSetDs18b20.clicked.connect(ds18b20.handleSetDs18b20Params)
		
		self.ui.pushButtonUpdateDHT.clicked.connect(dht.handleDHTReadAll)
		self.ui.pushButtonSetDHT.clicked.connect(dht.handleSetDHTParams)
		

		
		self.ui.pushButtonIButtonUpdate.clicked.connect(self.handleUpdateIButton)
		self.ui.pushButtonIButtonDeleteAll.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x0C]))
		
		self.ui.tableWidgetIButton.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
		
		self.ui.tableWidgetIButtonEepromKeys.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())
		
		self.ui.pushButtonIButtonGetEepromKeys.clicked.connect(self.handleUpdateIButtonEepromKeys)
		self.ui.pushButtonIButtonSaveFromCR.clicked.connect(self.handleIButtonSaveFromCR)				
		
		self.ui.pushButtonPWMDelaySet.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x40,  QInputDialog.getInt(self, "Get delay","Seconds:", 28, 0, 100, 1)[0]]))
		self.ui.pushButtonPWMDelayGet.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x40]))
		
		self.ui.pushButtonGetRunTimeStats.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xf5]))
		self.ui.pushButtonGetRunTimeStats.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xf4]))

		#self.ui.pushButtonSDS8TempTimeoutGet.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x20]))
		#self.ui.pushButtonSDS8TempTimeoutSet.clicked.connect(sensorDs8.handleSensorDs8SetTemperatureTimeout)
		#self.ui.pushButtonSDS8GetReleState.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x28]))
		
		self.ui.groupBoxDht.layout().setAlignment(Qt.AlignmentFlag.AlignTop)
		ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"   # Part of the regular expression
        # Regulare expression
		ipRegex = QRegularExpression("^" + ipRange + "\\." + ipRange + "\\." + ipRange + "\\." + ipRange + "$")
		ipValidator = QRegularExpressionValidator(ipRegex, self)   
		
		self.ui.lineEditTcpAddr.setValidator(ipValidator)    
		self.ui.lineEditTcpAddr.setText(settings.value("tcpAddr", "127.0.0.1"))
		self.ui.pushButtonTcpConnect.clicked.connect(self.handleButtonTcpConnect)
		self.ui.lineEditTcpPort.setText(settings.value("tcpPort", "21"))
		#print("tcpSocketConnected restore: " + str(settings.value("tcpSocketConnected", False, type=bool)) + "   :" + self.ui.pushButtonTcpConnect.text() )
		if settings.value("tcpSocketConnected", False, type=bool) == True :
			self.handleButtonTcpConnect()
		
		
		self.ui.checkBoxOneLineResponse.setChecked(settings.value("oneLineResponse", True, type=bool))
		self.ui.checkBoxOneLineResponse.clicked.connect(lambda: settings.setValue("oneLineResponse", self.ui.checkBoxOneLineResponse.isChecked()))
		
		self.ui.tabWidget.setCurrentIndex(settings.value("currentTabInd", 0, type=int))
		self.ui.tabWidget.currentChanged.connect(lambda ind: settings.setValue("currentTabInd", ind))
		#self.ui.tabWidget.currentChanged.connect(lambda b: print("aaa " + str(b)))
	
		self.ui.pushButtonSendRaw.clicked.connect(self.handleSendRaw)
		
		regExpStr = ""
		for i in range(60):
			regExpStr += "[0-9a-fA-F][0-9a-fA-F]\\ "
		#print("regExpStr: "  + regExpStr)
		self.ui.lineEditRaw.setValidator(QRegularExpressionValidator (QRegularExpression(regExpStr)))
		self.ui.listWidgetRawHistory.itemClicked.connect(self.listWidgetItemClicked)
		
		historyCount = settings.value("historyCount", 0, type=int)
		for i in range(historyCount):
			self.ui.listWidgetRawHistory.addItem(settings.value("historyLine"+str(i)))
			
		self.ui.pushButtonGetError.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xf5]))
		self.ui.pushButtonGetError_2.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xf5]))
				
		#self.ui.dimmerLightUpTimeSet.clicked.connect(lambda: print(self.ui.spinBoxLightUpTime.value()))
		self.ui.dimmerLightUpTimeSet.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x35, int(self.ui.dimmerLightUpTimeChannel.value()), int(self.ui.spinBoxLightUpTime.value()/100)]))
		self.ui.dimmerLightUpTimeGet.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x35, int(self.ui.dimmerLightUpTimeChannel.value())]))
		
		self.ui.pushButtonRadioSend.clicked.connect(self.handlePushButtonRadioSend)
		
		self.ui.pushButtonSendFw.clicked.connect(self.handleSendFw)
		self.ui.pushButtonSendFwSelectFile.clicked.connect(self.handleSelectFwFile)
		self.restoreFwPath()
		
		self.ui.radioButtonLenWidth1.clicked.connect(self.handleLenWidth1)
		self.ui.radioButtonLenWidth2.clicked.connect(self.handleLenWidth2)
		self.lenWidth = settings.value("lenWidth", 2);
		#print("lenWidth: " + str(lenWidth))
		if self.lenWidth == 2:
			self.ui.radioButtonLenWidth2.setChecked(True)
		else:
			self.ui.radioButtonLenWidth1.setChecked(True)
			
		self.ui.checkBoxDebugPrint.clicked.connect(self.handleCheckBoxDebugPrintClicked)
		
		self.ui.pushButtonIRGateSend.clicked.connect(self.handleIRGateSendClicked)
		self.ui.textEditIRGateString.textChanged.connect(self.handletextEditIRGateTextChanged)
		self.ui.pushButtonClearLog.clicked.connect(lambda: self.ui.textEditLog.clear())
	
		self.ui.pushButtonIrGateEventsStateGet.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x4C]))
		self.ui.checkBoxIrGateEventsStateSet.clicked.connect(lambda b: self.sendQuery(SET_PROP, [0x4C, int(b)]))
		self.ui.pushButtonIrGateClearRecvd.clicked.connect(lambda: self.ui.textEditIRGateEventMsg.clear())
		self.ui.pushButtonTestEeprom.clicked.connect(lambda: self.sendQuery(GET_PROP, [0xf3]))
				
		dali.initTab(self)
		dimmer.init(self)
		pwm.init(self)
		portMode.init(self)
		dht.init(self)
		dscr.init(self)
		adc.init(self)
		ds18b20.init(self)
	
	def handletextEditIRGateTextChanged(self):
		ttLen = len(self.ui.textEditIRGateString.toPlainText())
		if ttLen > 1024:
			self.ui.lineEdiIRGateStringLen.setStyleSheet("color: red;")
		else :
			self.ui.lineEdiIRGateStringLen.setStyleSheet("color: black;")
		self.ui.lineEdiIRGateStringLen.setText(str(ttLen))
		
	def handleCheckBoxDebugPrintClicked(self):
		global DEBUG_PRINT
		DEBUG_PRINT = self.ui.checkBoxDebugPrint.isChecked()
		setParserDebugPrint(DEBUG_PRINT)
	
	def handleIRGateSendClicked(self):
		tt= self.ui.textEditIRGateString.toPlainText()
		print("send: \"" + tt + "\"")
		ttAscii = tt.encode(encoding='ascii', errors='replace')
		ttAsciiInArr = 1024*[0]
		#print(ttAsciiInArr)
		for i in range(len(ttAscii)):
			ttAsciiInArr[i] = int(ttAscii[i])
		ttAsciiInArr[len(ttAscii)] = 0xd
		ttAsciiInArr[len(ttAscii)+1] = 0xa
		#print(ttAsciiInArr)
		#ttAsciiInArr = np.array(ttAsciiInArr)
		#print(ttAsciiInArr)
		#ttAsciiInArr = array.array('i', ttAsciiInArr)  
		self.sendQuery(SET_PROP, [0x4d] + ttAsciiInArr)
	
	def handleLenWidth1(self):
		settings.setValue("lenWidth", 1)
		self.lenWidth = 1
	def handleLenWidth2(self):
		settings.setValue("lenWidth", 2)
		self.lenWidth = 2
		#self.closeEvent.connect(lambda:  settings.setValue('geometry', self.ui.saveGeometry()))
		#super(SmartBoxMonApp, self).closeEvent.connect(lambda:  settings.setValue('geometry', self.ui.saveGeometry()))
	def closeEvent(self, event):
		print("X is clicked")
			
	def closeEvent(self, event):
		print("close event")
		geometry = self.saveGeometry()
		self.settings.setValue('geometry', geometry)		
		super(SmartBoxMonApp, self).closeEvent(event)
		
	def getc(self, size, timeout=1):
		if (self._socket == None):
			return None
		else:
			startToTime = time.time()
			rr = None
			while((time.time() - startToTime < timeout)):
				if self.lastSocketRcvdData != None:
					rr = self.lastSocketRcvdData
					self.lastSocketRcvdData = None
					break
			print(rr)
			return rr
		
	def putc(self, data, timeout=1):
		#print("putc " + str(data))
		#print("putc " + ':'.join('{:02x}'.format(x) for x in data))
		#ba = bytes(data)
		if(comPort.isOpen()):
			#print("send com " + str(bytes(data)))
			iSend = comPort.write(bytes(data))
				
		if (self._socket == None):
			return False
		else:
			#print("sendquerytcp " + str(bytes(data)))
			return self._socket.write(bytes(data))
		
	def handleSendFw(self):
		filePath = self.ui.lineEditFwPath.text()
		if not filePath:
			return
		print("send fw")
		stream = open(filePath, 'rb')
		#self._socket.readyRead.disconnect()
		self.modem = XMODEM(self.getc, self.putc)
		res = self.modem.send(stream)
		print("XMODEM res: " + str(res))
		#self._socket.readyRead.connect(self.handleSocketReadyRead)
	
	def restoreFwPath(self):
		filePath = settings.value("fwPath", "");
		self.ui.lineEditFwPath.setText(filePath)
		self.ui.lineEditFwPath.setToolTip(filePath)
	
	def handleSelectFwFile(self):
		#print("selectFw")
		filePath = self.ui.lineEditFwPath.text()
		#print(filePath)
		options = 0 #QFileDialog.options()
		#options |= QFileDialog.ViewMode.DontUseNativeDialog
		#filePath, _ = QFileDialog.getOpenFileName(self,"select elx device firmware bin",filePath,"Bin fw Files (*.bin)", options=options)
		filePath, _ = QFileDialog.getOpenFileName(self,"select elx device firmware bin",filePath,"Bin fw Files (*.bin)")
		if filePath:
			print(filePath)
			self.ui.lineEditFwPath.setText(filePath)
			self.ui.lineEditFwPath.setToolTip(filePath)
			settings.setValue("fwPath", filePath)
			
	def listWidgetItemClicked(self, item):
		print("selected:" + item.text())
		self.ui.lineEditRaw.setText(item.text())
		
	def handleSendRaw(self):
		sqiStr = self.ui.lineEditRaw.text()
		if len(sqiStr) == 0 :
			return
		listWdgCount = self.ui.listWidgetRawHistory.count()
		#print("---")
		bExistInHistory = False
		for i in range(listWdgCount):
			lw = self.ui.listWidgetRawHistory.item(i)
			if lw.text() == sqiStr:
				bExistInHistory = True
				print ("text equal")
				#self.ui.listWidgetRawHistory.removeItemWidget(lw)
				#self.ui.listWidgetRawHistory.addItem(str)
				#return
			#print(lw.text())
		if bExistInHistory == False:
			self.ui.listWidgetRawHistory.addItem(sqiStr)
			settings.setValue("historyLine"+str(listWdgCount), sqiStr)
			listWdgCount = self.ui.listWidgetRawHistory.count()
			settings.setValue("historyCount", listWdgCount)
		
		sqiArr = sqiStr.split(' ')
		sqiIntArr = []
		for sqi in sqiArr:
			#print("convert: '" + sqi + "'")
			if len(sqi) == 0 : 
				continue
			sqiIntArr += [int(sqi, 16)]
		sqiByteArr = bytes(sqiIntArr)
		#print("arr:" + str(sqiArr))
		#return
		#sendList = bytes([0x02, flag, (id>>24)&0xff, (id>>16)&0xff, (id>>8)&0xff, (id>>0)&0xff, len(arr)] +  arr )
		#crc16 = self.crc16(sendList, 0, len(sendList))
		#sendList = bytes([0x7e]) + bytes(sendList + crc16.to_bytes(2, byteorder="big"))		
		#sendListStr = str(sendList)
		#sendListBa = sendListStr.encode()
		#print(str(len(sendList)))
		#self.sendToComList.append(sendList)
		iSend = comPort.write(sqiByteArr)
		#print(" iSended: " + iSend + "send arrLen " + str(len(sendListBa)) + " arr: "+ str(sendListBa))
		sendListHexStr = self.appendColorTegs(sqiByteArr)
		#sendListHexStr += "<br>"
		#print(sendListStr)
		#if bPrint==True:
			#print(" iSended: " + str(iSend) + " send arrLen " + str(len(sendList)) + " arr: "+ sendListHexStr)
			
		self.appendLogString(" -> " + sendListHexStr )  #repr(sendList)#
		


	def initReleyButtons(self) :	
		#self.ui.pushButtonDimmerReadAll.clicked.connect(lambda: self.sendQuery(GET_PROP, [0x2f]))
		#self.ui.pushButtonPWMReadAll.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x3e]))
		self.ui.pushButtonDimmerWriteEEPROM.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x30]))
		self.ui.pushButtonPWMWriteEEPROM.clicked.connect(lambda: self.sendQuery(SET_PROP, [0x3f]))
				
	def handleComOpenCloseButton(self) :
		#print("comOpenClose")
		if(self.ui.pushButtonComOpen.text() == "open") :
			#print("open")
			if(comPort.isOpen() == False) :
				comName = self.ui.comComboBox.currentText()
				
				#self.ftThread = FtThread(comName)
				#self.ftThread.start()

				if(len(comName) > 0) :
					comPort.setPortName(comName)
					
					if(comPort.open(QSerialPort.OpenModeFlag.ReadWrite) == False) :
						print(comName + " port open FAIL")
						return
					self.ui.pushButtonComOpen.setText("close")
					self.appendConnectLogString(comName + " port opened")
					self.ui.statusbar.showMessage(comName + " port opened", 5000)
					settings.setValue("usbMain", comName)
					settings.setValue("comPortOpened", True)
				else:
					self.ui.statusbar.showMessage("COM: no com ports in system", 5000)
					
		else : 
			#print("close")
			comPort.close()
			self.ui.pushButtonComOpen.setText("open")
			settings.setValue("comPortOpened", False)
			
	def handleComRefreshButton(self) :
		#print("comRefresh")
		self.ui.comComboBox.clear()
		serialPortInfos = QSerialPortInfo.availablePorts()
		i=0
		for port in serialPortInfos :
			description = port.description()
			manufacturer = port.manufacturer()
			serialNumber = port.serialNumber()
			#print("port" + str(port)+" "+ port.portName()+ " "+ description + manufacturer + serialNumber + "\n")
			self.ui.comComboBox.addItem(port.portName(), port.portName())
			toolTipString = description +"\nManufacturer:"+ manufacturer
			if(len(serialNumber) > 0) :
				toolTipString += "\nserialNumber:" + serialNumber
			self.ui.comComboBox.setItemData(i, toolTipString, Qt.ItemDataRole.ToolTipRole)
			i+=1
			
			
			
	def crc16(self, data : bytearray, offset , length):
		if data is None or offset < 0 or offset > len(data)- 1 and offset+length > len(data):
			return 0
		crc = 0xFFFF
		for i in range(0, length):
			crc ^= data[offset + i] << 8
			for j in range(0,8):
				if (crc & 0x8000) > 0:
					crc =(crc << 1) ^ 0x1021
				else:
					crc = crc << 1
		return crc & 0xFFFF
		
	def appendColorTegs(self, arr) :
		sendListHexStr = ""
		for i in range(len(arr)):
			sb = arr[i]
			if i == 0 :	
				sendListHexStr += "<font color=\"Blue\">"
			elif i == 2 :	
				sendListHexStr += "<font color=\"Green\">"
			elif i == 3 :	
				sendListHexStr += "<font color=\"Gray\">" #"<font color=\"Red\">"
			elif i == 7 :	
				sendListHexStr += "<font color=\"Brown\">"
			elif i == 9 :	
				sendListHexStr += "<font color=#FF8040>"
				#sendListHexStr += "<font color=Orange>" #Orange
			elif i == len(arr)-2 :	
				sendListHexStr += "<font color=\"purple\">"

			sendListHexStr += " " + "%0.2X" % sb  #hex(sb)
			if (i==1) or (i==2) or (i==6) or (i==8) or (i==len(arr)-3)or (i==len(arr)-1):	
				sendListHexStr += "</font>"
		return sendListHexStr
		
	def getIdentAsString(self, arr) :
		sendListHexStr = ""
		for i in range(3, min(len(arr), 7)):
			sb = arr[i]
			sendListHexStr += " " + "%0.2X" % sb  #hex(sb)
		return sendListHexStr
		
	def sendQuery(self, flag, arr, id=-1, bPrint=True):
		#id = 0xafbacd
		#print(str(id.to_bytes(4, byteorder='big')))
		#buf = QByteArray(b'\x7e\x01\x00\x00\x00\x00\x01\x01\x07')
		#sendList = [chr(0x7e), chr(0x01), chr(0x01), chr(0x00),chr(0x00), chr(0x00), chr(0x01)] + [len(arr)] + arr + [chr(0),chr(0)]
		ver = 0x02
		#sendList = bytes([0x7e, 0x01, flag, 0x00,0x00, 0x00, 0x01, len(arr)] +  arr + [22,33])
		if(id==-1):
			id = self.msgId
			self.msgId+=1
		msgLen = len(arr)
		if self.lenWidth == 1:
			sendList = bytes([ver, flag, (id>>24)&0xff, (id>>16)&0xff, (id>>8)&0xff, (id>>0)&0xff, msgLen&0xff] +  arr )
		elif self.lenWidth == 2:
			sendList = bytes([ver, flag, (id>>24)&0xff, (id>>16)&0xff, (id>>8)&0xff, (id>>0)&0xff, (msgLen>>8)&0xff, (msgLen>>0)&0xff] +  arr )
		crc16 = self.crc16(sendList, 0, len(sendList))
		sendList = bytes([0x7e]) + bytes(sendList + crc16.to_bytes(2, byteorder="big"))		
		#sendListStr = str(sendList)
		#sendListBa = sendListStr.encode()
		#print(str(len(sendList)))
		#self.sendToComList.append(sendList)
		if(comPort.isOpen()):
			iSend = comPort.write(sendList)
			#print("iSend " + str(iSend))
		#print(" iSended: " + iSend + "send arrLen " + str(len(sendListBa)) + " arr: "+ str(sendListBa))
		sendListHexStr = self.appendColorTegs(sendList)
		#sendListHexStr += "<br>"
		#print(sendListStr)
		#if bPrint==True:
			#print(" iSended: " + str(iSend) + " send arrLen " + str(len(sendList)) + " arr: "+ sendListHexStr)
			
		self.appendLogString(" -> " + sendListHexStr )  #repr(sendList)#
		#print("sendquerytcp " + str(sendList))
		if (self._socket != None) and self._socket.isOpen(): 
			self._socket.write(sendList)
		

	#def handleDiscreteSetParams(self) :
	#	for i in range(1, 9) :
	#		self.sendQuery(SET_PROP, [0x01, i, 0x1, 0xf4, 0x33, 0x50, 0,0,0,0])
		
	#curRelayReq = 0
	#def handleRelayReadAll(self) :
		#print("relay read all")
		#for i in range(12) :
		
		#	print(i)
		#	time.sleep(0.01)	
		
	def closeEvent(self, event):
		print("closeevent")
		#for serPort in serialArr:
		#	print("close port ",serPort.portName())
		#	serPort.close()
		event.accept() # let the window close

	def handleTimer(self):
		print(">handleTimer")
		#for serPort in serialArr:
			#serPort.write(b'?\r\n');
			
	def handleSerialReadyRead(self):
		#print("handleSerialReadyRead")
		#inStr = repr(comPort.readAll().data().decode("utf-8"))
		strArr = comPort.readAll()
		
		if self.modem:
			self.modem.processChar(strArr)
		#print(strArr)
		#print(bytes(strArr).hex())
		#for x in strArr:
		#	print(x.hex())
		#inStr = strArr.toHex().data().decode('utf8')
		#print(strArr)
		#print(">handleSerialReadyRead: ", inStr)
		#self.appendLogString(" <-      " + str(strArr))
		parseInputData(self, strArr)				

	
	def handleSerialReadyReadOld(self, ind):
		print("handleSerialReadyRead")
		strArr = serialArr[ind].readAll()
		for x in strArr:
			print(x)
		print(bytes(strArr))
		ardString = strArr.data().decode("utf-8")
		ardArray = ardString.split(',')
		#print("ardArray:", ardArray)
		print(">handleSerialReadyRead:", serialArr[ind].portName(), " ->",ardArray)
		if len(ardArray) == 4:
			ident = -1
			try: 
				ident = int(ardArray[0])
			except ValueError:
				print("error parse ident:",ardArray[0])
				return False

			#print("ident",ident)		
			if not (ident>=0 and ident<8) :
				print("response ident err! ident=", ident)
				return
			#mObj = maketObjArr[ident]
			#mObj.groupBox.setEnabled(True)
			#mObj.comPort = serialArr[ind]
			#mObj.lineEditStat.setStyleSheet("""QLineEdit { background-color: green; color: white }""")
			##mObj.groupBox.setStyleSheet("QGroupBox { background-color: green; }")
			#mObj.lineEditStat.setText(serialArr[ind].portName())
			
			#mObj.lineLights.setText(ardArray[2])
			#mObj.lineLights.setText(ardArray[3])
			#if ardArray[2]=="ON" :
			#else :
			#	mObj.lineDemo
			# if ardArray[3].startswith("ON"):
				# mObj.lineDemo.setText("ON")
			# elif ardArray[3].startswith("OFF") :
				# mObj.lineDemo.setText("OFF")

				#value = ardArray[1]
			#if key == "ident" :
			#	ardInd=int(value)
			#	print(ind,">process bytes:", ardString, " conv:", ardInd)
			#	self.ardMapComs[ardInd] = serialArr[ind]
			#	print(self.ardMapComs)
			#	print("ardInd:", ardInd, "ind:", ind)
			#	
			#elif key == "stat" :
			#	print("stat:", repr(ardString))
		elif ardString == "OK\r\n":
			print("OK recvd")
		else :
			print("error array split")
		
		#print(ind,">process bytes:", serial.readAll())
		
	def readPendingDatagrams(self):
		while self.udpSocket.hasPendingDatagrams():
			(data, sender, senderPort) = self.udpSocket.readDatagram(50)
			print("UDP: {0}:{1} -> {2}".format(sender.toString(), senderPort, data))
			data = data.decode().lower().split(' ');
			#print(data)
			if(data[1] == "off") :
				print("UDP OFF")
				ind = int(data[0])
				self.handleCommandButton(ind, b'OFF\r\n')
			if(data[1] == "on") :
				print("UDP OFF")
				ind = int(data[0])
				self.handleCommandButton(ind, b'ON\r\n')
			
	def handleDemoBut(self, ind) :
		print("handleDemoBut=",ind)
		mObj = maketObjArr[ind]
		if mObj.comPort == None :
			print("no port open")
		else :
			print("start ind:", ind," ardMapComs:", mObj.comPort)
			print("write:", mObj.comPort.write(b'demo ON\r\n'));
		
		#print("self.ardMapComs.keys():", self.ardMapComs.keys())
		#print("self.ardMapComs[ind]:", self.ardMapComs.get(ind))
		#comPort = self.ardMapComs.get(ind)		
	
	def handleLightsBut(self, ind) :
		print("handleLightsBut=",ind)
		mObj = maketObjArr[ind]
		if mObj.comPort == None :
			print("no port open")
		else :
			print("start ind:", ind," ardMapComs:", mObj.comPort)
			print("write:", mObj.comPort.write(b'lights ON\r\n'));
		
	def handleCommandButton(self, ind, cmd) :
		print("handleCommandButton=",ind, "with cmd:",cmd)
		mObj = maketObjArr[ind]
		if mObj.comPort == None :
			print("no port open")
		else :
			mObj.comPort.write(cmd)
			#print("write:", );
	
	setADCPropsWindow = None
	comboPortAdcNum = None
	periodComboBox = None
	minBorderU1LineEdit = None
	minBorderU2LineEdit = None
	dataPeriod = None
		
	def closeEvent(self, event):
		#if self.setPortModeWindow != None :
		#	self.setPortModeWindow.close()
		if self.setADCPropsWindow != None :
			self.setADCPropsWindow.close()
		
	'''def tickGetDepTimer(self) :
		if self.depNum < dep.DEPENDENS_COUNT :
			self.depNum += 1
			self.sendQuery(GET_PROP, [0x0F, self.depNum], False)			
			self.depQuerySended = True
			QTimer().singleShot(10, self.tickGetDepTimer)
		else :
			self.depNum = 0
			self.depQuerySended = False'''
	
		
	#def setDepTableVal(self, data) :
	#	depNum = int.from_bytes(data[10], byteorder='little')
	#	reasonPort = int.from_bytes(data[11], byteorder='little')
	#	reasonCode = int.from_bytes(data[12], byteorder='little')
	#	threshold = int.from_bytes(data[13:15], byteorder='little')
	#	actionPort = int.from_bytes(data[15], byteorder='little')
	#	actionCode = int.from_bytes(data[16:17], byteorder='big')
	#	timeout =  int.from_bytes(data[17:18], byteorder='big')
	#	bEna = int.from_bytes(data[18], byteorder='big')
	#	print("setDepTable " + str(depNum) + " bEna:" + str(bEna) + " rp:" + str(reasonPort) + " rc:" + str(reasonCode) + 
	#							" ap:" + str(actionPort) + " ac" + str(actionCode)) # + " : " + str(data)
	#	
	#	dep.setDependence(self, depNum, reasonPort, reasonCode, actionPort, actionCode, bEna, timeout)
		
	def handleSetTime(self):
		currentDT = datetime.datetime.now()
		#print ("Current Year is: %d" % currentDT.year)
		#print ("Current Month is: %d" % currentDT.month)
		#print ("Current Day is: %d" % currentDT.day)
		#print ("Current Hour is: %d" % currentDT.hour)
		#print ("Current Minute is: %d" % currentDT.minute)
		#print ("Current Second is: %d" % currentDT.second)
		#print ("Current Microsecond is: %d" % currentDT.microsecond)
		h = currentDT.hour
		m = currentDT.minute
		s = currentDT.second
		self.sendQuery(SET_PROP, [0xfc, h, m, s])
			
			

	def handleCheckBoxDataRush(self, bEn):
		self.ui.lineEditDataRush.setEnabled(bEn)
		#self.ui.horizontalSliderDataRush.setEnabled(bEn)
		v = self.ui.horizontalSliderDataRush.value()
		self.ui.lineEditDataRush.setText(str(v))

		if bEn==True :
			self.timerDataRush = QTimer()
			self.timerDataRush.timeout.connect(self.handleDataRushTimer)
			v = self.ui.horizontalSliderDataRush.value()
			self.timerDataRush.setInterval(int(1000/v))
			self.timerDataRush.start()
		else :
			self.timerDataRush.stop()
		
	def handleHorizontalSliderDataRush(self, val) :
		self.ui.lineEditDataRush.setText(str(val))
		if(hasattr(self, 'timerDataRush')) : 
			self.timerDataRush.setInterval(int(1000/val))
	
	dimmerVal = [0,0,0,0,0]
	dimmerCurInd=1
	def handleDataRushTimer(self) :
		#self.sendDimmerState(self.dimmerCurInd, 1, self.dimmerVal[self.dimmerCurInd-1])
		self.sendQuery(GET_PROP, [0xfe])
		return
		self.sendDimmerState(1, 1, self.dimmerVal[0])
		
		self.dimmerVal[self.dimmerCurInd-1] += 1
		if self.dimmerVal[self.dimmerCurInd-1] > 255 :
			self.dimmerVal[self.dimmerCurInd-1] = 0
		self.dimmerCurInd+=1
		if(self.dimmerCurInd>5) :
			self.dimmerCurInd = 1
			
	def handleUpdateIButton(self):	
		for i in range(11) :
			self.sendQuery(GET_PROP, [0x0A, i+1], IBUT_CLASS+i)
	def handleUpdateIButtonEepromKeys(self):	
		for i in range(10) :
			self.sendQuery(GET_PROP, [0x0D, i+1], IBUT_EEPROM_CLASS+i)	
	def handleIButtonSaveFromCR(self):	
		portNum = self.ui.spinBoxIButtonPortNum.value()
		keyNum = self.ui.spinBoxIButtonKeyNum.value()
		#print("sp:" + str(self.ui.spinBoxIButtonKeyNum.value()))
		self.sendQuery(GET_PROP, [0x0B, portNum, keyNum])	
		

	def handleButtonTcpConnect(self):
		if(self.ui.pushButtonTcpConnect.text() == "connect") :
			self.ui.lineEditTcpAddr.setEnabled(False)
			self.ui.lineEditTcpPort.setEnabled(False)
			tcpAddr = self.ui.lineEditTcpAddr.text()
			tcpPort = self.ui.lineEditTcpPort.text()
			print("connecting to: " + tcpAddr + ":" + tcpPort)
			self._socket = QTcpSocket()
			#self._socket.connectToHost(QHostAddress.LocalHost, self.port)
			settings.setValue("tcpAddr", tcpAddr)
			settings.setValue("tcpPort", tcpPort)			
			self.ui.pushButtonTcpConnect.setText("disconnect")
			
			self._socket.connected.connect(self.handleSocketConnected)
			self._socket.disconnected.connect(self.handleSocketDisconnected)
			#self._socket.error.connect(self.handleSocketError)  #!!!!! add socket error handler
			self._socket.readyRead.connect(self.handleSocketReadyRead)
			self._socket.connectToHost(tcpAddr, int(tcpPort))
			
			self.appendConnectLogString("try to connect to " + str(tcpAddr) + ":" + str(tcpPort) )
			
		elif(self.ui.pushButtonTcpConnect.text() == "disconnect") :
			self.ui.pushButtonTcpConnect.setText("connect")
			self.ui.lineEditTcpAddr.setEnabled(True)
			self.ui.lineEditTcpPort.setEnabled(True)
			self._socket.close()
			settings.setValue("tcpSocketConnected", False)
			
	
	def handleSocketConnected(self):
		print("tcp socket connected")
		settings.setValue("tcpSocketConnected", True)
		self.appendConnectLogString("tcp socket connected to " + str(self._socket.peerAddress().toString())+ ":" + str(self._socket.peerPort()))
	def handleSocketDisconnected(self):
		print("tcp socket disconnected")
		self.appendConnectLogString("tcp socket disconnected")
	def handleSocketError(self):
		print("tcp socket error")
		self.appendConnectLogString("tcp socket error")
	def handleSocketReadyRead(self):
		#print("socket ready read")
		strArr = self._socket.readAll()
		if self.modem:
			self.modem.processChar(strArr)
		parseInputData(self, strArr)
		
	
	#==============	 radio     ==============	
	def handlePushButtonRadioSend(self):
		print("llala")
		sendList = bytes([0x55, 0x00, 0x48, 0x00, 0x00] )
		crc16 = self.crc16(sendList, 0, len(sendList))
		sendList = bytes(sendList + crc16.to_bytes(2, byteorder="big"))		
		#sendListStr = str(sendList)
		#sendListBa = sendListStr.encode()
		for i in range(len(sendList)):
			print(hex(sendList[i]))
		print(str(len(sendList)))
		#self.sendToComList.append(sendList)
		iSend = comPort.write(sendList)
	

#def main():
app = QApplication(sys.argv)  # Новый экземпляр QApplication
window = SmartBoxMonApp()  # Создаём объект класса ExampleApp
	
	#window = QMainWindow()
window.show()  # Показываем окно
app.exec()  # и запускаем приложение

#if __name__ == '__main__':  # Если мы запускаем файл напрямую, а не импортируем
#	#print(sys.argv)
#	for arg in sys.argv :
#		print(arg)
#	#if len(sys.argv) > 1:
#		main()  # то запускаем функцию main()
	
