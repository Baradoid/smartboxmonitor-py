import struct
from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QTableWidgetItem
import dependence as dep
import discrete as dscr
import rele
import traceback
import dimmer,pwm
import dht
import dali, portMode

GET_PROP = 0
SET_PROP = 1
EVENT_PROP = 2

DISCRETE_CLASS =  0x01010100
ADC_CLASS = 	  0x01010200
SENSORDS8_CLASS = 0x01010300
DHT_CLASS = 	  0x01010500
IBUT_CLASS = 	  0x01010A00
IBUT_EEPROM_CLASS = 0x01010D00

c21_CLASS = 	  0x01012100
c22_CLASS = 	  0x01012200
c23_CLASS = 	  0x01012300
c24_CLASS = 	  0x01012400
c26_CLASS = 	  0x01012600
c25_CLASS = 	  0x01012500

DEPS_CLASS =      0x01010f00
cF6_CLASS = 	  0x0101f600
cFB_CLASS = 	  0x0101fB00
cFD_CLASS = 	  0x0101fD00
cFE_CLASS = 	  0x0101fE00

DEBUG_PRINT = 0
def setParserDebugPrint(b) :
	global DEBUG_PRINT
	DEBUG_PRINT = b
	

def setTableCell(table, row, cell, text):
	twi = QTableWidgetItem(text)
	twi.setTextAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
	table.setItem(row, cell, twi)
		
class TSqiMessage:
	ver = 0
	flag = 0
	ident = 0
	len = 0
	cmd = 0
	errCode = 0
	body = []
	crc16 = 0
	raw = []
	
recvUartBufByteArr = bytearray()
#state = ParserState.SfKey
def parseInputData(self, strArr):
	global recvUartBufByteArr
	sendListHexStr = ""
	for sb in strArr:
		#print(sb)
		#sendListHexStr += " " + "%0.2X" %  sb  #hex(sb)
		sendListHexStr += " " + "%0.2X" %  int.from_bytes(sb, byteorder='little')  #hex(sb)
	if DEBUG_PRINT:
		print("recvd hex: " + sendListHexStr)
	
	recvUartBufByteArr+=strArr
	
	#print("recvd hex ba: " + str(self.recvUartBufByteArr))

	lw = self.lenWidth
	#try:
	while True:
		if DEBUG_PRINT and  len(recvUartBufByteArr) > 0:
			print("parse data: "  + ' '.join(x.hex() for x in recvUartBufByteArr))
		keyInd = recvUartBufByteArr.indexOf(b'\x7e')
		if(keyInd < 0) :
			return
		#print("key detected")
		arrLen = recvUartBufByteArr.length()
		if(arrLen < (keyInd+9+lw)) :
			return
		
		if DEBUG_PRINT :
			print("Parser keyInd " + str(keyInd) + " recv arr len: " + str(arrLen) )

		#respFlag = int.from_bytes(msgBaArr[2], 'little')
		#respCmd = int.from_bytes(msgBaArr[8], 'little')
		#respErrCode = int.from_bytes(msgBaArr[9], 'little')
		#self.appendResponse(identString, "&lt;- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sendListHexStr + ("  event!" if respFlag==2 else "") )
		
		#print("lenWidth: " + str(self.lenWidth))
		sqi = TSqiMessage()
		sqi.flag = int.from_bytes(recvUartBufByteArr[keyInd+2], 'little')
		sqi.ident = int.from_bytes(recvUartBufByteArr[keyInd+3:keyInd+7], 'big')
		sqi.len = int.from_bytes(recvUartBufByteArr[keyInd+7:keyInd+7+lw], 'big')
		#print("!!!! bodyLen: " + hex(sqi.len))
		sqi.cmd  = int.from_bytes(recvUartBufByteArr[keyInd+7+lw], 'little')
		sqi.errCode = respCode = int.from_bytes(recvUartBufByteArr[keyInd+8+lw], 'little')

		msglastInd = keyInd+sqi.len+2+7+lw - 1 #crc=2 header=8
		if DEBUG_PRINT :
			print("Parser body len: " + str(sqi.len) + " msglastInd:" + str(msglastInd))
		if  msglastInd>(arrLen-1) :	
			return
		if DEBUG_PRINT :
			print("Parser  arr:" + str(recvUartBufByteArr[keyInd]) +"  " +
				  " msglastInd:" + str(recvUartBufByteArr[msglastInd]))
		if sqi.flag == 2: #event
			sqi.body = recvUartBufByteArr[keyInd+8+lw:msglastInd-1]
		else:
			sqi.body = recvUartBufByteArr[keyInd+9+lw:msglastInd-1]
		sqi.raw =  recvUartBufByteArr[keyInd:msglastInd+1]
		if DEBUG_PRINT and  len(recvUartBufByteArr) > 0:
			print("parse raw msg: "  + ' '.join(x.hex() for x in sqi.raw ))
		recvUartBufByteArr.remove(0,msglastInd+1)
		try:
			processMessage(self, sqi)
		except Exception as e:
			print("ProcessMessage fall with exception: " + str(e))
			print(traceback.format_exc())

	# respFlag = int.from_bytes(strArr[2], 'little')

	# self.appendLogString(" <-      " + str(sendListHexStr) + ("  event!" if respFlag==2 else "") )
	
	# #else :
	# #	self.appendLogString(" <-      " + str(sendListHexStr) )
	
	# if(len(strArr) < 11) :
		# return

	# #respLen = int.from_bytes(strArr[7], 'little')
	# respLen = int.from_bytes(msgBaArr[7:9], 'big')
	# respCode = int.from_bytes(strArr[9], 'little')
	# #if(len(strArr) > respLen) :
	# #	print("strLen:" + str(len(strArr)) + "  l:" + str(respLen) + "  arr:" + str(strArr[:respLen+10]))
	# while len(strArr[:respLen+11])>0:
		# #print("arr:" + str(strArr[:respLen+10]))
		# self.dataProcessor(strArr[:respLen+11])
		# strArr = strArr[respLen+11:]
	# #except IndexError:
	# #	print ("IndexError: sequence index out of range")
	
def processMessage(self, sqi):
	recvIntArr = []
	for sb in sqi.raw:
		recvIntArr  += [int.from_bytes(sb, byteorder='little') ]
	sendListHexStr = self.appendColorTegs(recvIntArr)
	identString = self.getIdentAsString(recvIntArr)
	
	
	suffix = ""
	if sqi.errCode != 0xff :
		suffix = "<font color=\"red\"> -- err! </font>"
	
	if DEBUG_PRINT :
		print("processMessage flag:" +str(sqi.flag) +" cmd:" + hex(sqi.cmd) +" err:" + hex(sqi.errCode) + " body:"  + ' '.join(x.hex() for x in sqi.body))
	
	eventBody = ""
	if sqi.flag==2:
		eventType = ""
		if sqi.cmd == 1:
			eventType = "discrete"
			iCh = int.from_bytes(sqi.body[0], 'little')
			pushCount = int.from_bytes(sqi.body[6], 'little')
			pushDuration = int.from_bytes(sqi.body[7:9], 'big')				
			state = int.from_bytes(sqi.body[9], 'little')
			eventBody = "chNum:" + str(iCh+1) + " state:" + str(state)+" pushCount:" + str(pushCount) + "   pushDuration:" + str(pushDuration) + " ms"
		elif sqi.cmd == 2:
			eventType = "adc"
		elif sqi.cmd == 3:
			eventType = "ds19b20"	
			portId = sqi.errCode #int.from_bytes(sqi.errCode, 'little')
			res = int.from_bytes(sqi.body[1], 'little')
			temp = struct.unpack('>h', sqi.body[2:4])[0]/100.
			eventBody = "portId:" + str(portId) + " res:" + str(res) + " temp:" + str(temp)
		elif sqi.cmd == 4:
			eventType = "DHT"			
			portId = int.from_bytes(sqi.body[0], 'little')
			hum = struct.unpack('>h', sqi.body[2:4])[0]
			temp = struct.unpack('>h', sqi.body[4:6])[0]
			eventBody = "portId:" + str(portId) + " rh:" + str(hum/100) + " temp:" + str(temp/100)
		elif sqi.cmd == 5:
			releId = int.from_bytes(sqi.body[0], 'little')
			releState = int.from_bytes(sqi.body[1], 'little')
			eventType = "rele " + str(releId) + " " + ("on " if releState==1 else "off ")
		elif sqi.cmd == 0x0a:
			eventType = "iButton"
		elif sqi.cmd == 0x21:
			eventType = "SensorDS8"						
		elif sqi.cmd == 0x33:
			eventType = "Dimmer ena"
			eventBody = "ch " + str(int.from_bytes(sqi.body[0], 'little')) + " ena " + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.cmd == 0x34:
			eventType = "Dimmer level"
			eventBody = "ch " + str(int.from_bytes(sqi.body[0], 'little')) + " level " + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.cmd == 0x36:
			eventType = "Dimmer discr level"
			eventBody = "ch " + str(int.from_bytes(sqi.body[0], 'little')) + " discr " + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.cmd == 0x42:
			eventType = "PWM"
			eventBody = "ch " + str(int.from_bytes(sqi.body[0], 'little')) + " ena:" + str(int.from_bytes(sqi.body[1], 'little'))
		elif sqi.cmd == 0x43:
			eventType = "PWM"
			eventBody = "ch " + str(int.from_bytes(sqi.body[0], 'little')) + " level:" + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.cmd == 0xf:
			eventType = "Dependence"
			depNum = int.from_bytes(sqi.body[0], byteorder='little')
			reasonPort = int.from_bytes(sqi.body[1], byteorder='little')
			reasonCode = int.from_bytes(sqi.body[2], byteorder='little')
			reasonThr = int.from_bytes(sqi.body[3:5], byteorder='big')
			actionPort = int.from_bytes(sqi.body[5], byteorder='little')
			actionCode = int.from_bytes(sqi.body[6], byteorder='little')
			actionProp = int.from_bytes(sqi.body[7], byteorder='little')
			actionTimeout =  int.from_bytes(sqi.body[8:10], byteorder='big')
			bEna = int.from_bytes(sqi.body[10], byteorder='big')
			tag =  int.from_bytes(sqi.body[11:13], byteorder='big')
			eventBody = "depNum:" + str(depNum) + " ena:" +str(bEna) + " tag:" + str(tag)
		elif sqi.cmd == 0x74:
			eventType = "Dali"
			daliDevCount = int.from_bytes(sqi.body[0], byteorder='little')
			eventBody = "Dali device count: " + str(daliDevCount)
		elif sqi.cmd == 0x75:
			eventType = "Dali"
			unitInd = int.from_bytes(sqi.body[0], byteorder='little')
			ena = int.from_bytes(sqi.body[1], byteorder='little')
			eventBody = "unit " + str(unitInd) + (" enable" if ena else " disable")
		elif sqi.cmd == 0x76:
			eventType = "Dali"
			unitInd = int.from_bytes(sqi.body[0], byteorder='little')
			level = int.from_bytes(sqi.body[1], byteorder='little')
			eventBody = "unit " + str(unitInd) + " level: "+ str(level)
		elif sqi.cmd == 0x79:
			eventType = "Dali"
			groupNum = int.from_bytes(sqi.body[0], byteorder='little')
			ena = int.from_bytes(sqi.body[1], byteorder='little')
			eventBody = "group " + str(groupNum) + (" enable" if ena else " disable")
		elif sqi.cmd == 0x7A:
			eventType = "Dali"
			groupNum = int.from_bytes(sqi.body[0], byteorder='little')
			level = int.from_bytes(sqi.body[1], byteorder='little')
			eventBody = "group " + str(groupNum) + " level: "+ str(level)

		
		self.appendLogString(sendListHexStr + " &nbsp;&nbsp;&nbsp;&nbsp;" + eventType+ "  event! " + eventBody)
	elif self.ui.checkBoxOneLineResponse.isChecked():
		self.appendResponse(identString, sendListHexStr + " &nbsp;&nbsp;&nbsp;&nbsp;" + suffix)
	else:
		if sqi.flag == GET_PROP and sqi.cmd == 0x2 and sqi.errCode == 0xff:
			adcVal = struct.unpack('>h', sqi.body[2:4])[0]
			suffix = " adc chInd " + str(int.from_bytes(sqi.body[0], 'little')) +" val: " + str(adcVal)
		elif sqi.flag == GET_PROP and sqi.cmd == 0x5 and sqi.errCode == 0xff:
			suffix = " rele ch " + str(int.from_bytes(sqi.body[0], 'little')) + " state: " + ("on" if int.from_bytes(sqi.body[1], 'little')==1 else "off")
		elif sqi.flag == GET_PROP and sqi.cmd == 0x7 and sqi.errCode == 0xff:
			suffix = " rele state: " + ",".join( [("on" if int.from_bytes(sqi.body[0 + i], 'little')==1 else "off") for i in range(16)])
		elif sqi.flag == GET_PROP and sqi.cmd == 0x41 and sqi.errCode == 0xff:
			suffix = " PWM ch " + str(int.from_bytes(sqi.body[0], 'little')) + " min: " + str(int.from_bytes(sqi.body[1], 'little')) + " max: " + str(int.from_bytes(sqi.body[2], 'little'))
		elif sqi.flag == GET_PROP and sqi.cmd == 0x42 and sqi.errCode == 0xff:
			suffix = " PWM ch " + str(int.from_bytes(sqi.body[0], 'little')) + " ena:" + str(int.from_bytes(sqi.body[1], 'little'))
		elif sqi.flag == GET_PROP and sqi.cmd == 0x43 and sqi.errCode == 0xff:
			suffix = " PWM ch " + str(int.from_bytes(sqi.body[0], 'little')) + " level:" + str(int.from_bytes(sqi.body[1], 'little')) 

		elif sqi.flag == GET_PROP and sqi.cmd == 0x32 and sqi.errCode == 0xff:  #getVer
			min = int.from_bytes(sqi.body[1], 'little')
			max = int.from_bytes(sqi.body[2], 'little')
			suffix = " Dimmer ch" + str(int.from_bytes(sqi.body[0], 'little')) + " min " + str(min) + " max " + str(max)
		elif sqi.flag == GET_PROP and sqi.cmd == 0x33 and sqi.errCode == 0xff:
			suffix = " Dimmer ch " + str(int.from_bytes(sqi.body[0], 'little')) + " ena " + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.flag == GET_PROP and sqi.cmd == 0x34 and sqi.errCode == 0xff:
			suffix = " Dimmer ch " + str(int.from_bytes(sqi.body[0], 'little')) + " level " + str(int.from_bytes(sqi.body[1], 'little')) 
		elif sqi.flag == GET_PROP and sqi.cmd == 0x36 and sqi.errCode == 0xff:
			suffix = " Dimmer ch " + str(int.from_bytes(sqi.body[0], 'little')) + " discr " + str(int.from_bytes(sqi.body[1], 'little')) 
		
		elif sqi.flag == GET_PROP and sqi.cmd == 0x35 and sqi.errCode == 0xff:
			suffix = " Dimmer ch " + str(int.from_bytes(sqi.body[0], 'little')) + " light up ms: "  + str(100*int.from_bytes(sqi.body[1], 'little')) 
			
		elif sqi.flag == GET_PROP and sqi.cmd == 0xFD and sqi.errCode == 0xff:  #getVer
			y = int.from_bytes(sqi.body[0], 'little')
			m = int.from_bytes(sqi.body[1], 'little')>>4
			d = ((int.from_bytes(sqi.body[1], 'little')&0xf)<<4) | ((int.from_bytes(sqi.body[2], 'little')>>4)&0xf)
			timeVal = ((int.from_bytes(sqi.body[2], 'little')&0xf)<<8) | int.from_bytes(sqi.body[3], 'little') 
			h = int(timeVal/100)
			min = (timeVal-h*100)
			suffix = " get ver: " +  str(y) + "-" + str(m) + "-" + str(d) + " " + '%02d'%(h) +":"+'%02d'%(min)

		elif sqi.flag == GET_PROP and sqi.cmd == 0xFB and sqi.errCode == 0xff:  #getVer
			suffix = " get dev ident"
			
		elif sqi.flag == GET_PROP and sqi.cmd == 0xFE and sqi.errCode == 0xff:  #getUptime
			uptime = int.from_bytes(sqi.body[1:5], 'big')
			suffix = " get uptime: " + str(uptime) + " sec" 
			
		elif sqi.flag == GET_PROP and sqi.cmd == 0x75 and sqi.errCode == 0xff:  #getVer
			suffix = " Dali unit ena"
		elif sqi.flag == GET_PROP and sqi.cmd == 0x76 and sqi.errCode == 0xff:  #getVer
			suffix = " Dali unit level"
		elif sqi.flag == GET_PROP and sqi.cmd == 0x77 and sqi.errCode == 0xff:  #getVer
			suffix = " Dali unit member of groups: "
			val = int.from_bytes(sqi.body[1:3], 'big')
			for i in range(16):
				if val&0x1: 
					suffix += str(i) + " "
				val >>= 1
			#print("Dali group: " + hex(val))
		self.appendLogString("&lt;- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sendListHexStr + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + suffix)
		
	#self.appendLogString("&lt;- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + sendListHexStr + ("  event!" if respFlag==2 else "") )
	
	dataProcessor(self, sqi)
	
	
def dataProcessor(self, sqi):
	
	if DEBUG_PRINT :
		print("flag:" + str(sqi.flag) + " len:" +str(sqi.len) + " respCmd:" +hex(sqi.cmd) + " respCode:" + hex(sqi.errCode)+ " ident:" + hex(sqi.ident) ) #'''+ " arr:" + str(strArr)''' 

	if sqi.flag != EVENT_PROP and sqi.errCode != 0xff:
		#print("answer with err")
		print("answer with err " + "flag:" + str(sqi.flag) + " len:" +str(sqi.len) + " respCmd:" +hex(sqi.cmd) + " respCode:" + hex(sqi.errCode)+ " ident:" + hex(sqi.ident) )
	elif sqi.flag == GET_PROP :   #get prop
		#print("processGetData")
		processGetData(self, sqi)
	elif sqi.flag == SET_PROP : #set prop
		if DEBUG_PRINT :
			print("set prop ans")
	elif sqi.flag == EVENT_PROP :
		processEvents(self, sqi);
	else:
		print ("unknown answer")
		#print ("answer with error " + hex(respCode))
		
		
def processEvents(self, sqi, ):
	#print("flag:" + str(sqi.flag) + " len:" +str(sqi.len) + " respCmd:" +hex(sqi.cmd) + " respCode:" + hex(sqi.errCode)+ " ident:" + hex(sqi.ident) ) #'''+ " arr:" + str(strArr)''' 
	if sqi.cmd == 0x01:
		pInd = int.from_bytes(sqi.body[0], 'big') #int.from_bytes(sqi.body[0], 'little')

		count = int.from_bytes(sqi.body[3:6], 'big')
		state = int.from_bytes(sqi.body[9], 'little')
		#print("ev: " + str(portNum) + " " + hex(count) + " " + str(state))			
		setTableCell(self.ui.tableWidgetDiscrete, pInd, 3, str(count))						
		setTableCell(self.ui.tableWidgetDiscrete, pInd, 4, str(state))			
	elif sqi.cmd == 0x02 :
		portNum = sqi.errCode
		adcVal = struct.unpack('>h', sqi.body[0:2])[0]
		#signalState = int.from_bytes(strArr[12], 'little')
		adcPortId = portNum-23
		#print("ADC EVENT " + str(portNum) + " adcVal:"+ str(adcVal))
		setTableCell(self.ui.tableWidgetADC, adcPortId, 2, str(adcVal))						
	elif sqi.cmd == 0x03:
		#print("ds18b20 event")
		portId = sqi.errCode #int.from_bytes(sqi.errCode, 'little')
		res = int.from_bytes(sqi.body[1], 'little')
		temp = struct.unpack('>h', sqi.body[2:4])[0]/100.
		#state = int.from_bytes(strArr[13], 'little')
		if DEBUG_PRINT:
			print("ds19b20 event data processor + " + str(portId) + "  " + str(res) + " " + str(temp)) # '''+ " state:" + str(state)+"("+hex(state)+")"''')
		r = portId
		setTableCell(self.ui.tableWidgetDht, r, 0, "DS")
		#setTableCell(self.ui.tableWidgetDht, r, 2, hex(state))
		setTableCell(self.ui.tableWidgetDht, r, 3, str(res))
		setTableCell(self.ui.tableWidgetDht, r, 4, str(temp))
		setTableCell(self.ui.tableWidgetPorts, r, 2, str(temp))
		setTableCell(self.ui.tableWidgetPorts, r, 0, "DS")			
	elif sqi.cmd == 0x04 :
		dht.parseDhtEvents(self, sqi)
		
	elif sqi.cmd == 0x05:  
		print("rele event " +str(sqi.errCode) + "  "+ str(sqi.body) )
		releInd = sqi.errCode
		releState = int.from_bytes(sqi.body[1], byteorder='little') == 1
		#print(" releInd" + str(releInd)+ " releState:"+str(releState) )
		if (releInd >= 0) and (releInd < 22) :
			rele.releCheckBoxes[releInd].setChecked(releState)		
			
	elif sqi.cmd == 0x0f:
		#print("dep event")
		dep.parseDependenceEvent(self,sqi)

	elif sqi.cmd == 0x33:
		dimmer.parseDimmerEvent(sqi)
	elif sqi.cmd == 0x34:
		dimmer.parseDimmerEvent(sqi)
	elif sqi.cmd == 0x36:
		dimmer.parseDimmerEvent(sqi)
	elif sqi.cmd == 0x43:
		pass #print("pwm props change event")
	elif sqi.cmd == 0x4d:
		print("IRGate event")
		#print(sqi.body)
		
		#irStr = []
		#for c in sqi.body:
			#irStr += [c]
			#if c == b'0':
			#	break
		#irText = sqi.body.data().decode("utf-8")
		irText = sqi.body.data().decode("unicode_escape")
		
		#print("irText: " + irText)
		zeroInd = irText.find("\x00")
		#print("irText: " + str())
		irText = irText[:zeroInd]
		irText =  repr(irText)
		print("irText: " + irText)
		
		self.ui.textEditIRGateEventMsg.setText( irText)
	elif sqi.cmd == 0x74:
		daliDevCount = int.from_bytes(sqi.body[0], byteorder='little')
		self.ui.lineEditDaliCount.setText(str(daliDevCount))
	
	else:
		pass
		#print("unknown event")
		#print("unknown event  " + "flag:" + str(respFlag) + " len:" +str(respLen) + " respCmd:" +hex(respCmd) + " respCode:" + hex(respCode)+ " ident:" + hex(ident) )
	


def processGetData(self, sqi):
	if DEBUG_PRINT :
		print("processGetData cmd:" + hex(sqi.cmd) + " body:"  + ' '.join(x.hex() for x in sqi.body))

	if sqi.cmd==0x04 :
		dht.parseDhtGet(self, sqi)
	elif sqi.cmd == 0x0f:
		dep.parseDependenceGet(self, sqi)

	elif (sqi.cmd==0xf4) : 
		cntArr = struct.unpack('>' +'I'*(len(sqi.body)//(4)), sqi.body)
		
		print("TASK monitor data:" + ' '.join(hex(x) for x in cntArr))
		setTableCell(self.ui.tableWidgetTaskMon, 0, 0, "portTask")				
		setTableCell(self.ui.tableWidgetTaskMon, 0, 1, str(cntArr[0]))
		
		setTableCell(self.ui.tableWidgetTaskMon, 1, 0, "dicretePortTask")				
		setTableCell(self.ui.tableWidgetTaskMon, 1, 1, str(cntArr[1]))
		
		setTableCell(self.ui.tableWidgetTaskMon, 2, 0, "dimmerTask")
		setTableCell(self.ui.tableWidgetTaskMon, 2, 1, str(cntArr[2]))
		
		setTableCell(self.ui.tableWidgetTaskMon, 3, 0, "pwmTask")
		setTableCell(self.ui.tableWidgetTaskMon, 3, 1, str(cntArr[3]))
		
		setTableCell(self.ui.tableWidgetTaskMon, 4, 0, "adcTask")
		setTableCell(self.ui.tableWidgetTaskMon, 4, 1, str(cntArr[4]))
		
		#print("f4 arr:" + hex(sqi.cmd) + " arr: "  + ' '.join(hex(x) for x in cntArr))
		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 0, 0, "dimmerIrqCnt")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 0, 1, str(cntArr[6]))
		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 1, 0, "dimmerTim2UpdCnt")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 1, 1, str(cntArr[7]))
		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 2, 0, "dimmerTim2CCCnt0")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 2, 1, str(cntArr[8]))
		setTableCell(self.ui.tableWidgetDimmerMonitor, 3, 0, "dimmerTim2CCCnt1")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 3, 1, str(cntArr[9]))
		setTableCell(self.ui.tableWidgetDimmerMonitor, 4, 0, "dimmerTim2CCCnt2")			
		setTableCell(self.ui.tableWidgetDimmerMonitor, 4, 1, str(cntArr[10]))
		setTableCell(self.ui.tableWidgetDimmerMonitor, 5, 0, "dimmerTim2CCCnt3")			
		setTableCell(self.ui.tableWidgetDimmerMonitor, 5, 1, str(cntArr[11]))
		
		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 6, 0, "dimmerTim3UpdCnt")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 6, 1, str(cntArr[12]))
		setTableCell(self.ui.tableWidgetDimmerMonitor, 7, 0, "dimmerTim3CCCnt0")		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 7, 1, str(cntArr[13]))
		setTableCell(self.ui.tableWidgetDimmerMonitor, 8, 0, "dimmerTim3CCCnt1")			
		setTableCell(self.ui.tableWidgetDimmerMonitor, 8, 1, str(cntArr[14]))
		
		setTableCell(self.ui.tableWidgetDimmerMonitor, 9, 0, "dimmerIrqPeriodLessThenMask")			
		setTableCell(self.ui.tableWidgetDimmerMonitor, 9, 1, str(cntArr[15]))

		
	elif (sqi.cmd==0xf5) : 
		#print("0xf5")
		i2cErrs = int.from_bytes(sqi.body[0:4], byteorder='little')
		
		self.ui.lineEditI2cRestores.setText(str(i2cErrs))
		self.ui.lineEditI2cRestores_2.setText(str(i2cErrs))
		totalExch = [0]*11
		okExch = [0]*11
		errExch = [0]*11
		for i in range(11):
			#print("parse " + str(i))\
			te =  	int.from_bytes(sqi.body[4+10*i + 0: 4+10*i + 4], byteorder='little')
			oe = 	int.from_bytes(sqi.body[4+10*i + 4: 4+10*i + 8], byteorder='little')
			ee = 	int.from_bytes(sqi.body[4+10*i + 8: 4+10*i + 12], byteorder='little')
			nr = 	int.from_bytes(sqi.body[4+10*i + 12:4+10*i + 16], byteorder='little')
			retr=	int.from_bytes(sqi.body[4+10*i + 16:4+10*i + 20], byteorder='little')
			
			setTableCell(self.ui.tableWidgetDht, i, 5, str(te))
			setTableCell(self.ui.tableWidgetDht, i, 6, str(oe))
			setTableCell(self.ui.tableWidgetDht, i, 7, str(ee))
			setTableCell(self.ui.tableWidgetDht, i, 8, str(nr))
			setTableCell(self.ui.tableWidgetDht, i, 9, str(retr))
			totalExch[i] = te
			okExch[i] = oe
			errExch[i] = ee
			#print(str(i) +": totalExch: " +str(totalExch[i])+ " okExch: " +str(okExch[i]) + " errExch: " +str(errExch[i])+ " notresp: " +str(nr)+ " retr: " +str(retr))
	elif (sqi.cmd==0xf6) :  #or (sqi.ident&0xffffff00 ==  cF6_CLASS)
		portMode.parsePortModeGet(sqi)
	elif sqi.cmd ==  0xfb:  #sqi.ident&0xffffff00 ==  cFB_CLASS
		di = [0] * 5
		for i in range(4) :
			di[i] = int.from_bytes(sqi.body[i], byteorder='little')	
		self.ui.lineEditDevIdentifer.setText("%02X %02X %02X %02X " % (di[0], di[1], di[2], di[3]))
	elif sqi.cmd ==  0xfd: #sqi.ident&0xffffff00 ==  cFD_CLASS: 
		major = int.from_bytes(sqi.body[0], byteorder='little')
		protoVer = int.from_bytes(sqi.body[1], byteorder='little')
		minor = int.from_bytes(sqi.body[2], byteorder='little')
		mminor = int.from_bytes(sqi.body[3], byteorder='little')
		y = int.from_bytes(sqi.body[6], byteorder='little')
		self.ui.lineEditFmwVer.setText(str(major) +"-"+ str(protoVer) +"-" +str(minor) +"-" +str(mminor)  ) #'''+ "-20" + str(y)'''
	elif sqi.cmd==0xfe :  #or (sqi.ident&0xffffff00 ==  cFE_CLASS)
		uptime = int.from_bytes(sqi.body[:5], byteorder='big')
		s = uptime - (uptime//60)*60
		m = (uptime//60) - ((uptime//60)//60)*60
		h = uptime//3600
		self.ui.lineEditUpTime.setText("[%02d"%h + ":%02d"%m + ":%02d]"%s+ "  %d"%uptime )				
		
	if sqi.ident == SENSORDS8_CLASS+0x30:
		print("dht response")
		if respCode != 0xFF:
			return	
		portNum = int.from_bytes(strArr[9], 'little')
		state = int.from_bytes(strArr[22], 'little')
		twi = None
		if state != 0 :
			#rh = int.from_bytes(strArr[23:24], 'little')
			#temp = int.from_bytes(strArr[23:25], 'little')
			rh = struct.unpack('>h', strArr[21:23])[0]
			temp = struct.unpack('>h', strArr[23:25])[0]
			#print("dht temp " + hex(temp/10.))
			twi = QTableWidgetItem("EE")				
			setTableCell(self.ui.tableWidgetDht, portNum-1, 2, str(rh/10.))
			setTableCell(self.ui.tableWidgetDht, portNum-1, 4, str(temp/10.))				
		else :
			twi = QTableWidgetItem("")
		#twi.setTextAlignment(QtCore.Qt.AlignCenter)
		#self.ui.tableWidgetDht.setItem(portNum-1, 5, twi)
	
	elif  ((sqi.cmd==0x03) or (sqi.ident&0xffffff00 == SENSORDS8_CLASS)) :
		if sqi.errCode != 0xFF:
			return	
		#print("get ")
		portNum = int.from_bytes(sqi.body[0], 'little')
		mode = int.from_bytes(sqi.body[1], 'little')
		#modeStr = ""
		#if mode==0 : modeStr = "D"
		#elif mode==1: modeStr = "ds"
		#elif mode==2: modeStr = "IB"
		#elif mode==3: modeStr = "DHT"
		#elif mode==4: modeStr = "ADC"
		#elif mode==5: modeStr = "WF"
		
		#print(str(i))
		
		#self.ui.tableWidgetPorts.setItem(portNum-1, 0, twi)
		##self.ui.tableWidgetPorts.resizeColumnsToContents()
		##self.ui.tableWidgetPorts.verticalHeader().setDefaultSectionSize(self.ui.tableWidgetPorts.verticalHeader().minimumSectionSize())	
		
		#T1 = struct.unpack('>h', strArr[11:13])[0]
		#T2 = struct.unpack('>h', strArr[13:15])[0]
		#period = int.from_bytes(strArr[11], byteorder='little') 
		sensRes = int.from_bytes(sqi.body[2], 'little')
		temp = struct.unpack('>h', sqi.body[3:5])[0]
		
		if portNum <= 11:
			#print("pp: " + str(portNum))
			#setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 0, modeStr)
									
			setTableCell(self.ui.tableWidgetPorts, portNum-1, 2, str(temp/100))				
			setTableCell(self.ui.tableWidgetDht, portNum-1, 0, "DS")
			#setTableCell(self.ui.tableWidgetDht, portNum-1, 1, str(T1))
			#setTableCell(self.ui.tableWidgetDht, portNum-1, 2, str(T2))
			#setTableCell(self.ui.tableWidgetDht, portNum-1, 1, str(period))
			setTableCell(self.ui.tableWidgetDht, portNum-1, 2, "")
			setTableCell(self.ui.tableWidgetDht, portNum-1, 3, str(sensRes))
			setTableCell(self.ui.tableWidgetDht, portNum-1, 4, str(temp/100))
			
			#setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 1, str(vec))
			#setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 2, str(inv))

	elif sqi.ident&0xffffff00 ==  c22_CLASS:
		if respErrCode != 0xFF:
			return
		portNum = int.from_bytes(strArr[9], 'little')
		vec =  int.from_bytes(strArr[10], 'little')
		inv =  int.from_bytes(strArr[11], 'little')
		tempTreshold =  int.from_bytes(strArr[12:14], 'big')
		print("c22_CLASS port:" + str(portNum) + " vec:"+str(vec) +
			 " inv:" + str(inv) + " tempTresh:" + str(tempTreshold))
		
		setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 1, str(vec))
		setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 2, str(inv))
		setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 3, str(tempTreshold))
	elif sqi.ident&0xffffff00 ==  c23_CLASS:
		if respErrCode != 0xFF:
			return
		
	elif sqi.ident&0xffffff00 ==  c24_CLASS:
		if respErrCode != 0xFF:
			return
		portNum = int.from_bytes(strArr[9], 'little')
		state =  int.from_bytes(strArr[10], 'little')
		stateStr = ""
		if state==0 : stateStr = "D"
		elif state==1: stateStr = "E"						
		print("c24_CLASS port:" + str(portNum) + " state:"+str(stateStr))
		setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 0, stateStr)
		
	elif sqi.ident&0xffffff00 ==  c26_CLASS:
		if respErrCode != 0xFF:
			return
		portNum = int.from_bytes(strArr[9], 'little')
		tempTreshold =  int.from_bytes(strArr[10:12], 'little')
		print("c26_CLASS port:" + str(portNum) + " maxTempTresh:" + str(tempTreshold) )
		setTableCell(self.ui.tableWidgetSensorDS8, portNum-1, 7, str(tempTreshold))
		
#		elif ident ==  SENSORDS8_CLASS+0xF7:
#			if respCode != 0xFF:
#				return			
#			portNum = int.from_bytes(strArr[9], byteorder='little')
#			T1 = struct.unpack('>h', strArr[10:12])[0]
#			T2 = struct.unpack('>h', strArr[12:14])[0]
#			period = struct.unpack('>h', strArr[14:16])[0]
#			sensRes = int.from_bytes(strArr[16], 'little')
#			val = struct.unpack('>h', strArr[17:19])[0] #int.from_bytes(strArr[17:19], byteorder='big')
#			#print(str(val))
#			#tempDataArr = strArr[8:]
#			#print("int arr:" + str(len(tempDataArr)/2)+" :" + str(tempDataArr))
#			#tempInts = struct.unpack('<' +'H'*(len(tempDataArr)//2), tempDataArr)
#			#print("dsPresence:" + str(dsPresence))
#			#for i in range(8):				
#				#cellText = ""
#				#if((dsPresence>>i)&0x1) != 0 :
#				#	cellText = str(tempInts[i]) + "°"
#				#else :			
#			setTableCell(self.ui.tableWidgetDs19b20, portNum-1, 0, str(val/100))
#			setTableCell(self.ui.tableWidgetDs19b20, portNum-1, 1, str(T1))
#			setTableCell(self.ui.tableWidgetDs19b20, portNum-1, 2, str(T2))
		
	elif sqi.cmd == 0x02:  
		#print("ADC resp " + str(sqi.body[0]))
		if sqi.errCode != 0xFF:
			return		
		#portNum = int.from_bytes(sqi.body[0], 'little')
		portId = int.from_bytes(sqi.body[0], 'little')
		adcPortId = portId-22
		dataSendPeriod = struct.unpack('>h', sqi.body[1:3])[0]
		#minU1 = struct.unpack('>h', strArr[13:15])[0]
		#minU2 = struct.unpack('>h', strArr[15:17])[0]
		#dataSendPeriod = struct.unpack('>h', strArr[17:19])[0]
		adcVal = struct.unpack('>h', sqi.body[2:4])[0]
		
		#print("ADC CLASS " + str(portId) + "  " + str(adcPortId) + #" "+str(period)+ " "+str(minU1)+ " "+ #str(minU2)+ 
		#					" " +str(dataSendPeriod) + " adcVal:"+str(adcVal))
		
		#rh = struct.unpack('>h', strArr[21:23])[0]
		#temp = struct.unpack('>h', strArr[23:25])[0]
		#portId = ident&0xff
	
		setTableCell(self.ui.tableWidgetPorts, portId, 2, str(adcVal))
		
		r = adcPortId
		#setTableCell(self.ui.tableWidgetADC, r, 1, str(period))
		#setTableCell(self.ui.tableWidgetADC, r, 2, str(minU1))
		#setTableCell(self.ui.tableWidgetADC, r, 3, str(minU2))
		setTableCell(self.ui.tableWidgetADC, r, 1, str(dataSendPeriod))
		setTableCell(self.ui.tableWidgetADC, r, 2, str(adcVal))


	elif sqi.cmd==0x01 :   #or (sqi.ident&0xffffff00 ==  DISCRETE_CLASS)
		if sqi.errCode != 0xff:
			return
		portId = int.from_bytes(sqi.body[0], 'little')
		#state = int.from_bytes(strArr[19], 'little')
		#val = struct.unpack('<h', strArr[18:20])[0]
		#period = struct.unpack('>H', strArr[11:12])[0]
		period = int.from_bytes(sqi.body[1], 'little')
		mask = int.from_bytes(sqi.body[2], 'little')
		T = int.from_bytes(sqi.body[3:5], 'big')
		#resInts = struct.unpack('>' +'I'*(len(strArr[14:18])//4), strArr[14:18])
		pushDelay = struct.unpack('>H', sqi.body[5:7])[0]
		count = struct.unpack('>I', sqi.body[7:11])[0]
		state = int.from_bytes(sqi.body[11], 'little')
		
		#print("pushDelay" + hex(pushDelay))

		print("DISCRETE_CLASS " + str(portId) + " state:"+str(state) + " period:"+str(period) +
				" mask:"+str(mask)+" T:"+str(T) + " pushDelay:" + str(pushDelay) + " count:" + str(count))
		
		setTableCell(self.ui.tableWidgetPorts, portId, 2, str(state))
		
		#self.ui.tableWidgetPorts.item(1,2).setBackground(QtGui.QColor(100,100,150) )
		#self.ui.tableWidgetPorts.item(portId, 2).
		setTableCell(self.ui.tableWidgetDiscrete, portId, 0, str(period))		
		setTableCell(self.ui.tableWidgetDiscrete, portId, 1, format(mask, "02x"))			
		setTableCell(self.ui.tableWidgetDiscrete, portId, 2, str(T))
		setTableCell(self.ui.tableWidgetDiscrete, portId, 3, str(count))
		setTableCell(self.ui.tableWidgetDiscrete, portId, 4, str(state))
		setTableCell(self.ui.tableWidgetDiscrete, portId, 5, str(pushDelay))
					
		dscr.mask[portId] = mask
		dscr.per[portId] = period
		#self.ui.tableWidgetDiscrete.setStretchLastSection(true) #; resizeToContents()
		#resInts = struct.unpack('<' +'B'*(len(strArr[9:17])//1), strArr[9:17])
	elif sqi.ident&0xffffff00 ==  IBUT_CLASS:
		#errCode = int.from_bytes(strArr[8], 'little')
		if sqi.errCode != 0xff:
			return
		portNum = int.from_bytes(sqi.body[0], 'little')
		nowCon = int.from_bytes(sqi.body[1], 'little')			
		pInd = portNum-1
		#key = int.from_bytes(strArr[11:19], 'little')
		key = ""
		for i in range(8) :
			k = int.from_bytes(sqi.body[1+i], 'little')
			key += "%02x "%(k)
		
		#print("nc: " + str(nowCon)+" key:" + key)
		setTableCell(self.ui.tableWidgetIButton, pInd, 0, str(nowCon))
		setTableCell(self.ui.tableWidgetIButton, pInd, 1, str(key))
	
	elif sqi.ident&0xffffff00 ==  IBUT_EEPROM_CLASS:
		if sqi.errCode != 0xff:
			return
		keyNum = int.from_bytes(sqi.body[0], 'little')
		keyId = keyNum-1
		key = ""
		for i in range(8) :
			k = int.from_bytes(sqi.body[0+i], 'little')
			key += "%02x "%(k)

		setTableCell(self.ui.tableWidgetIButtonEepromKeys, keyId, 0, key)					
		
	elif (sqi.cmd == 0x05)  and (sqi.len == 3) and (sqi.flag == 2) :
		relePropArr = struct.unpack('<' +'B'*(len(strArr[9:11])//1), strArr[9:11])
		releInd = relePropArr[0]-1
		releState = relePropArr[1]==1
		#print("propArr: "+str(relePropArr) + " releState:"+str(releState) )
		if (releInd >= 0) and (releInd < 22) :
			rele.releCheckBoxes[releInd].setChecked(releState)			
	#elif (respCode == 0xff or respCode == 0x21) and respLen == 18 :
	#	#print("ds resp")			
	#	dsPresence = int.from_bytes(strArr[9], byteorder='little')
	#	tempDataArr = strArr[10:26]
	#	#print("int arr:" + str(len(tempDataArr)/2)+" :" + str(tempDataArr))
	#	tempInts = struct.unpack('<' +'H'*(len(tempDataArr)//2), tempDataArr)
	#	#print("dsPresence:" + str(dsPresence))
	#	for i in range(8):				
	#		cellText = ""
	#		if((dsPresence>>i)&0x1) != 0 :
	#			cellText = str(tempInts[i]) + "°"
	#		else :
	#			cellText = "N/A"
	#		tw = QTableWidgetItem(cellText)  #
	#		tw.setTextAlignment(Qt.AlignCenter)
	#		self.ui.tableWidgetDs19b20.setItem(i, 0,  tw)
	elif (sqi.cmd == 7) :
		#releState = struct.unpack('<' +'H'*1, strArr[9:11])
		if DEBUG_PRINT:
			print("releState: "  + ' '.join(x.hex() for x in sqi.body))
		sqi.body
		releState = int.from_bytes(sqi.body[0:2], byteorder='little')
		#if DEBUG_PRINT:
		#	print("releState:" + str(releState))
		i = 0
		for x in sqi.body:
			bReleOn = (int.from_bytes(x, byteorder='little') != 0)
			#self.releCheckBoxes[i].setCheckable(True)
			#print("bReleOn " +str(x) + " "+ str(bReleOn))
			rele.releCheckBoxes[i].setChecked(bReleOn)
			i += 1
			#self.releCheckBoxes[i].setCheckable(False)
			#if bReleOn :
			#	print("aa")
			#else :
			#	print( "N/A")
	#elif respCode == 0xff and respLen == 9 :  #get firmware 
	#	#print ("parse res")
	#	resInts = struct.unpack('<' +'B'*(len(strArr[9:17])//1), strArr[9:17])
	#	print(str(resInts))
	#	for i in range(8):				
	#		cellText = ""
	#		if resInts[i] != 0 :
	#			cellText = str(resInts[i]) + "b"
	#		else :
	#			cellText = "N/A"
	#		tw = QTableWidgetItem(cellText)  #
	#		tw.setTextAlignment(Qt.AlignCenter)
	#		self.ui.tableWidgetDs19b20.setItem(i, 4,  tw)
	#elif respCode == 0xff and respLen==7 :
	#	iCh = int.from_bytes(strArr[9], 'little') -1
	#	bDsPresence = int.from_bytes(strArr[10], 'little')
	#	bReleState = int.from_bytes(strArr[11], 'little')
	#	bChState = int.from_bytes(strArr[12], 'little')
	#	temp = struct.unpack('<H', strArr[13:15])[0]
	#				
	#	tw = QTableWidgetItem(str(temp))  #
	#	tw.setTextAlignment(Qt.AlignCenter)
	#	self.ui.tableWidgetDs19b20.setItem(iCh, 0, tw)
		
		#print ("get wf ch " +str(iCh) + " ds " + str(bDsPresence)+
		#			" releState:"+str(bReleState) + " t:"+hex(temp)  )
	elif sqi.cmd == 0x32:
		dimmer.parseDimmerGet(self, sqi)
	elif sqi.cmd == 0x33:
		dimmer.parseDimmerGet(self, sqi)
	elif sqi.cmd == 0x34:
		dimmer.parseDimmerGet(self, sqi)
	elif sqi.cmd == 0x35:
		dimmer.parseDimmerGet(self, sqi)
	elif sqi.cmd == 0x36:
		dimmer.parseDimmerGet(self, sqi)		
	elif sqi.cmd == 0x42:
		pwm.parsePWMGet(self, sqi)
	elif sqi.cmd == 0x43:
		pwm.parsePWMGet(self, sqi)
	elif sqi.cmd == 0x36:
		dimmer.parseDimmerGet(self, sqi)
	elif sqi.cmd == 0x4c:
		bEna = int.from_bytes(sqi.body[0], 'little')
		#print("ena: " + str(bEna))
		self.ui.checkBoxIrGateEventsStateSet.setChecked(bEna)
	elif sqi.cmd == 0x77:
		dali.parseDimmerGet(self, sqi)
			