from PyQt6 import QtWidgets, QtCore
from PyQt6.QtWidgets import QPushButton, QMessageBox, QInputDialog, QLineEdit, QRadioButton, QCheckBox
from PyQt6.QtWidgets import QVBoxLayout, QHBoxLayout, QSpinBox
from PyQt6.QtCore import Qt, QTimer, QTime
from PyQt6.QtWidgets import QDialog, QApplication, QHeaderView, QLabel, QWidget

dlg = None

butWrite = None

comboPortVector = None
checkBoxInvers = None
lineEditTempTresh = None
pIndex = -1

def showSensorDs8Props(self, pInd=-1):
	global dlg, butWrite, comboPortVector, checkBoxInvers, lineEditTempTresh
	global pIndex
	
	pIndex = pInd
	
	dlg = QDialog(self)
	dlg.setWindowTitle("sensor ds8 params")
	#self.setADCPropsWindow.setWindowModality(Qt.WindowModal)
	
	vLayout = QVBoxLayout()
	#self.comboReason = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboReason.addItems(["rise","fall"])		
	#self.comboAction = QtWidgets.QComboBox(self.setPortModeWindow)
	#self.comboAction.addItems(["on", "off", "inv"])
	
	#self.comboPortNum1 = QtWidgets.QComboBox()
	#self.comboActionPort = QtWidgets.QComboBox()		
	#portList = ["1","2","3","4","5","6","7","8"]
	#self.comboPortNum1.addItems(portList)			
	#self.comboActionPort.addItems(portList)
	
	#print("add c")
	comboPortAdcNum = QtWidgets.QComboBox()
	scrpitNumArr = []
	for i in range(1,12) :
		scrpitNumArr += [str(i)]
	#print(scrpitNumArr)
	comboPortAdcNum.addItems(scrpitNumArr)	

	#print("add kk")
	#self.lineEditTimeOut = QLineEdit()
	#self.lineEditTimeOut.setMaxLength(4)
	#self.lineEditTimeOut.setValidator(QIntValidator(3,0xffff)	)
	#self.lineEditTimeOut.setText("3")
	#self.lineEditTimeOut.setAlignment(QtCore.Qt.AlignCenter)
	#self.lineEditTimeOut.setFixedWidth(60)
	
	#self.lineEditTimeOut = QSpinBox(self.setPortModeWindow)
	#self.lineEditTimeOut.setMinimum(3)
	#self.lineEditTimeOut.setMaximum(0xffff)
			
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("номер порта:"))
	if pInd == -1:
		layout1.addWidget(comboPortAdcNum)			
	else:
		layout1.addWidget(QLabel(str(pInd+1)))					
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	
	
	comboPortVector = QtWidgets.QComboBox()
	vecArr = ["0", "1"]
	comboPortVector.addItems(vecArr)	

	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Вектор:"))
	layout1.addWidget(comboPortVector)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Инверсия:"))
	checkBoxInvers = QCheckBox()
	layout1.addWidget(checkBoxInvers)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)
	
	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Порог температуры, °:"))
	lineEditTempTresh = QLineEdit("0")
	layout1.addWidget(lineEditTempTresh)			
	layout1.addStretch(1)
	vLayout.addLayout(layout1)

	#layout1 = QHBoxLayout()
	#layout1.addWidget(QLabel("Мин T2:"))
	#layout1.addWidget(QLineEdit(""))			
	#layout1.addStretch(1)
	#vLayout.addLayout(layout1)

	layout1 = QHBoxLayout()
	layout1.addWidget(QLabel("Период отправки данных, с:"))
	self.dataPeriodLineEdit = dataPeriod = QtWidgets.QComboBox()
	dataPeriod.addItem("0 (OFF)", 0)
	dataPeriod.addItem("1", 1)
	dataPeriod.addItem("2", 2)
	dataPeriod.addItem("5", 5)
	dataPeriod.addItem("10", 10)
	dataPeriod.setFixedWidth(60)
	#dataPeriod.setAlignment(Qt.AlignHCenter)
	#layout1.addWidget(dataPeriod)
	#layout1.addStretch(1)
	#vLayout.addLayout(layout1)
	
	layout4 = QHBoxLayout()
	layout4.addStretch(1)
	
	butWrite = QPushButton("write")
	layout4.addWidget(butWrite)
	
	butOk = QPushButton("close")
	layout4.addWidget(butOk)
	butOk.clicked.connect(lambda: dlg.close())
	vLayout.addLayout(layout4)

	dlg.setLayout(vLayout)

def handleSetWfPortCount(self):
	v = self.ui.spinBoxWfPortCount.value()
	#print("" + str(v))
	self.sendQuery(SET_PROP, [0x00, v])


		
def handleUpdateSensorDS8(self):
	print("update DS8")
	#self.sendQuery(GET_PROP, [0x24, 4])
	self.sendQuery(GET_PROP, [0x25], c25_CLASS)
	for i in range(11) :
		#self.sendQuery(GET_PROP, [0xf6, i+1], SENSORDS8_CLASS+0xF6)
		self.sendQuery(GET_PROP, [0x21, i+1], c21_CLASS+i)
		self.sendQuery(GET_PROP, [0x22, i+1], c22_CLASS+i)
		self.sendQuery(GET_PROP, [0x23, i+1], c23_CLASS+i)
		self.sendQuery(GET_PROP, [0x24, i+1], c24_CLASS+i)
		self.sendQuery(GET_PROP, [0x26, i+1], c26_CLASS+i)

setMaximumConcChan=0
def handleSetMaxConcurrentChannel(self):
	ints = ("0","1","2", "3", "4","5","6","7","8","9")
	it, okPressed = QInputDialog.getItem(self, "Set maximum concurrent channel","concurrent channels:", ints, self.setMaximumConcChan, False)
	self.setMaximumConcChan = ints.index(it)
	print("index: " + str(self.ds19b20TimeoutItemIndex))
	if it=="off":
		it=0
	if okPressed:
		print("Set setMaximumConcChan to " + str(it))
		self.sendQuery(SET_PROP, [0x25, int(it)])

	#print("sendList: \n" + str(self.sendToComList))

def handleSensorDs8SetTemperatureTimeout(self):
	ints = ("0(off)","1","2", "3", "4","5","6","7","8","9")
	it, okPressed = QInputDialog.getItem(self, "Set temperature timeout","temperature timeout:", ints, self.setMaximumConcChan, False)
	print(" ll " + str(it))
	timeout = 0
	if it != "0(off)":
		timeout = int(it)
	if okPressed:
		print("Set set timeout temperature" + str(it))
		self.sendQuery(SET_PROP, [0x20, timeout])
	
ds19b20TimeoutItemIndex = 0
def handleButtonDs19b20SetTimeout(self) :
	ints = ("off", "1", "2", "5", "10", "100")
	it, okPressed = QInputDialog.getItem(self, "Set ds19b20 timeout","timeout, seconds:", ints, self.ds19b20TimeoutItemIndex, False)
	self.ds19b20TimeoutItemIndex = ints.index(it)
	print("index: " + str(self.ds19b20TimeoutItemIndex))
	if it=="off":
		it=0
	if okPressed:
		print("Set sensords8 timeout to " + str(it))
		self.sendQuery(SET_PROP, [0x20, int(it)])	
		
	
def handleSetSensorDs8Params(self, pInd=-1) :
	sensorDs8.showSensorDs8Props(self, pInd)
	sensorDs8.butWrite.clicked.connect(self.handleWriteSensorDS8Props)
	sensorDs8.dlg.exec()
	
	#showSensorDs8Props
	#temp, okPressed = QInputDialog.getInt(self, "Set channel temp treshold","Temp, °:", 28, 0, 100, 1)
	#print("handleSetDs8TreshTemp: " + str(pInd) + "  " + str(okPressed) + "  " + str(temp))
	#if okPressed == False: 
	#	return
	#self.sendQuery(GET_PROP, [0x22, pInd])
def handleWriteSensorDS8Props(self):
	if sensorDs8.pIndex == -1 :
		return
	pNum = sensorDs8.pIndex + 1
	vec = 0 if sensorDs8.comboPortVector.currentIndex() == 0   else 1
	inv = sensorDs8.checkBoxInvers.isChecked()
	tempTresh = int(sensorDs8.lineEditTempTresh.text())
	self.sendQuery(SET_PROP, [0x22, pNum, vec, inv, (tempTresh>>8)&0xff, tempTresh&0xff])
	
def handleWriteSensorDS8Ena(self, pInd, e):
	self.sendQuery(SET_PROP, [0x24, pInd+1, e])
	
def handleWriteSensorMaxTempTresh(self, pInd):
	temp, okPressed = QInputDialog.getInt(self, "Get delay","Seconds:", 28, 0, 100, 1)
	print("handleSetDs8TreshTemp: " + str(pInd) + "  " + str(okPressed) + "  " + str(temp))
	if okPressed == False:
		return
	self.sendQuery(SET_PROP, [0x26, pInd+1, (temp>>8)&0xff, temp&0xff])
	